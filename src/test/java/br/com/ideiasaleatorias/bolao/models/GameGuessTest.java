package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;


public class GameGuessTest {


	@Test
	public void shouldCalculateJustPrincipalHit(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 1, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 1, 2);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(2), result);
	}
	
	@Test
	public void shouldCalculateJustVisitingHit(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 0, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 1, 0);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(2), result);
	}
	
	@Test
	public void shouldCalculateHittingResultWithoutScores(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 0, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 2, 2);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(5), result);
	}
	
	@Test
	public void shouldCalculateHittingResultWithPartialVisitingScore(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 2, 1, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 3, 1);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(7), result);
	}
	
	@Test
	public void shouldCalculateHittingResultWithPartialPrincipalScore(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 2, 1, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 2, 0);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(7), result);
	}
	
	@Test
	public void shouldCalculateHitAll(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 2, 1, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 2, 1);
		Number result = guess.calculateScore(actualResult);
		
		Assert.assertEquals(new BigDecimal(10), result);
	}	
}
