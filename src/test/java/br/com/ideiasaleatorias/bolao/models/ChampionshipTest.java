package br.com.ideiasaleatorias.bolao.models;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.Test;

public class ChampionshipTest {

	@Test
	public void shoudVerifyIfChampionshipHasStarted() throws Exception {
		Championship championship = new Championship("brasileirao","/assets/link", LocalDateTime.now().plusMinutes(10),"brasileirao-2016");
		Assert.assertFalse(championship.hasStarted());
	}
}
