package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import br.com.ideiasaleatorias.bolao.password.Password;

public class SweepstakesStartGuessTest {

	private Team team1 = new Team("brasil", "brasil");
	private Team team2 = new Team("argentina", "argentina");
	private Team team3 = new Team("italia", "italia");
	private Team team4 = new Team("alemanha", "alemanha");
	private Championship championship = new Championship("campeonato", "http...",
			LocalDateTime.now(), "slug");
	private SystemUser member = new SystemUser("name", "email", Password.buildWithRawText("1234"),
			"frase", Role.PARTICIPANTE);

	@Test
	public void allGuessesAreAllowed() {
		StartGuessHolder holder = createHolder(StartGuessType.values());

		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("300"));
	}

	private StartGuessHolder createHolder(StartGuessType...userTypes) {
		StartGuessHolder holder = new StartGuessHolder() {
			private Set<StartGuessType> types = new HashSet<>();
			{
				types.addAll(Arrays.asList(userTypes));
			}

			@Override
			public boolean hasStartGuess(StartGuessType type) {
				return types.contains(type);
			}
		};
		return holder;
	}
	
	@Test
	public void onlyBestDefense() {
		StartGuessHolder holder = createHolder(StartGuessType.BEST_DEFENSE_TEAM);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("30"));
	}
	
	@Test
	public void onlyBestStrike() {
		StartGuessHolder holder = createHolder(StartGuessType.BEST_STRIKE_TEAM);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("30"));
	}
	
	@Test
	public void onlyChampion() {
		StartGuessHolder holder = createHolder(StartGuessType.CHAMPION_TEAM);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("30"));
	}
	
	@Test
	public void onlyG4() {
		StartGuessHolder holder = createHolder(StartGuessType.G4_TEAMS);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("60"));
	}
	
	@Test
	public void onlyZ4() {
		StartGuessHolder holder = createHolder(StartGuessType.Z4_TEAMS);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("60"));
	}
	
	@Test
	public void onlyViceChampionTeam() {
		StartGuessHolder holder = createHolder(StartGuessType.VICE_CHAMPIONTEAM);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("30"));
	}
	
	@Test
	public void onlyTopScorer() {
		StartGuessHolder holder = createHolder(StartGuessType.TOP_SCORER);
		SweepstakesStartGuess guess = new SweepstakesStartGuess(team1, team1, team1, team2, team3,
				"neymar", Arrays.asList(team1, team2), Arrays.asList(team3, team4), championship,
				member);
		BigDecimal value = guess.calculateExtraScore(guess, holder);
		Assert.assertEquals(value,
				new BigDecimal("30"));
	}	
}
