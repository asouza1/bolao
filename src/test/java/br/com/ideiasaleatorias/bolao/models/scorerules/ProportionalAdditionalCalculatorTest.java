package br.com.ideiasaleatorias.bolao.models.scorerules;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;

@RunWith(MockitoJUnitRunner.class)
public class ProportionalAdditionalCalculatorTest {
	
	@Mock
	private GameGuessDao gameGuessDao;
	
	@Test
	public void  shouldNotCalculateIfGameOutcomeIsWrong(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 1, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 0, 0);
		
		ProportionalAdditionalCalculator calculator = new ProportionalAdditionalCalculator(gameGuessDao);
		BigDecimal extra = calculator.calculate(guess, actualResult, BigDecimal.TEN);
		
		Assert.assertEquals(BigDecimal.ZERO, extra);
	}
	
	@Test
	public void  shouldDoubleIfNobodyGuessLikeMe(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 1, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 1, 0);	
		
		Mockito.when(gameGuessDao.averagePossibleGameResults(game.getId())).thenReturn(Arrays.asList());
		
		ProportionalAdditionalCalculator calculator = new ProportionalAdditionalCalculator(gameGuessDao);
		BigDecimal partial = BigDecimal.TEN;
		BigDecimal extra = calculator.calculate(guess, actualResult, partial);
		
		Assert.assertEquals(partial, extra);
	}
	
	@Test
	public void  shouldCalculateProportionalToTheRisk(){
		SweepstakesMember member = Mockito.mock(SweepstakesMember.class);
		Game game = Mockito.mock(Game.class);
		GameGuess guess = new GameGuess(member, 1, 0, false, game);
		ActualGameResultMessage actualResult = new ActualGameResultMessage(game.getId(), 1, 0);	
		
		Mockito.when(gameGuessDao.averagePossibleGameResults(game.getId())).thenReturn(threePossibleGamesOutcome(game));
		
		ProportionalAdditionalCalculator calculator = new ProportionalAdditionalCalculator(gameGuessDao);
		BigDecimal partial = BigDecimal.TEN;
		BigDecimal extra = calculator.calculate(guess, actualResult, partial);
		
		BigDecimal riskIncreasePercentage = new BigDecimal("0.67");
		Assert.assertEquals(partial.multiply(riskIncreasePercentage ).setScale(2,RoundingMode.HALF_UP), extra);
	}

	private List<AveragePossibleGameResult> threePossibleGamesOutcome(Game game) {
		AveragePossibleGameResult principalWinner = new AveragePossibleGameResult(game,GameOutcome.PRINCIPAL_WINNER, 1, 3);
		AveragePossibleGameResult visitingWinner = new AveragePossibleGameResult(game,GameOutcome.VISITING_WINNER, 1, 3);
		AveragePossibleGameResult draw = new AveragePossibleGameResult(game,GameOutcome.DRAW, 1, 3);
		return Arrays.asList(principalWinner,visitingWinner,draw);
	}
	
}
