<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<p>Para se cadastrar e depois aceitar participar do grupo, acesse ${hostUrlBuilder.externalUrl(acceptanceLink)}</p>
	<p>É importante que você se cadastre usando este mesmo email, caso contrário você não será capaz de ver seus convites.</p>
	<p>Se você já está cadastrado, você também pode se logar e aceitar o convite pendente.</p>
</body>
</html>