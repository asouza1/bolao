package br.com.ideiasaleatorias.bolao.views;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.models.Team;
import br.com.ideiasaleatorias.bolao.services.AppSecurityContext;

@Component
public class MyTeamView {

	@Autowired
	private AppSecurityContext currentUser;
	
	public boolean currentUserChoosen(Team team){
		Optional<Team> userTeam = currentUser.get().getTeam();
		if(userTeam.isPresent()){
			return userTeam.get().equals(team);
		}
		return false;
	}
}
