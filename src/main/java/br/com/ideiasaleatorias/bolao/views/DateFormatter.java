package br.com.ideiasaleatorias.bolao.views;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class DateFormatter {

	/**
	 * Formata com o pattern default: kk:mm dd/MM/yyyy
	 * @param time
	 * @return
	 */
	public String format(LocalDateTime time){
		return DateTimeFormatter.ofPattern("kk:mm dd/MM/yyyy").format(time);
	}
}
