package br.com.ideiasaleatorias.bolao.views;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.ideiasaleatorias.bolao.dtos.AveragePossibleGameResultsNormalizer;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;

public class ScoreProportionsView {


	private Map<Object, List<AveragePossibleGameResult>> proportionsByGame;

	public ScoreProportionsView(List<AveragePossibleGameResult> possibleResults) {
		proportionsByGame = possibleResults.stream().collect(Collectors.groupingBy(result -> {
			return result.getGame();
		}));	
	}
	
	public List<AveragePossibleGameResult> of(Game game){
		List<AveragePossibleGameResult> results = AveragePossibleGameResultsNormalizer.normalize(game,proportionsByGame.get(game));
		results.sort((avg1,avg2) -> {
			return avg1.getPossibleGameResult().compareTo(avg2.getPossibleGameResult());
		});
		
		return results;
	}

}
