package br.com.ideiasaleatorias.bolao.views;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.Game;

public class GameResultsView {


	private Map<Game, List<ActualGameResult>> resultByGame;

	public GameResultsView(List<ActualGameResult> results) {
		resultByGame = results.stream().collect(Collectors.groupingBy(result -> {
			return result.getGame();
		}));
	}
	
	public ActualGameResult of(Game game){
		List<ActualGameResult> list = resultByGame.get(game);
		return list != null ? list.get(0) : null;
	}

}
