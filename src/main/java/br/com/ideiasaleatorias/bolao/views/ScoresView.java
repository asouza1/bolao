package br.com.ideiasaleatorias.bolao.views;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.ideiasaleatorias.bolao.models.CalculatedRoundScore;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

public class ScoresView {

	private Map<GameGuess, List<CalculatedRoundScore>> scoreByGuess;

	public ScoresView(List<CalculatedRoundScore> scores) {
		scoreByGuess = scores.stream().collect(Collectors.groupingBy(score -> {
			return score.getGuess();
		}));
	}
	
	public CalculatedRoundScore of(GameGuess guess){
		List<CalculatedRoundScore> list = scoreByGuess.get(guess);
		return list != null ? list.get(0) : null;
	}

}
