package br.com.ideiasaleatorias.bolao.views;

import java.util.Optional;

import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.Team;

@Component
public class DashboardView {

	public String teamShieldImage(String anonymousUser){
		return  "";
	}
	
	public String teamShieldImage(SystemUser currentUser){
		Optional<Team> team = currentUser.getTeam();
		if(team.isPresent()){
			return "<img src='"+team.get().getShieldLink()+"' class='img-circle'>";
		}
		return "";
	}
	public String teamShieldImage(SystemUser currentUser, int width){
		Optional<Team> team = currentUser.getTeam();
		if(team.isPresent()){
			return "<img src='"+team.get().getShieldLink()+"' class='img-circle' "
					+ "style='width:"+ width + "vw'>";
		}
		return "";
	}
	
	public String teamColorPrimary(String anonymousUser){
		return "";
	}
	
	
	public String teamColorPrimary(SystemUser currentUser){
		Optional<Team> team = currentUser.getTeam();
		return team.flatMap(t -> t.getPrimaryColor()).orElse("");
	}
	
	public String teamColorSecondary(SystemUser currentUser){
		Optional<Team> team = currentUser.getTeam();
		return team.flatMap(t -> t.getSecondaryColor()).orElse("");
	}
	
	public String teamColorSecondary(String anonymousUser){
		return "";
	}
}
