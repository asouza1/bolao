package br.com.ideiasaleatorias.bolao.views;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import br.com.ideiasaleatorias.bolao.models.CalculatedRoundScore;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

public class Guesses implements Iterator<GameGuess>{

	private Iterator<GameGuess> iterator;
	private BigDecimal total;
	//1017614411622210

	public Guesses(List<GameGuess> guesses,ScoresView scoresView) {
		iterator = guesses.iterator();
		/*
		 * #TODO refatoracao para retornar optional ou outra coisa.
		 * aqui só tem objeto de view e realmente métodos retornam null, por conta que na expression language funciona bem. 
		 * Talvez possa ser refatorado... 
		 */
		this.total = guesses.stream().map(guess -> {
			CalculatedRoundScore calculatedRoundScore = scoresView.of(guess);
			if(calculatedRoundScore != null){
				return calculatedRoundScore.getScore();
			}
			return BigDecimal.ZERO;
		}).reduce(BigDecimal.ZERO, (acumulator,next) -> {
			if(next != null){
				return acumulator.add(next);
			}
			return BigDecimal.ZERO;
		});
	}

	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}
	
	public BigDecimal getTotal() {
		return total;
	}

	@Override
	public GameGuess next() {
		return iterator.next();
	}

}
