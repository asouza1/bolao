package br.com.ideiasaleatorias.bolao.views;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.Game;

public class GamesResults {

	private List<ActualGameResult> results;

	public GamesResults(List<ActualGameResult> results) {
		this.results = results;
	}

	public Optional<ActualGameResult> findResult(Game game) {
		Stream<ActualGameResult> foundResults = this.results.stream().filter(result -> result.getGame().equals(game));
		return foundResults.findFirst();
	}

	public Integer getPrincipalScore(Game game) {
		Optional<ActualGameResult> result = findResult(game);
		if(!result.isPresent()){
			return null;
		}
		
		return result.get().getPrincipalScore();
	}
	
	public Integer getVisitingScore(Game game) {
		Optional<ActualGameResult> result = findResult(game);
		if(!result.isPresent()){
			return null;
		}
		
		return result.get().getVisitingScore();
	}

}
