package br.com.ideiasaleatorias.bolao.security.conf;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.ideiasaleatorias.bolao.security.LoginSuccessHandler;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment env;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry authorizeRequests = http
				.authorizeRequests();

		if (env.acceptsProfiles("prod", "homolog")) {
			configureDefault(authorizeRequests
					.antMatchers("/connect/facebook", "/disconnect/facebook", "/facebook/login",
							"/", "/users/**", "/templates/email/**", "/rules/**",
							"/password/update", "/password/update/form").permitAll()
					.antMatchers("/magic/**").permitAll().antMatchers("/**").authenticated().and());
		} else {
			configureDefault(authorizeRequests.antMatchers("/**").permitAll().and());
		}

	}

	private void configureDefault(HttpSecurity security) throws Exception {
		security.formLogin().loginProcessingUrl("/login").loginPage("/")
				.successHandler(new LoginSuccessHandler())
				.failureUrl("/?error").permitAll().and()
				.exceptionHandling().accessDeniedPage("/WEB-INF/views/errors/403.jsp").and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).and()
				.rememberMe().and().csrf().disable();
	}

	@Autowired
	private DataSource datasource;
	@Autowired
	private UserDetailsService users;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(users).passwordEncoder(new BCryptPasswordEncoder());
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/assets/**");
	}

}
