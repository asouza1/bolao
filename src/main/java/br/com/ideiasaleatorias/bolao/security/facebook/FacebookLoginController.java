package br.com.ideiasaleatorias.bolao.security.facebook;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.AppSecurityContext;
import br.com.ideiasaleatorias.bolao.services.actions.SaveOrUpdateUserAction;

@Controller
@Transactional
@Scope(scopeName=WebApplicationContext.SCOPE_REQUEST)
public class FacebookLoginController {

	@Autowired
	private ConnectionRepository repository;
	@Autowired
	private AppSecurityContext loggedUser;
	@Autowired
	private SaveOrUpdateUserAction saveOrUpdateUserAction;

	@RequestMapping(value = "/facebook/login", method = RequestMethod.GET)
	public ModelAndView handleFacebookLogin(@RequestParam(required=false,defaultValue="") String code) {
		
		if(!StringUtils.hasText(code)){
			ModelAndView modelAndView = new ModelAndView("/home/index");
			ViewMessages.error("Não foi possível realizar seu login",modelAndView);
			return modelAndView;
		}
		
		LinkedMultiValueMap<String, String> usersIds = new LinkedMultiValueMap<String,String>();
		usersIds.add("facebook", code);		
		Connection<Facebook> connection = (Connection<Facebook>) repository.findConnectionsToUsers(usersIds).getFirst("facebook");		
		User profile = connection.getApi().fetchObject("me", User.class, "id", "name", "link", "email");
		SystemUser currentUser = saveOrUpdateUserAction.execute(profile);		
		loggedUser.changeCurrentUser(currentUser);
		
		
		

		return new ModelAndView("redirect:/dashboard");
	}
}
