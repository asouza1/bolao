package br.com.ideiasaleatorias.bolao.security.database;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Repository
public class LoginModule implements UserDetailsService {

	@Autowired
	private EntityManager manager;

	@Override
	public SystemUser loadUserByUsername(String email) throws UsernameNotFoundException {
		List<SystemUser> users = manager.createQuery(
				"select u from SystemUser u join fetch u.roles where u.email = :email",
				SystemUser.class).setParameter("email", email).getResultList();
		
		if (users.isEmpty()) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}
		
		Assert.isTrue(users.size() == 1,"ERRO GRAVE! Tem mais de um usuário com o mesmo email");
		
		return users.get(0);
	}

}
