package br.com.ideiasaleatorias.bolao.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.springframework.social.facebook.api.GraphApi;

@Embeddable
public class SocialInfo {

	@Column(name = "social_id", unique = true)
	private String id;
	@Column(name = "social_link")
	private String link;

	/**
	 * @deprecated
	 */
	public SocialInfo() {

	}

	public SocialInfo(String id, String link) {
		super();
		this.id = id;
		this.link = link;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getImageUrl() {
		return GraphApi.GRAPH_API_URL + this.id + "/picture";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SocialInfo other = (SocialInfo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
