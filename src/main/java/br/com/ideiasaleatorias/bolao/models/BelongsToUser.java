package br.com.ideiasaleatorias.bolao.models;

public interface BelongsToUser {

	public boolean belongsTo(SystemUser user);
}
