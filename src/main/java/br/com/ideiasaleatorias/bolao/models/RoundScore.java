package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

public class RoundScore implements RankingItem{
	
	private SystemUser user;
	private BigDecimal acumulatedScore;

	public RoundScore(SystemUser user, BigDecimal acumulatedScore) {
		super();
		this.user = user;
		this.acumulatedScore = acumulatedScore;
	}
	
	public String getMemberName() {
		return user.getName();
	}
	
	public BigDecimal getAcumulatedScore() {
		return acumulatedScore;
	}
	

}
