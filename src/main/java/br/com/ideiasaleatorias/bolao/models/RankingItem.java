package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

public interface RankingItem {

	String getMemberName();

	BigDecimal getAcumulatedScore();

}
