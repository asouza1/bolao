package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

/**
 * Classe utilizada para representar o relatorio de projeção de ranking
 * @author alberto
 *
 */
public class ProjectRankingViewEntry implements RankingItem,Comparable<ProjectRankingViewEntry>{

	private final RankingViewEntry rankingViewEntry;
	private final BigDecimal score;
	
	public ProjectRankingViewEntry(RankingViewEntry rankingViewEntry, BigDecimal extraScore) {
		this.rankingViewEntry = rankingViewEntry;
		this.score = extraScore.add(rankingViewEntry.getAcumulatedScore());
	}

	@Override
	public String getMemberName() {
		return rankingViewEntry.getMemberName();
	}

	@Override
	public BigDecimal getAcumulatedScore() {
		return score;
	}

	@Override
	public int compareTo(ProjectRankingViewEntry other) {
		return other.score.compareTo(this.score);
	}
	
	
}

