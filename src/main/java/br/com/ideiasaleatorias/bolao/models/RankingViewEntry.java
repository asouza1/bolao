package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

@Entity
public class RankingViewEntry implements RankingItem{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	private SweepstakesMember member;
	@ManyToOne
	private Championship championship;
	@ManyToOne
	private SystemUser systemUser;
	@Column(scale = 2)
	@Audited
	private BigDecimal acumulatedScore;

	/**
	 * @deprecated
	 */
	public RankingViewEntry() {

	}

	public RankingViewEntry(SweepstakesMember member, Championship championship,
			SystemUser systemUser, BigDecimal acumulatedScore) {
		super();
		this.member = member;
		this.championship = championship;
		this.systemUser = systemUser;
		this.acumulatedScore = acumulatedScore;
	}

	public void updateScore(BigDecimal score) {
		this.acumulatedScore = this.acumulatedScore.add(score);
	}
	
	public BigDecimal getAcumulatedScore() {
		return acumulatedScore;
	}
	
	public String getMemberName() {
		return systemUser.getName();
	}
	
	public SystemUser getSystemUser() {
		return systemUser;
	}

}
