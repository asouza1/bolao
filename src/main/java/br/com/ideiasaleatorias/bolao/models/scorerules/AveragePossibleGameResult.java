package br.com.ideiasaleatorias.bolao.models.scorerules;

import java.math.BigDecimal;
import java.math.RoundingMode;

import br.com.ideiasaleatorias.bolao.models.Game;

/**
 * Representa a proporção do {@link GameOutcome} perante o todo
 * 
 * @author alberto
 *
 */
public class AveragePossibleGameResult {

	private GameOutcome possibleGameResult;
	private BigDecimal percentage;
	private Game game;

	public AveragePossibleGameResult(Game game,GameOutcome possibleGameResult, Number guessTotalResult,Number allResults) {
		super();
		this.game = game;
		this.possibleGameResult = possibleGameResult;
		this.percentage = new BigDecimal(guessTotalResult.longValue()).divide(new BigDecimal(allResults.longValue()),2,RoundingMode.HALF_UP);		
		
	}
	
	public BigDecimal getPercentage() {
		return percentage;
	}
	
	public GameOutcome getPossibleGameResult() {
		return possibleGameResult;
	}
	
	/**
	 * 
	 * @return percentual multiplicado por 100. Ex: 0.9 => 90
	 */
	public BigDecimal getPercentageBy100(){
		return percentage.multiply(BigDecimal.valueOf(100));
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((game == null) ? 0 : game.hashCode());
		result = prime * result
				+ ((possibleGameResult == null) ? 0 : possibleGameResult.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AveragePossibleGameResult other = (AveragePossibleGameResult) obj;
		if (game == null) {
			if (other.game != null)
				return false;
		} else if (!game.equals(other.game))
			return false;
		if (possibleGameResult != other.possibleGameResult)
			return false;
		return true;
	}

	

	@Override
	public String toString() {
		return "AveragePossibleGameResult [possibleGameResult=" + possibleGameResult
				+ ", percentage=" + percentage + ", game=" + game.getId() + "]";
	}

	public Object getGame() {
		return game;
	}

	
	
	
		

}
