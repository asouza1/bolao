package br.com.ideiasaleatorias.bolao.models.scorerules;

public enum GameOutcome {

	PRINCIPAL_WINNER,VISITING_WINNER,DRAW;
	
	public static GameOutcome discover(HasGameResult result){
		if(result.getPrincipalScore() > result.getVisitingScore()){
			return PRINCIPAL_WINNER;
		}
		
		if(result.getPrincipalScore() < result.getVisitingScore()){
			return VISITING_WINNER;
		}
		
		return DRAW;
		

	}
}
