package br.com.ideiasaleatorias.bolao.models;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Championship {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	private String name;
	@NotBlank
	private String flagLink;
	@ManyToMany
	private Set<Team> teams = new HashSet<Team>();
	@NotNull
	private LocalDateTime startTime;
	@NotBlank
	@Column(unique=true)
	private String slug;
	@Column(columnDefinition="tinyint(1) default 0")
	private boolean active = true;
	
	/**
	 * @deprecated
	 */
	public Championship() {

	}
	
	public Championship(String name, String flagLink,LocalDateTime startTime,String slug) {
		this.name = name;
		this.flagLink = flagLink;
		this.startTime = startTime;
		this.slug = slug;
		
		
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public LocalDateTime getStartTime() {
		return startTime;
	}

	public void addTeam(Team team) {
		teams.add(team);
	}

	@Override
	public String toString() {
		return "Championship [name=" + name + ", teams=" + teams + ", startTime=" + startTime + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Championship other = (Championship) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public boolean hasStarted() {
		return LocalDateTime.now().compareTo(this.startTime) >= 0;
	}
	
	
	
	
}
