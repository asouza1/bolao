package br.com.ideiasaleatorias.bolao.models.scorerules;

import java.math.BigDecimal;

import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

/**
 * Define o comportamento padrão de alguém que calcula a pontuação de um jogo.
 * @author alberto
 *
 */
public interface AdditionalRankingScoreCalculator {

	/**
	 * 
	 * @param guess
	 * @param message
	 * @param partial valor inicial para base do calculo
	 * @return o que deve ser somado a parcial.
	 */
	BigDecimal calculate(GameGuess guess, ActualGameResultMessage message, BigDecimal partial);

}