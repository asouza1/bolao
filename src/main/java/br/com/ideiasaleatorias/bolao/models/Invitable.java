package br.com.ideiasaleatorias.bolao.models;

public interface Invitable {

	public Iterable<GroupInvite> getInvites();
}
