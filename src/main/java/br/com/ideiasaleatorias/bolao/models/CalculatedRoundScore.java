package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class CalculatedRoundScore {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@OneToOne
	@JoinColumn(unique=true)
	private GameGuess guess;
	@Column(scale=2)
	private BigDecimal score;
	
	/**
	 * @deprecated
	 */
	public CalculatedRoundScore() {

	}

	public CalculatedRoundScore(GameGuess guess, BigDecimal score) {
		this.guess = guess;
		this.score = score;
	}

	public GameGuess getGuess() {
		return guess;
	}
	
	public BigDecimal getScore() {
		return score;
	}

	@Override
	public String toString() {
		return "CalculatedRoundScore [score=" + score + "]";
	}

	
	
}
