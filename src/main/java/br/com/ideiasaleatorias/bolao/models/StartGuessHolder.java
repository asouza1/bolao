package br.com.ideiasaleatorias.bolao.models;

public interface StartGuessHolder {

	boolean hasStartGuess(StartGuessType type);
}
