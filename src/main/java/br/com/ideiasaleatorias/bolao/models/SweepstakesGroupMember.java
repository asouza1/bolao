package br.com.ideiasaleatorias.bolao.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Membro de um {@link SweepstakesGroup}}(grupo especifico de bolao de um campeonato)
 * @author alberto
 *
 */
@Entity
public class SweepstakesGroupMember {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	@NotNull
	private SweepstakesGroup group;
	@ManyToOne
	@NotNull
	private SystemUser member;
	@NotNull
	private LocalDateTime creationDate;
	
	/**
	 * @deprecated
	 */
	public SweepstakesGroupMember() {

	}

	public SweepstakesGroupMember(SweepstakesGroup group, SystemUser currentUser) {
		this.group = group;
		member = currentUser;
		creationDate = LocalDateTime.now();
	}
	
	public SystemUser getMember() {
		return member;
	}
	
	public Championship getChampionship(){
		return group.getChampionship();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SweepstakesGroupMember other = (SweepstakesGroupMember) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		return true;
	}
	
	
	
	
	
}
