package br.com.ideiasaleatorias.bolao.models.scorerules;

import java.math.BigDecimal;

import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

@Component
public class DoubleResultAdditionalCalculator implements AdditionalRankingScoreCalculator{

	@Override
	public BigDecimal calculate(GameGuess guess, ActualGameResultMessage message, BigDecimal partial) {
		if(!guess.isDoubled()){
			return BigDecimal.ZERO;
		}
		return partial;
	}

}
