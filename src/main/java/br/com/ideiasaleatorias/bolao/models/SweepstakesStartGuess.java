package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.ForeignKey;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import br.com.ideiasaleatorias.bolao.validations.GuessTimeBeforeChampionship;

/**
 * Palpite inicial do campeonato, sobre coisas mais gerais
 * 
 * @author alberto
 *
 */
@Entity
@GuessTimeBeforeChampionship
public class SweepstakesStartGuess {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull
	@ManyToOne
	private Team bestDefenseTeam;
	@NotNull
	@ManyToOne
	private Team bestStrikeTeam;
	@NotNull
	@ManyToOne
	private Team championTeam;
	@NotNull
	@ManyToOne
	private Team worstTeam;
	@NotNull
	@ManyToOne
	private Championship championship;
	@ManyToOne
	@NotNull
	private SystemUser member;
	@NotNull
	private LocalDateTime creationTime;
	@NotNull
	private LocalDateTime updateTime;
	@Size(min = 4, max = 4)
	@ManyToMany
	@BatchSize(size = 4)
	private Set<Team> g4Teams = new HashSet<Team>();
	@Size(min = 4, max = 4)
	@BatchSize(size = 4)
	@ManyToMany
	private Set<Team> z4Teams = new HashSet<Team>();
	@NotNull
	@ManyToOne
	//FIXME por algum motivo tava dando erro na geração da constaint da chave estrangeira, não faço ideia do motivo. 
	@org.hibernate.annotations.ForeignKey(name = "none")
	private Team viceChampionTeam;
	@NotBlank
	private String topScorer;

	/**
	 * @deprecated
	 */
	public SweepstakesStartGuess() {

	}

	// TODO talvez seja um bom caso para um builder
	public SweepstakesStartGuess(Team bestDefenseTeam, Team bestStrikeTeam, Team championTeam,
			Team worstTeam, Team viceChampionTeam,String topScorer,Iterable<Team> g4Teams, Iterable<Team> z4Teams,
			Championship championship, SystemUser member) {
		super();
		this.bestDefenseTeam = bestDefenseTeam;
		this.bestStrikeTeam = bestStrikeTeam;
		this.championTeam = championTeam;
		this.worstTeam = worstTeam;
		this.viceChampionTeam = viceChampionTeam;
		this.topScorer = topScorer;
		this.championship = championship;
		this.member = member;
		this.creationTime = LocalDateTime.now();
		this.updateTime = LocalDateTime.now();
		g4Teams.forEach(team -> this.g4Teams.add(team));
		z4Teams.forEach(team -> this.z4Teams.add(team));
	}

	@PreUpdate
	private void preUpdate() {
		this.updateTime = LocalDateTime.now();
	}

	public Set<Team> getG4Teams() {
		return g4Teams;
	}

	public Set<Team> getZ4Teams() {
		return z4Teams;
	}

	public void updateWithNewGuess(SweepstakesStartGuess newGuess) {
		this.championship = newGuess.championship;
		this.bestDefenseTeam = newGuess.bestDefenseTeam;
		this.bestStrikeTeam = newGuess.bestStrikeTeam;
		this.championTeam = newGuess.championTeam;
		this.worstTeam = newGuess.worstTeam;
		this.g4Teams.clear();
		this.g4Teams.addAll(newGuess.getG4Teams());
		this.z4Teams.clear();
		this.z4Teams.addAll(newGuess.getZ4Teams());	
		this.viceChampionTeam = newGuess.viceChampionTeam;
		this.topScorer = newGuess.topScorer;
	}

	public Team getBestDefenseTeam() {
		return bestDefenseTeam;
	}

	public Team getBestStrikeTeam() {
		return bestStrikeTeam;
	}

	public Team getChampionTeam() {
		return championTeam;
	}

	public Team getWorstTeam() {
		return worstTeam;
	}

	public Championship getChampionship() {
		return championship;
	}

	public boolean beforeChampionshipTime() {
		return !championship.hasStarted();
	}
	
	public SystemUser getMember() {
		return member;
	}
	
	public String getTopScorer() {
		return topScorer;
	}
	
	public Team getViceChampionTeam() {
		return viceChampionTeam;
	}

	@Override
	public String toString() {
		return "SweepstakesStartGuess [bestDefenseTeam=" + bestDefenseTeam + ", bestStrikeTeam="
				+ bestStrikeTeam + ", championTeam=" + championTeam + ", worstTeam=" + worstTeam
				+ ", g4Teams=" + g4Teams + ", z4Teams=" + z4Teams + "]";
	}
	
	public BigDecimal calculateExtraScore(SweepstakesStartGuess guessToCompare, StartGuessHolder startGuessHolder) {
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal extra = new BigDecimal("30");
				
		if (startGuessHolder.hasStartGuess(StartGuessType.CHAMPION_TEAM) && guessToCompare.championTeam.equals(this.championTeam)) {
			total = total.add(extra);
		}
		
		if (startGuessHolder.hasStartGuess(StartGuessType.VICE_CHAMPIONTEAM) && guessToCompare.viceChampionTeam.equals(this.viceChampionTeam)) {
			total = total.add(extra);
		}

		if (startGuessHolder.hasStartGuess(StartGuessType.BEST_STRIKE_TEAM) && guessToCompare.bestStrikeTeam.equals(this.bestStrikeTeam)) {
			total = total.add(extra);
		}

		if (startGuessHolder.hasStartGuess(StartGuessType.BEST_DEFENSE_TEAM) && guessToCompare.bestDefenseTeam.equals(this.bestDefenseTeam)) {
			total = total.add(extra);
		}

		if (startGuessHolder.hasStartGuess(StartGuessType.WORST_TEAM) && guessToCompare.worstTeam.equals(this.worstTeam)) {
			total = total.add(extra);
		}
				
		if(startGuessHolder.hasStartGuess(StartGuessType.TOP_SCORER) && guessToCompare.topScorer.toUpperCase().equals(topScorer.toUpperCase())) {
			total = total.add(extra);
		}

		if(startGuessHolder.hasStartGuess(StartGuessType.G4_TEAMS)) {
			BigDecimal g4Extra = g4Teams.stream()
					.map(team -> guessToCompare.getG4Teams().contains(team) ? extra : BigDecimal.ZERO)
					.reduce(BigDecimal.ZERO, (accumlate, next) -> accumlate.add(next));
			
			total = total.add(g4Extra);
		}
		
		
		if(startGuessHolder.hasStartGuess(StartGuessType.Z4_TEAMS)) {
			BigDecimal z4Extra = z4Teams.stream()
					.map(team -> guessToCompare.getZ4Teams().contains(team) ? extra : BigDecimal.ZERO)
					.reduce(BigDecimal.ZERO, (accumlate, next) -> accumlate.add(next));
			total = total.add(z4Extra);
		}

		return total;
	}

}
