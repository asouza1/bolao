package br.com.ideiasaleatorias.bolao.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Participante do bolão do campeonato no geral
 * @author alberto
 *
 */
@Entity
public class SweepstakesMember {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	@NotNull
	private Championship championship;
	@ManyToOne
	private SystemUser systemUser;
	@NotNull
	private LocalDateTime joinTime;

	/**
	 * @deprecated
	 */
	public SweepstakesMember() {

	}

	public SweepstakesMember(Championship championship, SystemUser systemUser) {
		super();
		this.championship = championship;
		this.systemUser = systemUser;
		this.joinTime = LocalDateTime.now();
	}
	
	public Championship getChampionship() {
		return championship;
	}
	
	public SystemUser getSystemUser() {
		return systemUser;
	}

	public Integer getId() {
		return this.id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((championship == null) ? 0 : championship.hashCode());
		result = prime * result + ((systemUser == null) ? 0 : systemUser.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SweepstakesMember other = (SweepstakesMember) obj;
		if (championship == null) {
			if (other.championship != null)
				return false;
		} else if (!championship.equals(other.championship))
			return false;
		if (systemUser == null) {
			if (other.systemUser != null)
				return false;
		} else if (!systemUser.equals(other.systemUser))
			return false;
		return true;
	}

	
}
