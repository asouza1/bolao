package br.com.ideiasaleatorias.bolao.models;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.config.IntervalTask;

import br.com.ideiasaleatorias.bolao.validations.GameTimeAfterChampionship;

@Entity
@GameTimeAfterChampionship
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	@NotNull
	private Team principalTeam;
	@ManyToOne
	@NotNull
	private Team visitingTeam;
	@NotNull
	@DateTimeFormat(pattern = "dd/MM/yyyy kk:mm")
	private LocalDateTime gameTime;
	@NotNull
	@ManyToOne
	private Championship championship;
	@NotBlank
	private String site;
	@NotNull
	@Min(1)
	private Integer round;
	@NotBlank
	@Column(unique=true)
	//#gambi saída canalha para não usar uma chave composta gerenciada pelo hibernate
	private String compositeKey;

	/**
	 * @deprecated
	 */
	public Game() {

	}

	public Game(Team principalTeam, Team visitingTeam, LocalDateTime gameTime,
			Championship championship, String site,Integer round) {
		super();
		this.principalTeam = principalTeam;
		this.visitingTeam = visitingTeam;
		this.gameTime = gameTime;
		this.championship = championship;
		this.site = site;
		this.round = round;
		this.compositeKey = principalTeam.getId().toString()+"-"+visitingTeam.getId().toString()+"-"+round+"-"+championship.getId();
	}
	
	@Override
	public String toString() {
		return "Game [principalTeam=" + principalTeam + ", visitingTeam=" + visitingTeam
				+ ", gameTime=" + gameTime + ", championship=" + championship.getName() + ", site=" + site
				+ "]";
	}

	public boolean afterChampionshipStart() {
		return gameTime.compareTo(this.championship.getStartTime()) >= 0;
	}
	
	public Integer getId() {
		return id;
	}
	
	public Team getPrincipalTeam() {
		return principalTeam;
	}
	
	public Team getVisitingTeam() {
		return visitingTeam;
	}
	
	public LocalDateTime getGameTime() {
		return gameTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((championship == null) ? 0 : championship.hashCode());
		result = prime * result + ((principalTeam == null) ? 0 : principalTeam.hashCode());
		result = prime * result + ((round == null) ? 0 : round.hashCode());
		result = prime * result + ((visitingTeam == null) ? 0 : visitingTeam.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Game other = (Game) obj;
		if (championship == null) {
			if (other.championship != null)
				return false;
		} else if (!championship.equals(other.championship))
			return false;
		if (principalTeam == null) {
			if (other.principalTeam != null)
				return false;
		} else if (!principalTeam.equals(other.principalTeam))
			return false;
		if (round == null) {
			if (other.round != null)
				return false;
		} else if (!round.equals(other.round))
			return false;
		if (visitingTeam == null) {
			if (other.visitingTeam != null)
				return false;
		} else if (!visitingTeam.equals(other.visitingTeam))
			return false;
		return true;
	}
	
	public Championship getChampionship() {
		return championship;
	}

	public int getRound() {
		return round;
	}

	public boolean allowGuess() {
		int timeUntilTheGame = 10;
		return LocalDateTime.now().plus(timeUntilTheGame, ChronoUnit.MINUTES).compareTo(gameTime) <= 0;
	}
	

}
