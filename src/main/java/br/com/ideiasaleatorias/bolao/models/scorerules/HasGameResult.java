package br.com.ideiasaleatorias.bolao.models.scorerules;

public interface HasGameResult {

	public int getPrincipalScore();
	
	public int getVisitingScore();
}
