package br.com.ideiasaleatorias.bolao.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.com.ideiasaleatorias.bolao.models.scorerules.HasGameResult;

@Entity
public class ActualGameResult implements Serializable,HasGameResult{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2560633012873649561L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@OneToOne
	@NotNull
	@JoinColumn(unique=true)
	private Game game;
	@Min(0)
	private int principalScore;
	@Min(0)
	private int visitingScore;	
	@NotNull
	private LocalDateTime creationTime = LocalDateTime.now();
	
	/**
	 * @deprecated
	 */
	public ActualGameResult() {

	}

	public ActualGameResult(Game game, int principalScore, int visitingScore) {
		this.game = game;
		this.principalScore = principalScore;
		this.visitingScore = visitingScore;		
	}
	
	public LocalDateTime getCreationTime() {
		return creationTime;
	}
	
	public int getPrincipalScore() {
		return principalScore;
	}
	
	public int getVisitingScore() {
		return visitingScore;
	}
	
	public Game getGame() {
		return game;
	}

}
