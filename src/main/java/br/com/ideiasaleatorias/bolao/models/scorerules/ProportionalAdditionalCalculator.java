package br.com.ideiasaleatorias.bolao.models.scorerules;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

/**
 * Calculador de pontuacao proporcional ao risco assumido no palpite.
 * @author alberto
 *
 */
@Component
public class ProportionalAdditionalCalculator {

	private GameGuessDao gameGuessDao;
	private static Logger logger = LoggerFactory.getLogger(ProportionalAdditionalCalculator.class);

	@Autowired
	public ProportionalAdditionalCalculator(GameGuessDao gameGuessDao) {
		super();
		this.gameGuessDao = gameGuessDao;
	}

	public BigDecimal calculate(GameGuess guess, ActualGameResultMessage message, BigDecimal partial) {
		if (!guess.getPossibleGameResult().equals(message.getGameOutcome())) {
			return BigDecimal.ZERO;
		}

		List<AveragePossibleGameResult> report = gameGuessDao.averagePossibleGameResults(message
				.getGameId());

		Optional<AveragePossibleGameResult> average = report.stream().filter(item -> {			
			return item.getPossibleGameResult().equals(guess.getPossibleGameResult());
		}).findFirst();

		if (average.isPresent()) {
			BigDecimal risk = BigDecimal.ONE.subtract(average.get().getPercentage());
			if (risk.compareTo(BigDecimal.ZERO) == 0) {
				return BigDecimal.ZERO;
			}
			BigDecimal actualCalculatedValue = partial.multiply(risk);
			logger.info("Valor real calculado {} e valor arrendodado utilizado {}",
					actualCalculatedValue, actualCalculatedValue.setScale(2, RoundingMode.HALF_UP));
			return actualCalculatedValue.setScale(2, RoundingMode.HALF_UP);
		}

		return partial;

	}

}
