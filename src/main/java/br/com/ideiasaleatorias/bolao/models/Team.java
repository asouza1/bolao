package br.com.ideiasaleatorias.bolao.models;

import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	@Column(unique = true)
	private String name;
	private String shieldLink;
	@NotBlank
	@Column(unique = true)
	private String slug;
	private String primaryColor;
	private String secondColor;
	

	/**
	 * @deprecated
	 */
	public Team() {

	}

	public Team(String nome, String slug) {
		this.name = nome;
		this.shieldLink = "/assets/times/"+slug+".png";
		this.slug = slug;
	}
		
	public void changeColors(String primary,String second){
		this.primaryColor = primary;
		this.secondColor = second;
	}
	
	public String getShieldLink() {
		return shieldLink;
	}
	
	public String getSlug() {
		return slug;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getId() {
		return id;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((slug == null) ? 0 : slug.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (slug == null) {
			if (other.slug != null)
				return false;
		} else if (!slug.equals(other.slug))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Team [name=" + name + "]";
	}
	
	public Optional<String> getPrimaryColor() {
		return Optional.ofNullable(primaryColor);
	}
	
	public Optional<String> getSecondaryColor() {
		return Optional.ofNullable(secondColor);
	}

	public void changeSlug(String newSlug) {
		this.slug = newSlug;
	}

}
