package br.com.ideiasaleatorias.bolao.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.models.scorerules.GameOutcome;
import br.com.ideiasaleatorias.bolao.models.scorerules.HasGameResult;

@Entity
public class GameGuess implements HasGameResult {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@ManyToOne
	private SweepstakesMember member;
	@Min(0)
	private int principalScore;
	@Min(0)
	private int visitingScore;
	@NotNull
	private LocalDateTime time = LocalDateTime.now();
	private boolean doubled;
	@ManyToOne
	private Game game;
	@NotNull
	private GameOutcome possibleGameResult;
	@NotNull
	@Column(unique=true)
	private String compositeKey;
	
	
	/**
	 * @deprecated
	 */
	public GameGuess() {

	}


	public GameGuess(SweepstakesMember member, int principalScore,
			int visitingScore, boolean doubled,Game game) {
				this.member = member;
				this.principalScore = principalScore;
				this.visitingScore = visitingScore;
				this.doubled = doubled;
				this.game = game;
				this.possibleGameResult = GameOutcome.discover(this);
				this.compositeKey = game.getId() + "-" + member.getId();
	}
	
	public SweepstakesMember getMember() {
		return member;
	}
	
	public GameOutcome getPossibleGameResult() {
		return possibleGameResult;
	}
	
	public Game getGame() {
		return game;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public boolean isDoubled() {
		return doubled;
	}
	
	public int getPrincipalScore() {
		return principalScore;
	}
	
	public int getVisitingScore() {
		return visitingScore;
	}

	public LocalDateTime getTime() {
		return time;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((game == null) ? 0 : game.hashCode());
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameGuess other = (GameGuess) obj;
		if (game == null) {
			if (other.game != null)
				return false;
		} else if (!game.equals(other.game))
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		return true;
	}
	
	public BigDecimal calculateScore(ActualGameResultMessage message) {
		int total = 0;
		
		boolean hitPrincipalScore = this.getPrincipalScore() == message.getPrincipalScore();
		if(hitPrincipalScore){
			total +=2;
		}
		
		boolean hitVisitingScore = this.getVisitingScore() == message.getVisitingScore();
		if(hitVisitingScore){
			total +=2;
		}
				
		boolean hitAtLeastResult = message.getGameOutcome().equals(this.getPossibleGameResult());
		if(hitAtLeastResult){
			total +=5;
		}
		
		if(hitPrincipalScore && hitVisitingScore && hitAtLeastResult){
			total +=1;
		}
		
		return new BigDecimal(total);
	}	
	
	
	
}
