package br.com.ideiasaleatorias.bolao.models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Convite genérico para qualquer email
 * @author alberto
 *
 */
@Entity
public class GroupInvite implements BelongsToUser{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	@Email
	private String email;
	@NotBlank
	private String uuid = UUID.randomUUID().toString();
	private boolean accepted;
	@ManyToOne
	@NotNull
	private SweepstakesGroup sweepstakesGroup;

	/**
	 * @deprecated
	 */
	public GroupInvite() {

	}

	public GroupInvite(String email,SweepstakesGroup sweepstakesGroup) {
		super();
		this.email = email;
		this.sweepstakesGroup = sweepstakesGroup;
	}
	
	public SweepstakesGroup getSweepstakesGroup() {
		return sweepstakesGroup;
	}
	
	public String getUuid() {
		return uuid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupInvite other = (GroupInvite) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	public void accept() {
		this.accepted = true;
	}

	public boolean isAccepted() {
		return accepted;
	}
	
	public String getEmail() {
		return email;
	}

	public Integer getId() {
		return id;
	}

	public boolean belongsTo(SystemUser currentUser) {
		return this.email.equals(currentUser.getEmail());
	}

	@Override
	public String toString() {
		return "GroupInvite [email=" + email + ", accepted=" + accepted + "]";
	}
	
	

}
