package br.com.ideiasaleatorias.bolao.models;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import br.com.ideiasaleatorias.bolao.password.Password;

@Entity
public class SystemUser implements UserDetails{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	private String name;
	@NotBlank
	@Email
	@Column(unique=true)
	private String email;
	@Embedded
	private SocialInfo socialInfo = new SocialInfo();
	@ManyToMany
	private Set<Role> roles = new HashSet<Role>();
	@ManyToOne
	private Team team;
	private String password;
	@Column(unique=true)
	private String secretPhrase;
	
	/**
	 * @deprecated
	 */
	public SystemUser() {

	}
	
	public SystemUser(Integer id) {
		this.id = id;
		
	}

	public SystemUser(String name, String email, Password password, String secretPhrase, Role... roles) {
		this.name = name;
		this.email = email;
		this.secretPhrase = secretPhrase;
		this.password = password.getEncoded();
		this.roles.addAll(Arrays.asList(roles));
	}
	
	public String getSecretPhrase() {
		return secretPhrase;
	}
	
	public void setSecretPhrase(String secretPhrase) {
		this.secretPhrase = secretPhrase;
	}

	public SocialInfo getSocialInfo() {
		return socialInfo;
	}

	public void setSocialInfo(SocialInfo socialInfo) {
		this.socialInfo = socialInfo;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public SystemUser updateSocial(org.springframework.social.facebook.api.User profile) {		
		this.socialInfo.setLink(profile.getLink());
		this.socialInfo.setId(profile.getId());
		this.email = profile.getEmail();
		this.name = profile.getName();
		return this;
		
	}
	
	public static SystemUser buildFrom(org.springframework.social.facebook.api.User profile,Role... perfis) {
		SystemUser user = new SystemUser();
		for (Role perfil : perfis) {
			user.addRole(perfil);			
		}
		return user.updateSocial(profile);
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return email;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public void addRole(Role role) {
		roles.add(role);
	}

	public Optional<Team> getTeam() {
		return Optional.ofNullable(team);
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemUser other = (SystemUser) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}

	public void changePassword(Password password) {
		this.password = password.getEncoded();
	}

	public boolean isSocialUser() {
		return socialInfo!=null && StringUtils.hasLength(socialInfo.getId());
	}
	
	
}
