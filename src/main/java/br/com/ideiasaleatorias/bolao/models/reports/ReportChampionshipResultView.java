package br.com.ideiasaleatorias.bolao.models.reports;

import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.Team;
import br.com.ideiasaleatorias.bolao.models.scorerules.GameOutcome;

public class ReportChampionshipResultView {

	private ActualGameResult result;
	private Team teamAnalysed;

	public ReportChampionshipResultView(ActualGameResult result,Team teamAnalysed) {
		this.result = result;
		this.teamAnalysed = teamAnalysed;
	}

	public int getPrincipalScore() {
		return result.getPrincipalScore();
	}

	public int getVisitingScore() {
		return result.getVisitingScore();
	}

	public Game getGame() {
		return result.getGame();
	}
	
	/**
	 * 
	 * @return uma string representando o resultado do time sendo analisado
	 */
	public String getOutcome(){
		GameOutcome outcome = GameOutcome.discover(result);
		if(outcome.equals(GameOutcome.DRAW)){
			return "empate";
		}
		
		if(result.getGame().getPrincipalTeam().equals(teamAnalysed) && outcome.equals(GameOutcome.PRINCIPAL_WINNER)){
			return "vitoria";
		}
		
		if(result.getGame().getVisitingTeam().equals(teamAnalysed) && outcome.equals(GameOutcome.VISITING_WINNER)){
			return "vitoria";
		}
		
		return "derrota";
	}
	
	

}
