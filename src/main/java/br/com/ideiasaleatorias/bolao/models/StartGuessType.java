package br.com.ideiasaleatorias.bolao.models;

/**
 * Representa os tipos de palpites iniciais que um grupo do bolão pode suportar
 * 
 * @author alberto
 *
 */
public enum StartGuessType {

	BEST_DEFENSE_TEAM("melhor defesa"), BEST_STRIKE_TEAM("melhor ataque"), CHAMPION_TEAM(
			"campeão"), WORST_TEAM("pior ataque"), G4_TEAMS("g4"), Z4_TEAMS(
					"z4"), VICE_CHAMPIONTEAM("vice-campeão"), TOP_SCORER("artilheiro");

	private String label;

	StartGuessType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public String getName() {
		return this.name();
	}

}
