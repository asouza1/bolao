package br.com.ideiasaleatorias.bolao.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Representa o grupo do bolão
 * 
 * @author alberto
 *
 */
@Entity
public class SweepstakesGroup implements Invitable, BelongsToUser,StartGuessHolder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotBlank
	private String name;
	// SADPANDA nao consegui evitar esse bidirecional aqui. Não sei se realmente
	// precisa ter esse relacionamento
	@OneToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "sweepstakesGroup")
	private Set<GroupInvite> invites = new HashSet<GroupInvite>();
	@ManyToOne
	@NotNull
	private Championship championship;
	@ManyToOne
	@NotNull
	private SystemUser owner;
	@NotBlank
	@Column(unique = true)
	private String shareCode;
	@OneToMany(mappedBy = "group")
	private List<SweepstakesGroupMember> members = new ArrayList<>();
	@ElementCollection
	@Size(min=1)
	@Enumerated(EnumType.STRING)
	private Set<StartGuessType> startTypeGuesses = new HashSet<>();

	/**
	 * @deprecated
	 */
	public SweepstakesGroup() {
	}

	public SweepstakesGroup(String name, Championship championship, SystemUser owner, Collection<StartGuessType> startTypeGuesses) {
		super();
		this.name = name;
		this.championship = championship;
		this.owner = owner;
		this.invites.addAll(invites);
		this.shareCode = UUID.randomUUID().toString();
		this.startTypeGuesses.addAll(startTypeGuesses);
	}

	public void addInvites(Collection<GroupInvite> newInvites) {
		invites.addAll(newInvites);
	}

	public String getShareCode() {
		return shareCode;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public Iterable<GroupInvite> getInvites() {
		// mas perceba que eu retorno Iterable, para não deixar ninguém mexer na
		// lista
		return invites;
	}

	public Championship getChampionship() {
		return championship;
	}

	public boolean accepts(SystemUser currentUser) {
		return owner.equals(currentUser)
				|| members.contains(new SweepstakesGroupMember(this, currentUser))
				|| invites.stream().anyMatch(invite -> {
					return invite.belongsTo(currentUser);
				});
	}

	@Override
	public boolean belongsTo(SystemUser user) {
		return owner.equals(user);
	}

	public Set<GroupInvite> copyInvites(SweepstakesGroup newGroup) {
		return invites.stream().map(invite -> {
			return new GroupInvite(invite.getEmail(), newGroup);
		}).collect(Collectors.toSet());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((shareCode == null) ? 0 : shareCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SweepstakesGroup other = (SweepstakesGroup) obj;
		if (shareCode == null) {
			if (other.shareCode != null)
				return false;
		} else if (!shareCode.equals(other.shareCode))
			return false;
		return true;
	}

	public boolean hasStartGuess(StartGuessType type) {
		return startTypeGuesses.contains(type);
	}

}
