package br.com.ideiasaleatorias.bolao.controllers.generators;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.Team;
import br.com.ideiasaleatorias.bolao.password.Password;
import br.com.ideiasaleatorias.bolao.scrappers.GloboTeamsList;

/**
 * Serve para cadastrar as coisas que não precisam de cruds
 * 
 * @author alberto
 *
 */
@Controller
@Transactional
public class CampeonatosETimesSetupController {

	@PersistenceContext
	private EntityManager manager;
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private SystemUserDao systemUserDao;

	@Autowired
	private GloboTeamsList globoTeamList;

	@RequestMapping("/magic/clubes/87jhfgsdkhfgkhj34hgkg")
	@ResponseBody
	public String setup() {
		Team[] clubes = new Team[] { new Team("Goiás", "GOI"), new Team("Athletico-PR", "CAP"),
				new Team("Atlético-MG", "CAM"), new Team("Palmeiras", "PAL"),
				new Team("Corinthians", "COR"), new Team("São Paulo", "SAO"),
				new Team("Santos", "SAN"), new Team("Ceará", "CEA"), new Team("Botafogo", "BOT"),
				new Team("Chapecoense", "CHA"), new Team("Cruzeiro", "CRU"),
				new Team("Fortaleza", "FOR"), new Team("Flamengo", "FLA"),
				new Team("Fluminense", "FLU"), new Team("Grêmio", "GRE"), new Team("Vasco", "VAS"),
				new Team("Internacional", "INT"), new Team("Bahia", "BAH"), new Team("CSA", "CSA"),
				new Team("Avaí", "AVA") };

		HashSet<Team> timesDoCampeonato = new HashSet<>();

		for (Team clube : clubes) {
			Optional<Team> clubeComNomeQueTalvezExista = teamDao.findByName(clube.getName());
			if (!clubeComNomeQueTalvezExista.isPresent()) {
				manager.persist(clube);
				timesDoCampeonato.add(clube);
			} else {
				Team team = clubeComNomeQueTalvezExista.get();
				team.changeSlug(clube.getSlug());
				timesDoCampeonato.add(team);
			}

		}

		Championship campeonato = new Championship("Brasileirão 2019",
				"/assets/campeonatos/brasileirao.png", LocalDateTime.of(2019, 4, 27, 16, 00),
				"brasileirao-2019");
		manager.persist(campeonato);

		for (Team clube : timesDoCampeonato) {
			campeonato.addTeam(clube);
		}

		return "clubes cadastrados";
	}

	@RequestMapping("/magic/worldcup/87jhfgsdkhfgkhj34hgkg")
	@ResponseBody
	public String setupWorldCup() {
		HashSet<Team> timesDoCampeonato = new HashSet<>();

		for (Team clube : globoTeamList.allTeams()) {
			Optional<Team> clubeQueTalvezExiste = teamDao.findBySlug(clube.getSlug());
			if (!clubeQueTalvezExiste.isPresent()) {
				manager.persist(clube);
				timesDoCampeonato.add(clube);
			} else {
				timesDoCampeonato.add(clubeQueTalvezExiste.get());
			}

		}

		Championship campeonato = new Championship("Copa do mundo 2018",
				"/assets/campeonatos/copa.png", LocalDateTime.of(2018, 6, 14, 12, 00), "copa-2018");
		manager.persist(campeonato);

		for (Team clube : timesDoCampeonato) {
			campeonato.addTeam(clube);
		}

		return "copa configurada";
	}

	@GetMapping("/magic/clubes/lfhdkh4uirthkfvfuir")
	@ResponseBody
	public String methodName(String slug, String primary, String second) {
		Team team = teamDao.findBySlug(slug).get();
		team.changeColors(primary, second);

		return "cor alterada com sucesso";
	}

	@GetMapping("/magic/gerasenhas/fdsjklfhdsjfhgkfdhglure843")
	@ResponseBody
	@Transactional
	public String generatePasswords() {
		Iterator<SystemUser> users = systemUserDao.findAll().iterator();
		users.forEachRemaining(user -> {
			user.changePassword(Password.buildWithRawText("vaipalmeiras"));
			user.setSecretPhrase(user.getEmail());
		});
		return "senhas alterads com sucesso";
	}

}
