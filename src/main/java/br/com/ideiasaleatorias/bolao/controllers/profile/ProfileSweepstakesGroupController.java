package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.SweepstakesGroupForm;
import br.com.ideiasaleatorias.bolao.controllers.forms.SweepstakesGroupInviteForm;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.StartGuessType;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.VerifyOwnershipService;
import br.com.ideiasaleatorias.bolao.services.actions.BecomeSweepstakesGroupMemberAction;
import br.com.ideiasaleatorias.bolao.services.actions.InviteAction;
import br.com.ideiasaleatorias.bolao.validations.AllEmailsAreValidValidator;
import br.com.ideiasaleatorias.bolao.validations.CannotInviteOwnerValidator;
import br.com.ideiasaleatorias.bolao.validations.EmailsNeededValidator;

@Controller
@Transactional
@RequestMapping("/profile/group/sweepstakes")
public class ProfileSweepstakesGroupController {

	@Autowired
	private SweepstakesGroupDao sweepstakesGroupDao;
	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private InviteAction inviteAction;
	@Autowired
	private BecomeSweepstakesGroupMemberAction becomeGroupMemberAction;

	@InitBinder
	public void init(WebDataBinder webDataBinder, @AuthenticationPrincipal SystemUser currentUser) {
		webDataBinder.addValidators(new AllEmailsAreValidValidator(),
				new CannotInviteOwnerValidator(currentUser),new EmailsNeededValidator());
	}

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String form(SweepstakesGroupForm form, Model model,@AuthenticationPrincipal SystemUser currentUser) {
		model.addAttribute("championshipList", championshipDao.findByActive(true));
		model.addAttribute("ownerGroups", sweepstakesGroupDao.findByOwnerId(currentUser.getId()));
		model.addAttribute("startGuessTypes",StartGuessType.values());
		return "profile/sweepstakes-group-form";
	}

	@RequestMapping(value = "/{groupId}/invite/form", method = RequestMethod.GET)
	public String formExtraInvite(@PathVariable("groupId") Integer groupId,
			SweepstakesGroupInviteForm form, Model model,@AuthenticationPrincipal SystemUser currentUser) {
		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);
		VerifyOwnershipService.userHasProperty(currentUser, group,"Você não é dono deste recurso");
		model.addAttribute("group", group);
		return "profile/sweepstakes-group-add-invite";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String create(@Valid SweepstakesGroupForm form, BindingResult bindingResult,
			Model model, RedirectAttributes redirectAttributes,
			@AuthenticationPrincipal SystemUser currentUser) {
		if (bindingResult.hasErrors()) {
			return form(form, model,currentUser);
		}

		SweepstakesGroup sweepstakesGroup = form.build(championshipDao,sweepstakesGroupDao,currentUser);
		sweepstakesGroupDao.save(sweepstakesGroup);

		becomeGroupMemberAction.execute(sweepstakesGroup, currentUser);
		inviteAction.execute(sweepstakesGroup);

		ViewMessages.successFlash("group criado e emails enviados", redirectAttributes);

		return "redirect:/profile/group/sweepstakes/form";
	}

	@RequestMapping(value = "/{groupId}/invite", method = RequestMethod.POST)
	public String extraInvites(@PathVariable("groupId") Integer groupId,
			@Valid SweepstakesGroupInviteForm form, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes, @AuthenticationPrincipal SystemUser currentUser) {
		if (bindingResult.hasErrors()) {
			return formExtraInvite(groupId, form, model,currentUser);
		}

		SweepstakesGroup sweepstakesGroup = sweepstakesGroupDao.findOne(groupId);
		VerifyOwnershipService.userHasProperty(currentUser, sweepstakesGroup,"Você não é dono deste recurso");
		
		//TODO aqui fora ou dentro do build?
		Set<GroupInvite> newInvites = form.build(sweepstakesGroup);
		sweepstakesGroup.addInvites(newInvites);

		inviteAction.execute(() -> {
			return newInvites;
		});

		ViewMessages.successFlash("convites enviados", redirectAttributes);

		return "redirect:/profile/group/sweepstakes/" + groupId + "/invite/form";
	}
}
