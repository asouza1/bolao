package br.com.ideiasaleatorias.bolao.controllers.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.GroupInviteDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
	
	@Autowired
	private GroupInviteDao groupInviteDao;
	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private SweepstakesGroupDao sweepstakesGroupDao;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public String index(Model model,@AuthenticationPrincipal SystemUser currentUser) {
		model.addAttribute("invites", groupInviteDao.findByEmailAndAccepted(currentUser.getEmail(), false));
		model.addAttribute("enrolledChampionships",championshipDao.findAllForUser(currentUser.getId()));
		model.addAttribute("groups",sweepstakesGroupDao.findAllGroupsForMember(currentUser.getId()));
		return "profile/dashboard";
	}
}
