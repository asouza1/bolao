package br.com.ideiasaleatorias.bolao.controllers.generators;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.daos.ActualGameResultDao;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.messages.Queues;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.views.GamesResults;

@Controller
@RequestMapping("/magic")
@Transactional
public class GenerateGameResultController {
	
	@Autowired
	private GameDao gameDao;
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private ActualGameResultDao actualGameResultDao;

	@RequestMapping(value="/score/klfhjdfsdkhfjkdshfjkd9589476",method=RequestMethod.POST)
	public String computeScore(Integer gameId,Integer principalScore,Integer visitingScore,RedirectAttributes redirectAttributes){
		jmsTemplate.send(Queues.RESULTS_QUEUE, (session) -> {			
			return session.createObjectMessage(new ActualGameResultMessage(gameId,principalScore,visitingScore));
		});
		
		ViewMessages.successFlash("O resultado está sendo processado", redirectAttributes);
		
		Game game = gameDao.findOne(gameId);
		return "redirect:/magic/score/dkljaslfjhdskfhsrioeuryiehfkjds?championshipId="+game.getChampionship().getId();
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/score/dkljaslfjhdskfhsrioeuryiehfkjds")
	public String form(Model model,@RequestParam Integer championshipId) {
		model.addAttribute("gamesResults",new GamesResults(actualGameResultDao.findAllByGameChampionshipId(championshipId)));
		model.addAttribute("games", gameDao.findAllByChampionshipIdOrderByGameTimeAsc(championshipId));
		return "games/results";
	}
}
