package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.Team;

public class StartGuessForm {

	@NotNull
	private Integer championshipId;
	@NotNull
	private Integer bestDefenseTeamId;
	@NotNull
	private Integer bestStrikeTeamId;
	@NotNull
	private Integer championTeamId;
	@NotNull
	private Integer worstTeamId;
	@Size(min = 4, max = 4, message = "Escolha 4 times para o g4")
	private Set<Integer> g4TeamsIds = new LinkedHashSet<Integer>();
	@Size(min = 4, max = 4, message = "Escolha 4 times para o z4")
	private Set<Integer> z4TeamsIds = new LinkedHashSet<Integer>();
	@NotNull
	private Integer viceChampionTeamId;
	@NotBlank
	private String topScorer;

	public Integer getViceChampionTeamId() {
		return viceChampionTeamId;
	}

	public void setViceChampionTeamId(Integer viceChampionTeamId) {
		this.viceChampionTeamId = viceChampionTeamId;
	}

	public String getTopScorer() {
		return topScorer;
	}

	public void setTopScorer(String topScorer) {
		this.topScorer = topScorer;
	}

	public Integer getChampionshipId() {
		return championshipId;
	}

	public void setChampionshipId(Integer championshipId) {
		this.championshipId = championshipId;
	}

	public Integer getBestDefenseTeamId() {
		return bestDefenseTeamId;
	}

	public void setBestDefenseTeamId(Integer bestDefenseTeamId) {
		this.bestDefenseTeamId = bestDefenseTeamId;
	}

	public Integer getBestStrikeTeamId() {
		return bestStrikeTeamId;
	}

	public void setBestStrikeTeamId(Integer bestStrikeTeamId) {
		this.bestStrikeTeamId = bestStrikeTeamId;
	}

	public Integer getChampionTeamId() {
		return championTeamId;
	}

	public void setChampionTeamId(Integer championTeamId) {
		this.championTeamId = championTeamId;
	}

	public Integer getWorstTeamId() {
		return worstTeamId;
	}

	public void setWorstTeamId(Integer worstTeamId) {
		this.worstTeamId = worstTeamId;
	}

	public Set<Integer> getG4TeamsIds() {
		return g4TeamsIds;
	}

	public void setG4TeamsIds(Set<Integer> g4TeamsIds) {
		this.g4TeamsIds = g4TeamsIds;
	}

	public Set<Integer> getZ4TeamsIds() {
		return z4TeamsIds;
	}

	public void setZ4TeamsIds(Set<Integer> z4TeamsIds) {
		this.z4TeamsIds = z4TeamsIds;
	}

	public SweepstakesStartGuess build(ChampionshipDao championshipDao, TeamDao teamDao,
			SystemUser currentUser) {
		Iterable<Team> g4Teams = teamDao.findAll(g4TeamsIds);
		Iterable<Team> z4Teams = teamDao.findAll(z4TeamsIds);

		return new SweepstakesStartGuess(teamDao.findOne(bestDefenseTeamId),
				teamDao.findOne(bestStrikeTeamId), teamDao.findOne(championTeamId),
				teamDao.findOne(worstTeamId), teamDao.findOne(viceChampionTeamId), topScorer,
				g4Teams, z4Teams, championshipDao.findOne(championshipId), currentUser);
	}

	public void fillWithGuesses(SweepstakesStartGuess foundGuess) {
		this.bestDefenseTeamId = foundGuess.getBestDefenseTeam().getId();
		this.bestStrikeTeamId = foundGuess.getBestStrikeTeam().getId();
		this.championTeamId = foundGuess.getChampionTeam().getId();
		this.worstTeamId = foundGuess.getWorstTeam().getId();
		this.championshipId = foundGuess.getChampionship().getId();
		foundGuess.getG4Teams().forEach(team -> this.g4TeamsIds.add(team.getId()));
		foundGuess.getZ4Teams().forEach(team -> this.z4TeamsIds.add(team.getId()));
		this.viceChampionTeamId = foundGuess.getViceChampionTeam().getId();
		this.topScorer = foundGuess.getTopScorer();
	}

}
