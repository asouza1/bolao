package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.StartGuessType;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

public class SweepstakesGroupForm implements HasEmailsList {

	@NotBlank
	private String name;
	private String emails;
	@NotNull
	private Integer idChampionship;
	private Integer idOwnerGroup;
	@Size(min=1)
	private List<StartGuessType> startGuessTypes = new ArrayList<>();

	public List<StartGuessType> getStartGuessTypes() {
		return startGuessTypes;
	}

	public void setStartGuessTypes(List<StartGuessType> guessTypes) {
		this.startGuessTypes = guessTypes;
	}

	public boolean preferToImportCreatedGroupd() {
		return idOwnerGroup != null;
	}

	public Integer getIdOwnerGroup() {
		return idOwnerGroup;
	}

	public void setIdOwnerGroup(Integer idOwnerGroup) {
		this.idOwnerGroup = idOwnerGroup;
	}

	public Integer getIdChampionship() {
		return idChampionship;
	}

	public void setIdChampionship(Integer idChampionship) {
		this.idChampionship = idChampionship;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public SweepstakesGroup build(ChampionshipDao championshipDao,
			SweepstakesGroupDao sweepstakesGroupDao, SystemUser currentUser) {
		Championship championship = championshipDao.findOne(idChampionship);
		SweepstakesGroup group = new SweepstakesGroup(name, championship, currentUser,startGuessTypes);
		Set<GroupInvite> invites = GroupInviteBuilder.build(this, group);
		group.addInvites(invites);

		if (idOwnerGroup != null) {
			group.addInvites(sweepstakesGroupDao.findOne(idOwnerGroup).copyInvites(group));
		}

		return group;
	}

}
