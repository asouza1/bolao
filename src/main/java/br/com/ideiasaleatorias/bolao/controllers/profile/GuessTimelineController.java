package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ideiasaleatorias.bolao.daos.ActualGameResultDao;
import br.com.ideiasaleatorias.bolao.daos.CalculatedRoundScoreDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;
import br.com.ideiasaleatorias.bolao.views.GameResultsView;
import br.com.ideiasaleatorias.bolao.views.Guesses;
import br.com.ideiasaleatorias.bolao.views.ScoreProportionsView;
import br.com.ideiasaleatorias.bolao.views.ScoresView;

@Controller
public class GuessTimelineController {

	@Autowired
	private GameGuessDao gameGuessDao;
	@Autowired
	private ActualGameResultDao actualGameResultDao;
	@Autowired
	private CalculatedRoundScoreDao calculatedRoundScoreDao;

	@RequestMapping(method = RequestMethod.GET, value = "/guess/timeline")
	public String methodName(Model model, int round, Integer championshipId,
			@AuthenticationPrincipal SystemUser currentUser) {
		ScoresView scoresView = new ScoresView(calculatedRoundScoreDao.findAllByChampionshipAndRound(
				championshipId, round));
		
		List<GameGuess> guesses = gameGuessDao.findByChampionshipAndRound(championshipId,
				currentUser.getId(), round);

		model.addAttribute("gameGuessList", new Guesses(guesses,scoresView));
		List<ActualGameResult> resultsOfGames = actualGameResultDao.findAllByChampionshipAndRound(
				championshipId, round);
		model.addAttribute("results", new GameResultsView(resultsOfGames));

		List<AveragePossibleGameResult> possibleResults = gameGuessDao.averagePossibleResults(
				championshipId, round);
		model.addAttribute("proportions", new ScoreProportionsView(possibleResults));
		model.addAttribute("scores",scoresView);
		return "profile/guesses-timeline";
	}
}
