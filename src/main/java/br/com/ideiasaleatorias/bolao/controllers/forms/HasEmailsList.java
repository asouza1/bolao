package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.HashSet;
import java.util.Set;

import org.springframework.util.StringUtils;

public interface HasEmailsList {

	public default Set<String> getSplittedEmails(){
		String emailsSeparatedByComma = getEmails();
		HashSet<String> result = new HashSet<>();
		if(!StringUtils.hasText(emailsSeparatedByComma)){
			return result;
		}
			
		String[] emailsSplitted = emailsSeparatedByComma.split(",");
		
		for (String email : emailsSplitted) {
			result.add(email.toLowerCase());
		}
		
		return result;
	}
	
	/**
	 * 
	 * @return emails separados por ,
	 */
	String getEmails();
}
