package br.com.ideiasaleatorias.bolao.controllers.forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

import br.com.ideiasaleatorias.bolao.models.Role;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.password.Password;

public class SimpleUserForm {

	@NotBlank
	private String name;
	@NotBlank
	@Email
	private String email;
	@NotBlank
	@Length(min = 6, message = "Sua senha deve ter no mínimo 6 caracteres")
	private String rawPassword;
	@NotBlank
	private String secretPhrase;
	private String shareCode;
	
	public String getSecretPhrase() {
		return secretPhrase;
	}

	public void setSecretPhrase(String secretPhrase) {
		this.secretPhrase = secretPhrase;
	}

	public String getShareCode() {
		return shareCode;
	}

	public void setShareCode(String shareCode) {
		this.shareCode = shareCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		if (StringUtils.hasText(email)) {
			this.email = email.toLowerCase();
		} else {
			this.email = email;
		}
	}

	public String getRawPassword() {
		return rawPassword;
	}

	public void setRawPassword(String rawPassword) {
		this.rawPassword = rawPassword;
	}

	public SystemUser build(Role... roles) {
		Password password = Password.buildWithRawText(rawPassword);
		return new SystemUser(name, email, password,secretPhrase,roles);
	}

	public boolean hasShareCode() {
		return StringUtils.hasText(shareCode);
	}

}
