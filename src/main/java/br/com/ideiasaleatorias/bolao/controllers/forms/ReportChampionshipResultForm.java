package br.com.ideiasaleatorias.bolao.controllers.forms;

public class ReportChampionshipResultForm {

	private Integer championshipId;
	private Integer teamId;

	public Integer getChampionshipId() {
		return championshipId;
	}

	public void setChampionshipId(Integer championshipId) {
		this.championshipId = championshipId;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
	
	public boolean hasChampionship(){
		return championshipId!=null;
	}

}
