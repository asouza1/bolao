package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.HashSet;
import java.util.Set;

import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;

public class GroupInviteBuilder {

	/**
	 * 
	 * @param emails emails separados por ,
	 * @return
	 */
	public static Set<GroupInvite> build(HasEmailsList emailsList,SweepstakesGroup sweepstakesGroup) {
		Set<GroupInvite> invites = new HashSet<GroupInvite>();
		for (String email : emailsList.getSplittedEmails()) {
			invites.add(new GroupInvite(email,sweepstakesGroup));
		}			
		return invites;
	}

}
