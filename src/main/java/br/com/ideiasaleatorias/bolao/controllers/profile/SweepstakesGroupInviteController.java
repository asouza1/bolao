package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.daos.GroupInviteDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.VerifyOwnershipService;
import br.com.ideiasaleatorias.bolao.services.actions.BecomeSweepstakesGroupMemberAction;

@Controller
public class SweepstakesGroupInviteController {
	
	public static final String COOKIE_INVITATION_ID = "invitation-id";
	@Autowired
	private GroupInviteDao groupInviteDao;
	@Autowired
	private BecomeSweepstakesGroupMemberAction acceptSweepstakesPendingInviteAction;	

	@RequestMapping(method=RequestMethod.GET,value="sweepstakes/invite/{uuid}")
	public String acceptInviteForm(Model model,@PathVariable("uuid") String uuid, HttpServletResponse response){
		GroupInvite groupInvite = groupInviteDao.findByUuid(uuid);		
		model.addAttribute("groupInvite",groupInvite);
		return "sweepstakes/accept-invite-form";
	}
	
	@RequestMapping(method=RequestMethod.GET,value="sweepstakes/invite/pending")
	public String pendingInvites(Model model,@AuthenticationPrincipal SystemUser currentUser){
		List<GroupInvite> pendingInvites = groupInviteDao.findByEmailAndAccepted(currentUser.getEmail(),false);
		model.addAttribute("pendingGroupInviteList", pendingInvites);
		return "profile/pending-invite";
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/sweepstakes/invite/{pendingUuid}/accept")
	@Transactional
	public String acceptPendingInvite(@PathVariable String pendingUuid,Model model,@AuthenticationPrincipal SystemUser currentUser, RedirectAttributes redirectAttributes){
		GroupInvite pendingInvite = groupInviteDao.findByUuid(pendingUuid);
		
		VerifyOwnershipService.userHasProperty(currentUser,pendingInvite, "Esse convite não parece ser seu. Acessou o link errado? Será que o email está correto?");
		
		acceptSweepstakesPendingInviteAction.execute(pendingInvite,currentUser);
		
		ViewMessages.successFlash("você agora faz parte do grupo", redirectAttributes);
		return "redirect:/dashboard";
	}
}
