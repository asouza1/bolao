package br.com.ideiasaleatorias.bolao.controllers.forms;

import javax.validation.constraints.NotNull;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

public class SweepstakesMemberForm {

	@NotNull
	private Integer championshipId;

	public Integer getChampionshipId() {
		return championshipId;
	}

	public void setChampionshipId(Integer championshipId) {
		this.championshipId = championshipId;
	}

	public SweepstakesMember build(ChampionshipDao championshipDao, SystemUser currentUser) {
		return new SweepstakesMember(championshipDao.findOne(championshipId), currentUser);
	}
	
	
}
