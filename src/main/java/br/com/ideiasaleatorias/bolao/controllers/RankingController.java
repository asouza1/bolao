package br.com.ideiasaleatorias.bolao.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.ideiasaleatorias.bolao.controllers.forms.StartGuessForm;
import br.com.ideiasaleatorias.bolao.daos.ActualGameResultDao;
import br.com.ideiasaleatorias.bolao.daos.CalculatedRoundScoreDao;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.RankingViewEntryDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesStartGuessDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.exceptions.NotAuthorizedException;
import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.RoundScore;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.actions.ProjectionAcion;

@Controller
@RequestMapping("/ranking")
public class RankingController {

	@Autowired
	private RankingViewEntryDao rankingViewEntryDao;
	@Autowired
	private ActualGameResultDao actualGameResultDao;
	@Autowired
	private CalculatedRoundScoreDao calculatedRoundScoreDao;
	@Autowired
	private SweepstakesGroupDao sweepstakesGroupDao;
	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private SweepstakesStartGuessDao sweepstakesStartGuessDao;
	@Autowired
	private ProjectionAcion projectionAction;

	@RequestMapping(value = "/group/{groupId}", method = RequestMethod.GET)
	public String byGroup(@PathVariable("groupId") Integer groupId, Model model) {
		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);
		model.addAttribute("entries", rankingViewEntryDao.findByGroupIdAndGroupChampionshipId(group.getId(),group.getChampionship().getId()));
		model.addAttribute("lastResult", actualGameResultDao.findTopByGameChampionshipIdOrderByGameRoundDesc(group.getChampionship().getId()));
		
		return "ranking/byGroup";
	}

	@RequestMapping(value = "/championship/{championshipId}", method = RequestMethod.GET)
	public String byChampionship(@PathVariable("championshipId") Integer championshipId, Model model) {
		model.addAttribute("entries",
				rankingViewEntryDao.findByChampionshipIdOrderByAcumulatedScoreDesc(championshipId));
		model.addAttribute("lastResult", actualGameResultDao.findTopByGameChampionshipIdOrderByGameRoundDesc(championshipId));
		return "ranking/byChampionship";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/by/round")
	public String byRound(@PathVariable("groupId") Integer groupId,
			@RequestParam(required = false) Integer round, Model model,
			@AuthenticationPrincipal SystemUser currentUser) {

		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);

		if (!group.accepts(currentUser)) {
			throw new NotAuthorizedException("Você não pertence a esse grupo");
		}

		if (round == null) {
			//TODO isso aqui é regra de negocio, precisa sair daqui.
			Optional<ActualGameResult> result = actualGameResultDao
			.findTopByGameChampionshipIdOrderByGameRoundDesc(
					group.getChampionship().getId());			
			round = result.isPresent() ? result.get().getGame().getRound() : 1;
		}

		List<RoundScore> scores = calculatedRoundScoreDao.listRoundScoreOrderByDesc(round,
				group.getId(),group.getChampionship().getId());

		model.addAttribute("group", group);
		model.addAttribute("entries", scores);
		model.addAttribute("round", round);

		return "ranking/byRound";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/projection/form")
	public String projectionForm(@PathVariable("groupId") Integer groupId, StartGuessForm form,
			Model model) {
		model.addAttribute("championshipList", championshipDao.findByActive(true));
		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);
		model.addAttribute("teamList",
				teamDao.findAllByChampionshipOrderByNameAsc(group.getChampionship().getId()));
		return "ranking/projection";
	}

	@RequestMapping(method = RequestMethod.GET, value = "/group/{groupId}/projection")
	public String projection(@PathVariable("groupId") Integer groupId, @Valid StartGuessForm form,
			BindingResult bindingResult, Model model,
			@AuthenticationPrincipal SystemUser currentUser) {

		if (bindingResult.hasErrors()) {
			return projectionForm(groupId, form, model);
		}


		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);
		SweepstakesStartGuess guessToCompare = form.build(championshipDao, teamDao, currentUser);
				
		model.addAttribute("entries", projectionAction.execute(group,guessToCompare));
		return projectionForm(groupId, form, model);
	}
}
