package br.com.ideiasaleatorias.bolao.controllers.profile;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.Team;
import br.com.ideiasaleatorias.bolao.services.AppSecurityContext;
import br.com.ideiasaleatorias.bolao.services.actions.ChangeTeam;

@Controller
@Transactional
public class ProfileTeamController {
	
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private SystemUserDao userDao;
	@Autowired
	private AppSecurityContext securityContext;
	@Autowired
	private ChangeTeam changeTeam;

	@RequestMapping(method=RequestMethod.GET,value="/profile/choose/team")
	public String list(Model model){
		model.addAttribute("teamList",teamDao.findAllOrderByNameAsc());
		return "profile/my-club";
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/profile/choose/team/{slug}")
	public String choose(@PathVariable("slug") String slug,@AuthenticationPrincipal SystemUser currentUser, RedirectAttributes redirectAttributes){
		Team team = teamDao.findBySlug(slug).get();
		
		SystemUser updatedUser = changeTeam.execute(currentUser.getId(),team);
		
		securityContext.changeCurrentUser(updatedUser);
		
		
		ViewMessages.successFlash("time do coração escolhido", redirectAttributes);
		return "redirect:/profile/choose/team";
	}
}
