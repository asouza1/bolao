package br.com.ideiasaleatorias.bolao.controllers.generators;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;

@Controller
public class GenerateShareCodeForGroups {
	
	@Autowired
	private SweepstakesGroupDao groupDao;
	@PersistenceContext
	private EntityManager em;

	@GetMapping("/magic/jkfhsdjkfhwkuerffds")
	@Transactional
	@ResponseBody
	public String generateShareCode() {
		Iterable<SweepstakesGroup> groups = groupDao.findAll();
		groups.forEach(group -> {
			em.createQuery("update SweepstakesGroup sg set sg.shareCode = :shareCode where sg.id = :id")
			.setParameter("shareCode", UUID.randomUUID().toString())
			.setParameter("id", group.getId())
			.executeUpdate();
		});
		return "codigos de compartilhamento atualizados";
	}
}
