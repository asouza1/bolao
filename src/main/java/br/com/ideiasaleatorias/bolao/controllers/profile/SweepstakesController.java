package br.com.ideiasaleatorias.bolao.controllers.profile;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.SweepstakesMemberForm;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Controller
@RequestMapping("/championship")
@Transactional
public class SweepstakesController {

	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private SweepstakesMemberDao sweepstakesMemberDao; 

	@RequestMapping(method = RequestMethod.GET, value = "/join/form")
	public String joinForm(Model model, SweepstakesMemberForm member,@AuthenticationPrincipal SystemUser currentUser) {
		model.addAttribute("championshipList", championshipDao.findAllThatUserIsNotMember(currentUser.getId()));
		return "championship/join";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/join")
	public String join(Model model, @Valid SweepstakesMemberForm memberForm,
			BindingResult bindingResult, @AuthenticationPrincipal SystemUser currentUser, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return joinForm(model, memberForm,currentUser);
		}

		//TODO validar?
		SweepstakesMember member = memberForm.build(championshipDao,currentUser);
		sweepstakesMemberDao.save(member);
		
		ViewMessages.successFlash("Você agora participa do bolão do "
				+ member.getChampionship().getName(), redirectAttributes);
		return "redirect:/championship/join/form";
	}

	
	
}
