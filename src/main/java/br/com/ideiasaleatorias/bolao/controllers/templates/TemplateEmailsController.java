package br.com.ideiasaleatorias.bolao.controllers.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ideiasaleatorias.bolao.daos.GroupInviteDao;

/**
 * Controller que gera os templates de email. É aceitável um certo nível de falta coesão nessa classe.
 * @author alberto
 *
 */
@Controller
public class TemplateEmailsController {

	@Autowired
	private GroupInviteDao groupInviteDao;
	
	@RequestMapping(value="/templates/email/klfhsdkfhjk4rufkerurgkf3ugfy/{uuid}",name="emailGroupInvite",method=RequestMethod.GET)
	public String novaCompra(@PathVariable("uuid") String uuid,Model model){
		model.addAttribute("acceptanceLink","/sweepstakes/invite/"+uuid);
		return "emails/new-group-invite";
	}
}
