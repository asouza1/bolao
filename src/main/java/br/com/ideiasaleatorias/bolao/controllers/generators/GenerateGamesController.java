package br.com.ideiasaleatorias.bolao.controllers.generators;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.models.Game;

@Controller
public class GenerateGamesController {
	
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private GameDao gameDao;

	
	@RequestMapping("/magic/games/lfjdjfhiohuhueirgert")
	public String form(GameForm gameForm,Model model){
		model.addAttribute("teamList", teamDao.findAllOrderByNameAsc());
		model.addAttribute("championshipList", championshipDao.findByActive(true));
		return "magic/game-form";
	}
	
	@RequestMapping("/magic/games/fkldjhfsjhdfd9845kgjfdf")
	@ResponseBody
	public String save(@Valid GameForm gameForm){
		//nao fiz validacao de negocio pq é uma tela mágica. Talvez precise
		Game game = gameForm.build(teamDao,championshipDao);
		gameDao.save(game);
		return "jogo salvo "+game;
	}
}
