package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.controllers.forms.GameGuessForm;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDaoConcrete;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.dtos.AveragePossibleGameResultsNormalizer;
import br.com.ideiasaleatorias.bolao.infra.AjaxResponse;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;
import br.com.ideiasaleatorias.bolao.services.actions.LastGuessesAction;
import br.com.ideiasaleatorias.bolao.validations.GuessBeforeGame;
import br.com.ideiasaleatorias.bolao.validations.OnlyOneDoubleGuessPerRound;

@Controller
public class GameGuessController {

	@Autowired
	private GameDao gameDao;
	@Autowired
	private GameGuessDaoConcrete gameGuessDao;
	@Autowired
	private SweepstakesMemberDao memberDao;
	@Autowired
	private LastGuessesAction lastGuessesAction;
	@Autowired
	private ChampionshipDao championshipDao;

	@InitBinder
	public void init(WebDataBinder binder, @AuthenticationPrincipal SystemUser currentUser) {
		binder.addValidators(new OnlyOneDoubleGuessPerRound(currentUser, gameGuessDao, gameDao),
				new GuessBeforeGame(gameDao));
	}

	@RequestMapping(value = "/guess/{championshipId}/games/form", method = RequestMethod.GET)
	public String form(@PathVariable("championshipId") Integer championshipId, Model model,
			GameGuessForm form, @AuthenticationPrincipal SystemUser currentUser) {

		model.addAttribute("gameGuessList",
				lastGuessesAction.load(championshipId, currentUser.getId(),LocalDateTime.now()));
		
		return "games/next-games";
	}
	
	@RequestMapping(value = "/guess/done/{championshipId}/games", method = RequestMethod.GET)
	public String doneGuesses(@PathVariable("championshipId") Integer championshipId, Model model,
			GameGuessForm form, @AuthenticationPrincipal SystemUser currentUser) {
		
		Championship championship = championshipDao.findOne(championshipId);
		
		model.addAttribute("gameGuessList",
				lastGuessesAction.load(championshipId, currentUser.getId(),championship.getStartTime()));
		
		return "games/all-guesses";
	}

	@RequestMapping(value = "/guess/games", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AjaxResponse> save(@Valid GameGuessForm form,
			BindingResult bindingResult, @AuthenticationPrincipal SystemUser currentUser) throws InterruptedException {
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<AjaxResponse>(new AjaxResponse(bindingResult),
					HttpStatus.BAD_REQUEST);
		}

		Optional<GameGuess> foundGuess = gameGuessDao.findByGameIdAndMemberSystemUserId(
				form.getGameId(), currentUser.getId());		

		GameGuess newGuess = form.build(gameDao, memberDao, currentUser,foundGuess);

		gameGuessDao.save(newGuess);
		return new ResponseEntity<AjaxResponse>(new AjaxResponse("Palpite registrado com sucesso"),
				HttpStatus.OK);
	}
	
	@RequestMapping(value = "/magic/jkfhdkjfhsdfdsfds48f/guess/games", produces = MediaType.APPLICATION_JSON_VALUE)
	/**
	 * hack para palpitar fora de horário para casos bizarros.
	 * @param form
	 * @param bindingResult
	 * @param currentUser
	 * @return
	 * @throws InterruptedException
	 */
	public ResponseEntity<AjaxResponse> forceGuess(GameGuessForm form,
			BindingResult bindingResult, @AuthenticationPrincipal SystemUser currentUser) throws InterruptedException {
		return save(form, bindingResult, currentUser);
	}	
	
	@RequestMapping(value = "/guess/game/{gameId}/averages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<AveragePossibleGameResult> averages(@PathVariable("gameId") Integer gameId) {
		return AveragePossibleGameResultsNormalizer.normalize(gameDao.findOne(gameId),gameGuessDao.averagePossibleGameResults(gameId));
	}
}
