package br.com.ideiasaleatorias.bolao.controllers.ajax;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.infra.ChainedSelect;
import br.com.ideiasaleatorias.bolao.models.Team;

/**
 * Tudo que for carregado de ajax em função de um campeonato, pode ficar aqui
 * 
 * @author alberto
 *
 */

@Controller
public class AjaxChampionshipController {
	
	@Autowired
	private TeamDao teamDao;

	@RequestMapping(value="/ajax/championship/teams",produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> servicos(Integer id){
		if(id==null){
			return new ResponseEntity<String>("msg",HttpStatus.BAD_REQUEST);
		}
		
		List<Team> teams = teamDao.findAllByChampionshipOrderByNameAsc(id);
		return new ResponseEntity<String>(ChainedSelect.json(teams,"id","name"), HttpStatus.OK);
	}
}
