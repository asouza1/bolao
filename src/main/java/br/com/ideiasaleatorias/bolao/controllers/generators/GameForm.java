package br.com.ideiasaleatorias.bolao.controllers.generators;

import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.Team;

public class GameForm {

	@NotNull
	private Integer idPrincipalTeam;
	@NotNull
	private Integer idVisitingTeam;
	@NotNull
	@DateTimeFormat(pattern="dd/MM/yyyy kk:mm")
	private LocalDateTime gameTime;
	@NotNull
	private Integer idChampionship;
	@NotBlank
	private String site;
	@NotNull
	@Min(1)
	private Integer round;

	public Integer getIdPrincipalTeam() {
		return idPrincipalTeam;
	}

	public void setIdPrincipalTeam(Integer idPrincipalTeam) {
		this.idPrincipalTeam = idPrincipalTeam;
	}

	public Integer getIdVisitingTeam() {
		return idVisitingTeam;
	}

	public void setIdVisitingTeam(Integer idVisitingTeam) {
		this.idVisitingTeam = idVisitingTeam;
	}

	public LocalDateTime getGameTime() {
		return gameTime;
	}

	public void setGameTime(LocalDateTime gameTime) {
		this.gameTime = gameTime;
	}

	public Integer getIdChampionship() {
		return idChampionship;
	}

	public void setIdChampionship(Integer idChampionship) {
		this.idChampionship = idChampionship;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}
	
	public Integer getRound() {
		return round;
	}
	
	public void setRound(Integer round) {
		this.round = round;
	}

	public Game build(TeamDao teamDao, ChampionshipDao championshipDao) {
		Team principalTeam = teamDao.findOne(idPrincipalTeam);
		Team visitingTeam = teamDao.findOne(idVisitingTeam);
		Championship championship = championshipDao.findOne(idChampionship);
		return new Game(principalTeam, visitingTeam, gameTime, championship, site,round);
	}

	

}
