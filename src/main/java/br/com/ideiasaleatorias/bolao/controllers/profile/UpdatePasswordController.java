package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.UpdatePasswordForm;
import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.exceptions.NotFoundException;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.password.Password;

@Controller
public class UpdatePasswordController {

	@Autowired
	private SystemUserDao systemUserDao;

	@RequestMapping(method = RequestMethod.GET, value = "/password/update/form")
	public String form(UpdatePasswordForm form,@AuthenticationPrincipal Object user) {
		if(user instanceof SystemUser){
			SystemUser currentUser = (SystemUser) user;
			form.setEmail(currentUser.getEmail());
		}
		return "profile/update-password";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/password/update")
	@Transactional
	public String methodName(Model model, @Valid UpdatePasswordForm form,
			BindingResult bindingResult, RedirectAttributes redirectAttributes,@AuthenticationPrincipal Object currentUser) {
		if (bindingResult.hasErrors()) {
			return form(form,currentUser);
		}

		Optional<SystemUser> user = systemUserDao.findBySecretPhraseAndEmail(
				form.getSecretPhrase(), form.getEmail());

		if (!user.isPresent()) {
			bindingResult.rejectValue("email", "updatePasswordForm.user_not_exists",
					"Não achamos o usuário com este email e frase secreta");
			return form(form,currentUser);
		}

		user.get().changePassword(Password.buildWithRawText(form.getRawPassword()));

		ViewMessages.successFlash("Senha atualizada com sucesso", redirectAttributes);
		return "redirect:/password/update/form";
	}
}
