package br.com.ideiasaleatorias.bolao.controllers.generators;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.models.Role;

@Controller
@Transactional
public class GenerateSetupController {
	
	@PersistenceContext
	private EntityManager entityManager;

	@RequestMapping("/magic/generate/roles/dfhjskhgfsjdkgfjdkshgjfdjkdfkghjkfdhgjkf")
	@ResponseBody
	public String generateRoles(){
		entityManager.persist(Role.ADMIN);
		entityManager.persist(Role.PARTICIPANTE);
		return "gerado filhão";
	}
}
