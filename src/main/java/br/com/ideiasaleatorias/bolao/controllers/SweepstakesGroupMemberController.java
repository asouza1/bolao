package br.com.ideiasaleatorias.bolao.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupMemberDao;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Controller
public class SweepstakesGroupMemberController {
	
	@Autowired
	private SweepstakesGroupMemberDao sweepstakesGroupMemberDao;
	@Autowired
	private SweepstakesGroupDao sweepstakesGroupDao;

	@RequestMapping(method = RequestMethod.GET, value = "/sweepstakes/group/{groupId}/members")
	public String methodName(@PathVariable Integer groupId, Model model,@AuthenticationPrincipal SystemUser currentUser) {
		SweepstakesGroup group = sweepstakesGroupDao.findOne(groupId);
		if(!group.accepts(currentUser)){
			throw new RuntimeException("Acho que vc não pertence a esse grupo");
		}
		
		model.addAttribute("group",group);		
		model.addAttribute("members",sweepstakesGroupMemberDao.findAllByGroupId(groupId));
		return "sweepstakesGroupMember/list";
	}
}
