package br.com.ideiasaleatorias.bolao.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.ReportChampionshipResultForm;
import br.com.ideiasaleatorias.bolao.daos.ActualGameResultDao;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.exceptions.NotFoundException;
import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.Team;
import br.com.ideiasaleatorias.bolao.models.reports.ReportChampionshipResultView;

@Controller
public class ReportController {

	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private ActualGameResultDao actualGameResultDao;
	@Autowired
	private GameDao gameDao;
	@Autowired
	private GameGuessDao gameGuessDao;
	
	@GetMapping("/report/championship/results/form")
	public String resultsForm(Model model, ReportChampionshipResultForm form) {
		model.addAttribute("championshipList", championshipDao.findAll());
		if (form.hasChampionship()) {
			model.addAttribute("teamList",
					teamDao.findAllByChampionshipOrderByNameAsc(form.getChampionshipId()));
		}
		return "/reports/championship-results";
	}

	@GetMapping("/report/championship/results")
	public String resultByTeam(Model model, @Valid ReportChampionshipResultForm form,
			BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return resultsForm(model, form);
		}

		List<ActualGameResult> results = actualGameResultDao
				.findAllByChampionshipAndTeam(form.getChampionshipId(), form.getTeamId());
		Team team = teamDao.findOne(form.getTeamId());
		model.addAttribute("resultList",
				results.stream().map(result -> {
					return new ReportChampionshipResultView(result,team);
				}).collect(Collectors.toList()));

		return resultsForm(model, form);
	}
	
	@GetMapping("/report/guesses/game/{gameId}")
	public String guessePerGame(Model model, @PathVariable("gameId") Integer gameId) {
		Game game = gameDao.findOne(gameId);
		if(game.allowGuess()) {
			throw new NotFoundException("Você procura por algo que ainda não está disponível :P");
		}
		
		model.addAttribute("guesses",gameGuessDao.findAllByGameId(game.getId()));
		model.addAttribute("game", game);
		
		return "/reports/guesses-per-game";
	}
}
