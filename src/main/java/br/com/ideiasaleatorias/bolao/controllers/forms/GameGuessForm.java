package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.Optional;
import java.util.function.Supplier;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import br.com.ideiasaleatorias.bolao.builders.GameGuessBuilder;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

public class GameGuessForm {

	@NotNull
	@Min(0)
	private Integer principalScore;
	@Min(0)
	@NotNull
	private Integer visitingScore;
	private boolean doubled;
	@NotNull
	private Integer gameId;
	
	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}
	
	public Integer getGameId() {
		return gameId;
	}

	public Integer getPrincipalScore() {
		return principalScore;
	}

	public void setPrincipalScore(Integer principalScoreId) {
		this.principalScore = principalScoreId;
	}

	public Integer getVisitingScore() {
		return visitingScore;
	}

	public void setVisitingScore(Integer visitingScoreId) {
		this.visitingScore = visitingScoreId;
	}

	public boolean isDoubled() {
		return doubled;
	}

	public void setDoubled(boolean doubled) {
		this.doubled = doubled;
	}

	/**
	 * 
	 * @param gameDao
	 * @param memberDao
	 * @param currentUser
	 * @param guessSupplier parametro que indica se já existe um palpite para o jogo em questão
	 * @return
	 */
	public GameGuess build(GameDao gameDao,SweepstakesMemberDao memberDao,SystemUser currentUser,Optional<GameGuess> guessSupplier) {
		Game game = gameDao.findOne(gameId);
		GameGuess gameGuess = GameGuessBuilder.member(memberDao,currentUser.getId(),game.getChampionship().getId()).
				build(game, principalScore, visitingScore, doubled);
		if(guessSupplier.isPresent()){
			GameGuess existingGuess = guessSupplier.get();
			gameGuess.setId(existingGuess.getId());
		}
		return gameGuess;					
	}

}
