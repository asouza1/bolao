package br.com.ideiasaleatorias.bolao.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.SimpleUserForm;
import br.com.ideiasaleatorias.bolao.daos.RoleDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.Role;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.actions.BecomeSweepstakesGroupMemberAction;
import br.com.ideiasaleatorias.bolao.validations.SingleEmailValidator;

@Controller
@RequestMapping("/users")
@Transactional
public class SimpleUserController {
	
	@Autowired
	private SystemUserDao userDao;
	@Autowired
	private RoleDao roleDao;
	@Autowired
	private BecomeSweepstakesGroupMemberAction memberAction;
	@Autowired
	private SweepstakesGroupDao groupDao;
	
	@InitBinder
	public void init(WebDataBinder binder){
		binder.addValidators(new SingleEmailValidator(userDao));
	}

	@RequestMapping(value="/form",method=RequestMethod.GET)
	public String form(SimpleUserForm form){
		System.out.println("lkfjdlfjsd");
		return "users/form";
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public String save(Model model, @Valid SimpleUserForm form, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		if (bindingResult.hasErrors()) {
			return form(form);
		}
		
		SystemUser newUser = form.build(roleDao.findByName(Role.PARTICIPANTE.getName()));
		userDao.save(newUser);
		
		if(form.hasShareCode()){
			memberAction.execute(groupDao.findByShareCode(form.getShareCode()), newUser);
		}
		
		ViewMessages.successFlash("Usuário criado com sucesso. Agora faça o login!", redirectAttributes);
		return "redirect:/";
	}
}
