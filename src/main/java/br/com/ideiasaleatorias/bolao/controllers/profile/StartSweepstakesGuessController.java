package br.com.ideiasaleatorias.bolao.controllers.profile;

import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.ideiasaleatorias.bolao.controllers.forms.StartGuessForm;
import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesStartGuessDao;
import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.infra.ViewMessages;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.actions.RegisterStartSweepstakesGuessAction;
import br.com.ideiasaleatorias.bolao.validations.ChampionAndWorstMustBeDifferent;

@Controller
@Transactional
public class StartSweepstakesGuessController {

	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private TeamDao teamDao;
	@Autowired
	private SweepstakesStartGuessDao sweepstakesStartGuessDao;
	@Autowired
	private RegisterStartSweepstakesGuessAction registerGuessAction;
	
	@InitBinder
	public void init(WebDataBinder binder){
		binder.addValidators(new ChampionAndWorstMustBeDifferent());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/sweepstake/start/guess/form")
	public String form(Model model, StartGuessForm form,
			@AuthenticationPrincipal SystemUser currentUser) {

		model.addAttribute("championshipList", championshipDao.findAllForUser(currentUser.getId()));

		if (form.getChampionshipId() != null) {
			model.addAttribute("teamList", teamDao.findAllByChampionshipOrderByNameAsc(form.getChampionshipId()));
			Championship championship = championshipDao.findOne(form.getChampionshipId());
			model.addAttribute("currentChampionship",championship);
			Optional<SweepstakesStartGuess> foundGuess = sweepstakesStartGuessDao
					.findByMemberAndChampionship(currentUser.getId(), form.getChampionshipId());
			foundGuess.ifPresent(guess -> form.fillWithGuesses(guess)); 
		}
		return "profile/start-guess";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/sweepstake/start/guess")
	public String guess(Model model, @Valid StartGuessForm form, BindingResult bindingResult,
			RedirectAttributes redirectAttributes, @AuthenticationPrincipal SystemUser currentUser) {
		if (bindingResult.hasErrors()) {
			return form(model, form, currentUser);
		}

		SweepstakesStartGuess newGuess = form.build(championshipDao, teamDao, currentUser);

		//e agora? esse if é de validacao da entrada do usuario? crio uma classe de validacao do Spring MVC ou deixo aqui?
		if (!newGuess.beforeChampionshipTime()) {
			bindingResult.rejectValue("championshipId", "startGuessForm.invalidTime",
					"Você não pode mais fazer esse palpite, já passou da hora");
			return form(model,form,currentUser);
		}

		registerGuessAction.execute(currentUser,newGuess);


		ViewMessages.successFlash("chutes registrados com sucesso", redirectAttributes);
		return "redirect:/sweepstake/start/guess/form?championshipId=" + form.getChampionshipId();
	}
}
