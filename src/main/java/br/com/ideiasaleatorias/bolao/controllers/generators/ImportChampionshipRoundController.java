package br.com.ideiasaleatorias.bolao.controllers.generators;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.ideiasaleatorias.bolao.daos.ChampionshipDao;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.scrappers.GloboBrasileiraoRoundScrapper;
import br.com.ideiasaleatorias.bolao.scrappers.GloboCopaDoMundoRoundScrapper;

@Controller
public class ImportChampionshipRoundController {
	
	@Autowired
	private GloboBrasileiraoRoundScrapper brasileiraoScrapper;
	
	@Autowired
	private GloboCopaDoMundoRoundScrapper worldcupScrapper;
	
	@Autowired
	private ChampionshipDao championshipDao;
	@Autowired
	private GameDao gameDao;

	@RequestMapping(method = RequestMethod.GET, value = "/magic/importador/8574895jrekgfndjkghuirht8345y")
	@ResponseBody
	@Transactional
	public String importGames(int round) {
		Set<Game> games = brasileiraoScrapper.importGames(round, championshipDao.findBySlug("brasileirao-2019"));
		gameDao.save(games);
		return games.size() + "salvos";
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/magic/importador/kfjhsduihri4ufkdjgfdgfdhug")
	@ResponseBody
	@Transactional
	public String importWorldcupGames(int group,int round) {
		Set<Game> games = worldcupScrapper.importGames(group,round, championshipDao.findBySlug("copa-2018"));
		gameDao.save(games);
		return games.size() + "salvos";
	}
}
