package br.com.ideiasaleatorias.bolao.controllers.forms;

import java.util.Set;

import org.hibernate.validator.constraints.NotBlank;

import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;

public class SweepstakesGroupInviteForm implements HasEmailsList{

	@NotBlank
	private String emails;

	public String getEmails() {
		return emails;
	}

	public void setEmails(String emails) {
		this.emails = emails;
	}

	public Set<GroupInvite> build(SweepstakesGroup sweepstakesGroup) {
		Set<GroupInvite> newInvites = GroupInviteBuilder.build(this,sweepstakesGroup);
		return newInvites;
	}

}
