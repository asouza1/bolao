package br.com.ideiasaleatorias.bolao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6880791863339131195L;

	public NotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
