package br.com.ideiasaleatorias.bolao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.UNAUTHORIZED)
public class NotAuthorizedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3508180651198943830L;

	public NotAuthorizedException(String message) {
		super(message);
	}
	
}
