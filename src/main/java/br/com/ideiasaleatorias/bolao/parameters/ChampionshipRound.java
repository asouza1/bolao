package br.com.ideiasaleatorias.bolao.parameters;

import br.com.ideiasaleatorias.bolao.models.Game;

public class ChampionshipRound {

	private int round;
	private Integer championshipId;
	
	public ChampionshipRound(Game game) {
		this.round = game.getRound();
		this.championshipId = game.getChampionship().getId();
	}
	
	public int getRound() {
		return round;
	}
	
	public Integer getChampionshipId() {
		return championshipId;
	}
}
