package br.com.ideiasaleatorias.bolao.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.ideiasaleatorias.bolao.models.Game;

public class GameTimeAfterChampionshipValidator implements ConstraintValidator<GameTimeAfterChampionship, Game>{

	@Override
	public void initialize(GameTimeAfterChampionship constraintAnnotation) {
		
	}

	@Override
	public boolean isValid(Game game, ConstraintValidatorContext context) {
		return game.afterChampionshipStart();
	}

}
