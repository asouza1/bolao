package br.com.ideiasaleatorias.bolao.validations;

import java.util.Optional;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.GameGuessForm;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDaoConcrete;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.parameters.ChampionshipRound;

public class OnlyOneDoubleGuessPerRound implements Validator {

	private SystemUser currentUser;
	private GameGuessDaoConcrete gameGuessDao;
	private GameDao gameDao;

	public OnlyOneDoubleGuessPerRound(SystemUser currentUser, GameGuessDaoConcrete gameGuessDao,
			GameDao gameDao) {
		this.currentUser = currentUser;
		this.gameGuessDao = gameGuessDao;
		this.gameDao = gameDao;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!target.getClass().equals(GameGuessForm.class)) {
			return;
		}

		GameGuessForm guessForm = (GameGuessForm) target;
		if (guessForm.isDoubled()) {
			Game game = gameDao.findOne(guessForm.getGameId());
			Optional<GameGuess> doubledFound = gameGuessDao.findDoubledGuess(new ChampionshipRound(game),
					currentUser.getId());
			Optional<GameGuess> currentGuess = gameGuessDao.findByGameIdAndMemberSystemUserId(
					guessForm.getGameId(), currentUser.getId());			
			
			if (doubledFound.isPresent() && isNotSameGuess(doubledFound,currentGuess)) {
				errors.rejectValue("doubled", "gameGuessForm.doubled.registered",
						"Você já tem palpite dobrado nessa rodada");
			}
		}

	}

	private boolean isNotSameGuess(Optional<GameGuess> doubledFound,
			Optional<GameGuess> currentGuessFound) {
		if(!doubledFound.isPresent() || !currentGuessFound.isPresent()){
			return true;
		}
		
		GameGuess doubledGuess = doubledFound.get();
		GameGuess currentGuess = currentGuessFound.get();
		
		return !doubledGuess.getId().equals(currentGuess.getId());
	}

}
