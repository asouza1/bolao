package br.com.ideiasaleatorias.bolao.validations;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;

public class GuessTimeBeforeChampionshipValidator implements ConstraintValidator<GuessTimeBeforeChampionship, SweepstakesStartGuess>{

	@Override
	public void initialize(GuessTimeBeforeChampionship constraintAnnotation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isValid(SweepstakesStartGuess guess, ConstraintValidatorContext context) {
		return guess.beforeChampionshipTime();
	}

}
