package br.com.ideiasaleatorias.bolao.validations;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.GameGuessForm;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.models.Game;

public class GuessBeforeGame implements Validator {		

	private GameDao gameDao;

	public GuessBeforeGame(GameDao gameDao) {
		this.gameDao = gameDao;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!target.getClass().equals(GameGuessForm.class)) {
			return;
		}
		
		GameGuessForm form = (GameGuessForm) target;
		Game game = gameDao.findOne(form.getGameId());
		if(!game.allowGuess()){
			errors.rejectValue("gameId","gameGuessForm.gameId.invalid_time","Passou da hora de palpitar");
		}
		
	}

}
