package br.com.ideiasaleatorias.bolao.validations;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.StartGuessForm;

public class ChampionAndWorstMustBeDifferent implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(!target.getClass().equals(StartGuessForm.class)) {
			return;
		}
						
		StartGuessForm form = (StartGuessForm) target;
		if(form.getChampionTeamId() == null || form.getWorstTeamId() == null){
			return;
		}
		
		if(form.getChampionTeamId().equals(form.getWorstTeamId())){
			errors.rejectValue("worstTeamId","startGuessForm.best_and_worst_equal" ,"O pior time não pode também ser o campeão ;) ");
		}
	}

}
