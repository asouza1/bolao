package br.com.ideiasaleatorias.bolao.validations;

import java.util.Set;

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.HasEmailsList;

public class AllEmailsAreValidValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(!HasEmailsList.class.isAssignableFrom(target.getClass())){
			return;
		}
		
		HasEmailsList form = (HasEmailsList) target;
		
		Set<String> emails = form.getSplittedEmails();
		
		EmailValidator emailValidator = new EmailValidator();
		for (String email : emails) {
			if(!emailValidator.isValid(email, null)){
				errors.rejectValue("emails", "sweepstakesGroupInviteForm.emails-not-valid","O email \""+email+"\" não está valido");
			}
		}
	}

}
