package br.com.ideiasaleatorias.bolao.validations;

import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.SweepstakesGroupForm;

public class EmailsNeededValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(!SweepstakesGroupForm.class.isAssignableFrom(target.getClass())){
			return;
		}
		
		SweepstakesGroupForm form = (SweepstakesGroupForm) target;
		if(!StringUtils.hasText(form.getEmails()) && !form.preferToImportCreatedGroupd()){
			errors.rejectValue("emails", "sweepstakesGroupInviteForm.emails-needed","Você precisa ou passar os emails ou escolher um grupo para importar");			
		}
	}

}
