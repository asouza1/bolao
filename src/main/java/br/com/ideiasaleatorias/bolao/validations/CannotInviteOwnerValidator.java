package br.com.ideiasaleatorias.bolao.validations;

import java.util.Set;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import br.com.ideiasaleatorias.bolao.controllers.forms.HasEmailsList;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

public class CannotInviteOwnerValidator implements Validator {

	private SystemUser currentUser;

	public CannotInviteOwnerValidator(SystemUser currentUser) {
		this.currentUser = currentUser;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(!HasEmailsList.class.isAssignableFrom(target.getClass())){
			return;
		}
		
		HasEmailsList emailsList = (HasEmailsList) target;
		Set<String> emails = emailsList.getSplittedEmails();
		if(emails.contains(currentUser.getEmail())){
			errors.rejectValue("emails", "hasEmailsList.owner_email_invalid","Você não pode se convidar para o grupo");
		}
	}

}
