package br.com.ideiasaleatorias.bolao.messages.listeners;

import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.daos.CalculatedRoundScoreDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.daos.RankingViewEntryDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesGroupDao;
import br.com.ideiasaleatorias.bolao.messages.CalculatedRoundScoreMessage;
import br.com.ideiasaleatorias.bolao.messages.Queues;
import br.com.ideiasaleatorias.bolao.models.CalculatedRoundScore;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.RankingViewEntry;

@Transactional
@Component
public class ProcessCalculatedRoundScoreListener {

	@Autowired
	private GameGuessDao gameGuessDao;
	@Autowired
	private CalculatedRoundScoreDao calculatedRoundScoreDao;
	@Autowired
	private RankingViewEntryDao rankingViewEntryDao;
	@Autowired
	private SweepstakesGroupDao sweepstakesGroupDao;
	private static Logger logger = LoggerFactory
			.getLogger(ProcessCalculatedRoundScoreListener.class);

	@JmsListener(destination = Queues.CALCULATED_ROUND_SCORE_QUEUE)
	public void receiveMessage(CalculatedRoundScoreMessage message) {
		logger.info("Salvando o novo score por conta palpite {}",message);
		
		GameGuess guess = gameGuessDao.findOne(message.getGameGuessId());
		calculatedRoundScoreDao.save(new CalculatedRoundScore(guess, message.getScore()));

		Optional<RankingViewEntry> rankingViewEntry = rankingViewEntryDao.findByMemberId(guess
				.getMember().getId());

		if (rankingViewEntry.isPresent()) {
			rankingViewEntry.get().updateScore(message.getScore());
		} else {
			rankingViewEntryDao.save(new RankingViewEntry(guess.getMember(), guess.getMember()
					.getChampionship(), guess.getMember().getSystemUser(), message.getScore()));
		}

	}
}
