package br.com.ideiasaleatorias.bolao.messages;

import java.io.Serializable;
import java.math.BigDecimal;

import br.com.ideiasaleatorias.bolao.models.GameGuess;

public class CalculatedRoundScoreMessage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3428217711245599329L;
	private Integer gameGuessId;
	private BigDecimal score;

	public CalculatedRoundScoreMessage(GameGuess guess, BigDecimal score) {
		this.gameGuessId = guess.getId();
		this.score = score;
	}

	public Integer getGameGuessId() {
		return gameGuessId;
	}

	public BigDecimal getScore() {
		return score;
	}

	@Override
	public String toString() {
		return "CalculatedRoundScoreMessage [gameGuessId=" + gameGuessId + ", score=" + score + "]";
	}
	
	

}
