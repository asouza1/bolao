package br.com.ideiasaleatorias.bolao.messages.listeners;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.messages.ActualGameResultMessage;
import br.com.ideiasaleatorias.bolao.messages.CalculatedRoundScoreMessage;
import br.com.ideiasaleatorias.bolao.messages.Queues;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.scorerules.AdditionalRankingScoreCalculator;
import br.com.ideiasaleatorias.bolao.models.scorerules.ProportionalAdditionalCalculator;

@Component
@Transactional
public class ProcessGameResultListener {

	@Autowired
	private GameDao gameDao;
	@Autowired
	private GameGuessDao gameGuessDao;
	@Autowired
	private List<AdditionalRankingScoreCalculator> additionalCalculators;
	@Autowired
	private ProportionalAdditionalCalculator proportionalCalculator;
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	private EntityManager manager;
	

	@JmsListener(destination = Queues.RESULTS_QUEUE)
	public void receiveMessage(ActualGameResultMessage message) {
		List<GameGuess> guesses = gameGuessDao.findAllByGameId(message.getGameId());
		
		manager.persist(message.toActualGameResult(gameDao));
		
		for (GameGuess guess : guesses) {
			BigDecimal partial = guess.calculateScore(message);
			BigDecimal additional = calculateAdditionalScore(message, guess, partial);
			
			jmsTemplate.send(Queues.CALCULATED_ROUND_SCORE_QUEUE, (session) -> {			
				BigDecimal totalAcumulated = partial.add(additional);				
				BigDecimal totalWithProportionalRisk = totalAcumulated.add(proportionalCalculator.calculate(guess, message, totalAcumulated));
				
				return session.createObjectMessage(new CalculatedRoundScoreMessage(guess,totalWithProportionalRisk));
			});			
			
			
		}
	}

	private BigDecimal calculateAdditionalScore(ActualGameResultMessage message, GameGuess guess,
			BigDecimal partial) {
		BigDecimal additional = BigDecimal.ZERO;
		for (AdditionalRankingScoreCalculator calculator : additionalCalculators) {
			additional = additional.add(calculator.calculate(guess, message,partial));
		}
				
		return additional;
	}
}
