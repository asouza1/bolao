package br.com.ideiasaleatorias.bolao.messages;

import java.io.Serializable;

import javax.persistence.EntityManager;

import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.models.ActualGameResult;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.scorerules.HasGameResult;
import br.com.ideiasaleatorias.bolao.models.scorerules.GameOutcome;

/**
 * Representa a mensagem do resultado de um jogo
 * @author alberto
 *
 */
public class ActualGameResultMessage implements Serializable, HasGameResult {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2745510032827387996L;
	private Integer gameId;
	private int principalScore;
	private int visitingScore;

	public ActualGameResultMessage(Integer gameId, int principalScore, int visitingScore) {
		this.gameId = gameId;
		this.principalScore = principalScore;
		this.visitingScore = visitingScore;
	}
	
	public Integer getGameId() {
		return gameId;
	}
	
	public int getPrincipalScore() {
		return principalScore;
	}
	
	public int getVisitingScore() {
		return visitingScore;
	}

	@Override
	public String toString() {
		return "GameResultMessage [gameId=" + gameId + ", principalScore=" + principalScore
				+ ", visitingScore=" + visitingScore + "]";
	}

	public GameOutcome getGameOutcome() {
		return GameOutcome.discover(this);
	}
	
	public ActualGameResult toActualGameResult(GameDao gameDao){
		//FIXME Deveria receber o dao? eu, alberto, não vejo problema
		return new ActualGameResult(gameDao.findOne(gameId),principalScore,visitingScore);
	}

	
}
