package br.com.ideiasaleatorias.bolao.messages;

public interface Queues {

	public final static String RESULTS_QUEUE = "results-queue";
	public final static String CALCULATED_ROUND_SCORE_QUEUE = "calculated-round-score-queue";
}
