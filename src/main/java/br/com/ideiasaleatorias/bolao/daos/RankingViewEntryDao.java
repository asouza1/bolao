package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.RankingViewEntry;

@Repository
public interface RankingViewEntryDao extends CrudRepository<RankingViewEntry, Integer>{

	Optional<RankingViewEntry> findByMemberId(Integer id);

	@Query("select entry from RankingViewEntry entry "
			+ " where entry.championship.id = :championshipId and entry.systemUser.id in (select sgm.member.id from SweepstakesGroupMember sgm where sgm.group.id = :groupId)"
			+ " order by entry.acumulatedScore desc")
	List<RankingViewEntry> findByGroupIdAndGroupChampionshipId(@Param("groupId")Integer groupId,@Param("championshipId") Integer championshipId);

	List<RankingViewEntry> findByChampionshipIdOrderByAcumulatedScoreDesc(Integer championshipId);

}
