package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;

import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.SweepstakesGroupMember;

@Repository
public interface SweepstakesGroupMemberDao extends
		org.springframework.data.repository.Repository<SweepstakesGroupMember, Integer> {

	List<SweepstakesGroupMember> findAllByGroupId(Integer groupId);

	
}
