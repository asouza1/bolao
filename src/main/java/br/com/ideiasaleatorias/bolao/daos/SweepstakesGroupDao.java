package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;

@Repository
public interface SweepstakesGroupDao extends CrudRepository<SweepstakesGroup, Integer>{

	@Query("select sgm.group from SweepstakesGroupMember sgm  where sgm.member.id = :memberId and sgm.group.championship.active = true")
	List<SweepstakesGroup> findAllGroupsForMember(@Param("memberId")Integer memberId);
	
	List<SweepstakesGroup> findByOwnerId(Integer id);

	SweepstakesGroup findByShareCode(String shareCode);

}
