package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;
import br.com.ideiasaleatorias.bolao.parameters.ChampionshipRound;

@Repository
public class GameGuessDaoConcrete {

	@Autowired
	private GameGuessDao gameGuessDao;
	@PersistenceContext
	private EntityManager manager;

	public List<GameGuess> findAllByChampionshipAndUser(Integer championshipId, Integer userId) {
		return gameGuessDao.findAllByChampionshipAndUser(championshipId, userId);
	}

	public <S extends GameGuess> S save(S entity) {
		return gameGuessDao.save(entity);
	}

	public Optional<GameGuess> findDoubledGuess(ChampionshipRound round, Integer userId) {
		String jpql = "select g from GameGuess g where g.game.championship.id = :championshipId and g.member.systemUser.id = :userId and g.doubled=true and g.game.round = :round";
		List<GameGuess> list = manager.createQuery(jpql,GameGuess.class)
			.setParameter("championshipId", round.getChampionshipId())
			.setParameter("round", round.getRound())
			.setParameter("userId", userId).getResultList();
		if(list.isEmpty()){
			return Optional.empty();
		}
		
		Assert.state(list.size() == 1,"Não poderia haver mais de um palpite dobrado para o campeonato!!!. "+round+" user = "+userId);
		
		return Optional.of(list.get(0));
		
	}

	public Optional<GameGuess> findByGameIdAndMemberSystemUserId(Integer gameId, Integer id) {
		return gameGuessDao.findByGameIdAndMemberSystemUserId(gameId,id);
	}

	public List<AveragePossibleGameResult> averagePossibleGameResults(Integer gameId) {
		return gameGuessDao.averagePossibleGameResults(gameId);
	}

	
}
