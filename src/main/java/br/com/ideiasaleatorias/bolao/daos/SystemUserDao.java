package br.com.ideiasaleatorias.bolao.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Repository
public interface SystemUserDao extends CrudRepository<SystemUser, Integer>{

	@Query("select u from SystemUser u where u.socialInfo.id = :id")
	Optional<SystemUser> findBySocialId(@Param("id") String id);

	@Query("select u from SystemUser u join fetch u.roles where u.id = :id")
	SystemUser loadFullUser(@Param("id")Integer id);

	Optional<SystemUser> findByEmail(String email);

	Optional<SystemUser> findBySecretPhraseAndEmail(String secretPhrase,String email);
		
}
