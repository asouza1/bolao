package br.com.ideiasaleatorias.bolao.daos;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Repository
public interface SweepstakesMemberDao extends CrudRepository<SweepstakesMember, Integer>{

	/**
	 * 
	 * @param championshipId id do {@link Championship} que ta sendo buscado
	 * @param memberId id do {@link SystemUser} que está sendo buscado
	 * @return
	 */
	@Query
	Optional<SweepstakesMember> findByChampionshipIdAndSystemUserId(@Param("championshipId")Integer championshipId, @Param("memberId")Integer memberId);
}
