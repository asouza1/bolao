package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.CalculatedRoundScore;
import br.com.ideiasaleatorias.bolao.models.RoundScore;

@Repository
public interface CalculatedRoundScoreDao extends CrudRepository<CalculatedRoundScore, Integer>{

	@Query("select c from CalculatedRoundScore c where c.guess.game.championship.id = :championshipId and c.guess.game.round = :round")
	List<CalculatedRoundScore> findAllByChampionshipAndRound(@Param("championshipId")Integer championshipId, @Param("round")int round);

	@Query("select new br.com.ideiasaleatorias.bolao.models.RoundScore(c.guess.member.systemUser,sum(c.score)) from CalculatedRoundScore c where c.guess.game.championship.id = :championshipId and c.guess.game.round = :round and c.guess.member.systemUser.id in (select groupMember.member.id from SweepstakesGroupMember groupMember where groupMember.group.id = :groupId) group by c.guess.member.systemUser order by sum(c.score) desc")
	List<RoundScore> listRoundScoreOrderByDesc(@Param("round") Integer round,@Param("groupId") Integer groupId, @Param("championshipId") Integer championshipId);

}
