package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;

@Repository
public interface GameGuessDao extends CrudRepository<GameGuess, Integer>{

	@Query("select g from GameGuess g where g.game.championship.id = :championshipId and g.member.systemUser.id = :userId")
	List<GameGuess> findAllByChampionshipAndUser(@Param("championshipId")Integer championshipId, @Param("userId") Integer userId);

	List<GameGuess> findAllByGameId(@Param("gameId")Integer gameId);

	@Query("select new br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult(g.game,g.possibleGameResult,"
			+ "count(g.possibleGameResult),"
			+ "(select count(1) from GameGuess g2 where g2.game.id = :gameId))"
			+ " from GameGuess g where g.game.id = :gameId group by g.possibleGameResult")
	List<AveragePossibleGameResult> averagePossibleGameResults(@Param("gameId")Integer gameId);
	
	@Query("select new br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult(g.game,g.possibleGameResult,"
			+ "count(g.possibleGameResult),"
			+ "(select count(1) from GameGuess g2 where g2.game.id = g.game.id))"
			+ " from GameGuess g "
			+ " where g.game.championship.id = :championshipId and g.game.round = :round"
			+ " group by g.game.id,g.possibleGameResult ")
	List<AveragePossibleGameResult> averagePossibleResults(@Param("championshipId")Integer championshipId, @Param("round")int round);

	Optional<GameGuess> findByGameIdAndMemberSystemUserId(@Param("gameId")Integer gameId, @Param("id")Integer id);

	@Query("select g from GameGuess g where g.game.championship.id = :championshipId and g.member.systemUser.id = :userId and g.game.round = :round")
	List<GameGuess> findByChampionshipAndRound(@Param("championshipId")Integer championshipId,@Param("userId")Integer userId,@Param("round")int round);

}
