package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Team;

@Repository
public interface TeamDao extends JpaRepository<Team, Integer>{

	@Query("select t from Team t order by t.name asc")
	List<Team> findAllOrderByNameAsc();

	Optional<Team> findBySlug(String slug);

	@Query("select t from Championship c join c.teams t where c.id = :id order by t.name asc")
	List<Team> findAllByChampionshipOrderByNameAsc(@Param("id") Integer id);

	Optional<Team> findByName(String name);

}
