package br.com.ideiasaleatorias.bolao.daos;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Game;

@Repository
public interface GameDao extends CrudRepository<Game, Integer>{

	@Query("select g from Game g where g.championship.id = :championshipId and gameTime >= :time order by g.gameTime asc")
	List<Game> findNextGamesOfChampionship(@Param("championshipId")Integer championshipId, @Param("time")LocalDateTime time);

	List<Game> findAllByChampionshipIdOrderByGameTimeAsc(Integer championshipId);

}
