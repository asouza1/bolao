package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Championship;

@Repository
public interface ChampionshipDao extends org.springframework.data.repository.Repository<Championship, Integer>{

	@Query("select sm.championship from SweepstakesMember sm where sm.systemUser.id = :id and sm.championship.active = true")
	List<Championship> findAllForUser(@Param("id") Integer id);

	@Query("select c from Championship c where c.active = true and c.id not in(select sm.championship.id from SweepstakesMember sm where sm.systemUser.id = :id)")
	List<Championship> findAllThatUserIsNotMember(@Param("id") Integer id);

	Championship findBySlug(String slug);
	
	Championship findOne(Integer id);
	
	List<Championship> findByActive(boolean active);
	
	Iterable<Championship> findAll();
}
