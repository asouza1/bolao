package br.com.ideiasaleatorias.bolao.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Role;

@Repository
public interface RoleDao extends CrudRepository<Role, Integer>{

	public Role findByName(String name);
}
