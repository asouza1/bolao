package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.GroupInvite;

@Repository
public interface GroupInviteDao extends CrudRepository<GroupInvite, Integer>{

	GroupInvite findByUuid(String uuid);

	List<GroupInvite> findByEmailAndAccepted(String email, boolean isAccepted);

}
