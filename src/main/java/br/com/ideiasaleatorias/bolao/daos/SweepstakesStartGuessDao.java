package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;

@Repository
public interface SweepstakesStartGuessDao extends CrudRepository<SweepstakesStartGuess, Integer> {

	@Query("select s from SweepstakesStartGuess s where s.member.id = :memberId and s.championship.id = :championshipId ")
	Optional<SweepstakesStartGuess> findByMemberAndChampionship(@Param("memberId")Integer memberId,
			@Param("championshipId")Integer championshipId);

	@Query("select s from SweepstakesStartGuess s where s.member.id in (:userIds) and s.championship.id = :championshipId ")
	List<SweepstakesStartGuess> findAllByMembersAndChampionship(@Param("userIds") Set<Integer> userIds, @Param("championshipId")Integer championshipId);

}
