package br.com.ideiasaleatorias.bolao.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.ActualGameResult;

@Repository
public interface ActualGameResultDao extends
		org.springframework.data.repository.Repository<ActualGameResult, Integer> {

	@Query("select result from ActualGameResult result where result.game.championship.id = :championshipId and result.game.round = :round")
	List<ActualGameResult> findAllByChampionshipAndRound(@Param("championshipId") Integer championshipId, @Param("round")int round);

	List<ActualGameResult> findAllByGameChampionshipId(Integer championshipId);
	
	Optional<ActualGameResult> findTopByGameChampionshipIdOrderByGameRoundDesc(Integer championshipId);
	
	@Query("select g from ActualGameResult g where g.game.championship.id = :championshipId and (g.game.principalTeam.id = :teamId or g.game.visitingTeam.id = :teamId) order by g.game.gameTime asc")
	List<ActualGameResult> findAllByChampionshipAndTeam(@Param("championshipId") Integer championshipId, @Param("teamId") Integer teamId);	

}
