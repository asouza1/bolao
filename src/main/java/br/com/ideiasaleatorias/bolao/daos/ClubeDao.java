package br.com.ideiasaleatorias.bolao.daos;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.ideiasaleatorias.bolao.models.Team;

@Repository
public interface ClubeDao extends CrudRepository<Team, Integer>{
	
	public Team findBySlug(String slug);

}
