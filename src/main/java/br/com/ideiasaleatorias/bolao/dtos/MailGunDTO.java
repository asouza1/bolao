package br.com.ideiasaleatorias.bolao.dtos;

import java.util.ArrayList;
import java.util.List;

public class MailGunDTO {

	private String from;
	private List<String> to = new ArrayList<>();
	private String subject;
	private String text;

	public MailGunDTO(String from, String to, String subject, String text) {
		super();
		this.from = from;
		this.to.add(to);
		this.subject = subject;
		this.text = text;
	}

	public String getFrom() {
		return from;
	}

	public List<String> getTo() {
		return to;
	}

	public String getSubject() {
		return subject;
	}

	public String getText() {
		return text;
	}
	
	

}
