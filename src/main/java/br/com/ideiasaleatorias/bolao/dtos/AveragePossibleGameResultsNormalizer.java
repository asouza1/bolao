package br.com.ideiasaleatorias.bolao.dtos;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.scorerules.AveragePossibleGameResult;
import br.com.ideiasaleatorias.bolao.models.scorerules.GameOutcome;

/**
 * Normaliza a lista de médias, para sempre conter todas as possibilidades.
 * @author alberto
 *
 */
public class AveragePossibleGameResultsNormalizer {

	public static List<AveragePossibleGameResult> normalize(
			Game game, List<AveragePossibleGameResult> averagePossibleGameResults) {
		Set<GameOutcome> outcomes = averagePossibleGameResults.stream().map(average -> average.getPossibleGameResult())
				.collect(Collectors.toSet());
		
		ArrayList<AveragePossibleGameResult> completeList = new ArrayList<>(averagePossibleGameResults);
		
		for(GameOutcome outcome : GameOutcome.values()){
			if(!outcomes.contains(outcome)){
				completeList.add(new AveragePossibleGameResult(game,outcome, 
						0, 1));
			}
		}
		return completeList;
	}

}
