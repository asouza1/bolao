package br.com.ideiasaleatorias.bolao;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.http.HttpStatus;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import br.com.ideiasaleatorias.bolao.infra.DevAutoLoginFilter;

@SpringBootApplication
@EntityScan(basePackageClasses = { Boot.class, Jsr310JpaConverters.class })
@EnableJms
@EnableAsync
public class Boot extends SpringBootServletInitializer {

	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setExposeContextBeansAsAttributes(true);
		return resolver;
	}

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {

		return (container -> {
			ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND,
					"/WEB-INF/views/errors/404.jsp");
			container.addErrorPages(error404Page);
			container.setSessionTimeout(0);
		});
	}

	@Bean
	@Profile("dev")
	public FilterRegistrationBean filterRegistrationBean() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean();
		DevAutoLoginFilter autoLoginFilter = new DevAutoLoginFilter();
		registrationBean.setFilter(autoLoginFilter);
		registrationBean.setOrder(Integer.MAX_VALUE - 1);
		return registrationBean;
	}

	@Bean
	@Profile("dev")
	public JavaMailSender mailSenderDev() {
		return new JavaMailSender() {
			JavaMailSender mailSender = gmail();

			@Override
			public void send(MimeMessagePreparator... mimeMessagePreparators) throws MailException {
				throw new UnsupportedOperationException("Nao implementado para dev");
			}

			@Override
			public void send(MimeMessagePreparator mimeMessagePreparator) throws MailException {
				throw new UnsupportedOperationException("Nao implementado para dev");
			}

			@Override
			public void send(MimeMessage... mimeMessages) throws MailException {
				for (MimeMessage mimeMessage : mimeMessages) {
					send(mimeMessage);
				}
			}

			@Override
			public void send(MimeMessage mimeMessage) throws MailException {
				try {
					System.out.println(mimeMessage.getContent());
				} catch (IOException | MessagingException e) {
					e.printStackTrace();
				}
			}

			@Override
			public MimeMessage createMimeMessage(InputStream contentStream) throws MailException {
				return mailSender.createMimeMessage(contentStream);
			}

			@Override
			public MimeMessage createMimeMessage() {
				return mailSender.createMimeMessage();
			}

			@Override
			public void send(SimpleMailMessage... simpleMessages) throws MailException {
				System.out.println(Arrays.toString(simpleMessages));
			}

			@Override
			public void send(SimpleMailMessage simpleMessage) throws MailException {
				System.out.println(simpleMessage);
			}
		};
	}

	@Bean
	@Profile({ "prod", "homolog" })
	public JavaMailSender mailSender() {

		return gmail();
	}

	private JavaMailSender gmail() {
		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost("smtp.gmail.com");
		javaMailSenderImpl.setPassword("894uihkjgdfguoierhot4");
		javaMailSenderImpl.setPort(587);
		javaMailSenderImpl.setUsername("nossobolao165@gmail.com");
		Properties mailProperties = new Properties();
		mailProperties.put("mail.smtp.auth", true);
		mailProperties.put("mail.smtp.starttls.enable", true);
		javaMailSenderImpl.setJavaMailProperties(mailProperties);

		return javaMailSenderImpl;
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
//	@Override
//	public void onStartup(ServletContext servletContext) throws ServletException {
//		// TODO Auto-generated method stub
//		Connection connection;
//		try {
//			System.out.println("tentando conectar no banco...");
//			connection = DriverManager
//					.getConnection("jdbc:mysql://127.0.0.1/bolao_prod?createDatabaseIfNotExist=true&user=bolao&password=8708f3e471892e7545cf93f855914fd3d0fe875921f1087F&7");
//			System.out.println(connection.prepareStatement("select * from system_user").executeQuery());		
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		super.onStartup(servletContext);
//	}

	public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException,
			KeyStoreException, SQLException {
		SpringApplication.run(Boot.class, args);
	}
}
