package br.com.ideiasaleatorias.bolao.builders;

import java.util.Optional;

import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;

public class GameGuessBuilder {

	public static class MemberStep {

		private SweepstakesMember member;

		public MemberStep(SweepstakesMember member) {
			this.member = member;
		}

		/**
		 * 
		 * @param game
		 * @param principalScore placar do time da casa
		 * @param visitingScore placar do time visitante
		 * @param doubled pontuacao dobrada
		 * @return
		 */
		public GameGuess build(Game game, int principalScore,int visitingScore,boolean doubled) {			
			return new GameGuess(member, principalScore, visitingScore, doubled, game);
		}

	}
	
	/**
	 * 
	 * @param memberDao
	 * @param userId
	 * @param championshipId
	 * @return
	 */
	public static MemberStep member(SweepstakesMemberDao memberDao, Integer userId,
			Integer championshipId) {
		Optional<SweepstakesMember> member = memberDao.findByChampionshipIdAndSystemUserId(
				championshipId, userId);
		if (!member.isPresent()) {
			throw new IllegalArgumentException(
					"O usuario não está relacionado com o torneio. Usuario id=" + userId
							+ "; Campeonato id=" + championshipId);
		}
		return new MemberStep(member.get());
		
	}

}
