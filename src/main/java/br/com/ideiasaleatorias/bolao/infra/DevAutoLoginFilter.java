package br.com.ideiasaleatorias.bolao.infra;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.services.AppSecurityContext;

public class DevAutoLoginFilter implements Filter{
	
	private static Logger logger = LoggerFactory.getLogger(DevAutoLoginFilter.class);

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {		
		logger.info("Executando auto login filter");
		SystemUserDao users = ApplicationContextHolder.instance.getBean(SystemUserDao.class);
		AppSecurityContext securityContext = ApplicationContextHolder.instance.getBean(AppSecurityContext.class);
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if((authentication!=null && authentication.getPrincipal().equals("anonymousUser"))){
			Iterator<SystemUser> all = users.findAll().iterator();
			if(all.hasNext()){
				SystemUser firstUser = all.next();			
				securityContext.changeCurrentUser(users.loadFullUser(firstUser.getId()));
			}
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
