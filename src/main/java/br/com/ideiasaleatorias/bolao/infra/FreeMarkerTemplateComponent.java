package br.com.ideiasaleatorias.bolao.infra;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

@Component
public class FreeMarkerTemplateComponent {

	@Autowired
	private Configuration configuration;
	
	public String execute(String path, Map<String, Object> contents) {
		try {
			Template template = configuration.getTemplate(path);
			StringWriter out = new StringWriter();
			template.process(contents, out);
			return out.toString();
		} catch (IOException | TemplateException e) {
			throw new RuntimeException(e);
		}
	}

}