package br.com.ideiasaleatorias.bolao.infra.emails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Classe que serve para carregar um email baseado em uma URL
 * @author alberto
 *
 */
@Component
public class EmailTemplateLoader {
	
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * 
	 * @param url para carregar o template
	 * @return html do tempalte
	 */
	public String load(String url){
		try {
			return restTemplate.getForObject(url, String.class);
		} catch(Exception e){
			throw new RuntimeException(e);
		}
	}
}
