package br.com.ideiasaleatorias.bolao.infra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import br.com.ideiasaleatorias.bolao.exceptions.NotAuthorizedException;
import br.com.ideiasaleatorias.bolao.exceptions.NotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@ExceptionHandler(NotAuthorizedException.class)
	public String handleNotAuthorized(NotAuthorizedException exception){
		logger.info("Aconteceu um acesso a recurso proibido {}",exception);
		return "errors/401";
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(code=HttpStatus.INTERNAL_SERVER_ERROR)
	public String handleGenericError(Exception exception){
		logger.error("Aconteceu um erro: {}",exception);
		return "errors/500";
	}
	
	@ExceptionHandler({NoHandlerFoundException.class,NotFoundException.class})
	@ResponseStatus(code=HttpStatus.NOT_FOUND)
	public String handleNotFound(Exception exception){
		return "errors/404";
	}
	
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	@ResponseStatus(code=HttpStatus.METHOD_NOT_ALLOWED)
	public String methodNotAllowedHandler(Exception exception){
		return "errors/405";
	}
}
