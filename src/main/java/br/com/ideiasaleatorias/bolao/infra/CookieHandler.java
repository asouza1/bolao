package br.com.ideiasaleatorias.bolao.infra;

import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.util.CookieGenerator;
import org.springframework.web.util.WebUtils;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CookieHandler {

	@Autowired
	private NativeWebRequest request;	
	private static final int expiration = 86400;
	
	public void saveCookie(HttpServletResponse response,String key,String value) {
		saveCookie(response, key, value,expiration);
	}
	
	public void saveCookie(HttpServletResponse response,String key,String value,int maxAge) {
		CookieGenerator cookieGenerator = new CookieGenerator();
		cookieGenerator.setCookieName(key);
		cookieGenerator.setCookieMaxAge(maxAge);
		cookieGenerator.addCookie(response, value);
	}
	
	public Optional<String> readCookie(String key){
		Cookie cookie = WebUtils.getCookie(request.getNativeRequest(HttpServletRequest.class), key);
		if(cookie != null){
			return Optional.of(cookie.getValue());
		}
		return Optional.empty();
	}

	public void remove(String key, HttpServletResponse response) {
		saveCookie(response, key, "", 0);
	}
	
}
