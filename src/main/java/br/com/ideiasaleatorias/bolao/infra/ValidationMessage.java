package br.com.ideiasaleatorias.bolao.infra;

public class ValidationMessage {

	private String objectName;
	private String message;

	public ValidationMessage(String objectName, String defaultMessage) {
		this.objectName = objectName;
		this.message = defaultMessage;
	}

	public String getObjectName() {
		return objectName;
	}
	
	public String getMessage() {
		return message;
	}
}
