package br.com.ideiasaleatorias.bolao.infra;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class AjaxResponse {

	private String message;
	private List<ValidationMessage> validationMessages = new ArrayList<ValidationMessage>();

	public AjaxResponse(String okMessage) {
		this.message = okMessage;
	}

	public AjaxResponse(Errors errors) {
		this.message = "Existem erros";
		errors.getFieldErrors().stream().forEach(error -> {
			validationMessages.add(new ValidationMessage(error.getField(),error.getDefaultMessage()));
		});
	}
	
	public String getMessage() {
		return message;
	}
	
	public List<ValidationMessage> getValidationMessages() {
		return validationMessages;
	}

}
