package br.com.ideiasaleatorias.bolao.infra.emails;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.util.Assert;

public class HtmlMailBuilder {
	
	public static class SenderPart {

		private String from;
		private String subject;
		private String to;
		private String body;

		public SenderPart(String from, String subject, String to, String body) {
			this.from = from;
			this.subject = subject;
			this.to = to;
			this.body = body;
		}

		public MimeMessage build(JavaMailSender mailer) throws MessagingException {
			MimeMessage message = mailer.createMimeMessage();
			message.setFrom(from);
			message.setSubject(subject);
			
			MimeMessageHelper helper = new MimeMessageHelper(message, false);
			helper.setTo(to);
			helper.setText(body,true);
			return message;
		}

	}

	public static class BodyPart {

		private String from;
		private String to;
		private String subject;

		public BodyPart(String from, String subject, String to) {
			this.from = from;
			this.subject = subject;
			this.to = to;
		}

		public SenderPart body(String body) {
			Assert.hasText(body);
			return new SenderPart(from,subject,to,body);
		}

	}

	private HtmlMailBuilder(){
		
	}

	public static class ToPart {

		private String from;
		private String subject;

		public ToPart(String from, String subject) {
			this.from = from;
			this.subject = subject;
		}
		
		public BodyPart to(String to){
			Assert.hasText(to);
			return new BodyPart(from,subject,to);
			
		}
		
		

	}

	public static class SubjectPart {

		private String from;

		public SubjectPart(String from) {
			this.from = from;
		}

		public ToPart subject(String subject) {
			Assert.hasText(subject);
			return new ToPart(from,subject);
		}
		
		

	}

	public static SubjectPart from(String from) {
		Assert.hasText(from);
		return new SubjectPart(from);
	}

}
