package br.com.ideiasaleatorias.bolao.infra.emails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.MethodArgumentBuilder;

@Component
public class HostUrlBuilder {
	
	private Environment environment;

	@Autowired
	public HostUrlBuilder(Environment environment) {
		this.environment = environment;
	}

	/**
	 * 
	 * @param mappingName nome do mapeamento do {@link RequestMapping}
	 * @param args argumentos
	 * @return
	 */
	public String urlFromMappingName(String mappingName,Object... args){
		MethodArgumentBuilder builder = MvcUriComponentsBuilder.fromMappingName(mappingName);
		for (int i=0; i < args.length; i++) {
			builder.arg(i, args[i]);
		}
		
		String uri = builder.build();
		return internalUrl(uri);
	}
	
	/**
	 * 
	 * @param relativeLink endereço relativo ao servidor. Ex: /cliente/senha
	 * @return
	 */
	public String internalUrl(String relativeLink){
		if(relativeLink.startsWith("/")){
			relativeLink = relativeLink.substring(1);
		}
		
		return  environment.getProperty("site.host")+relativeLink;
	}
	
	/**
	 * 
	 * @param relativeLink endereço relativo ao servidor. Ex: /cliente/senha
	 * @return
	 */
	public String externalUrl(String relativeLink){
		if(relativeLink.startsWith("/")){
			relativeLink = relativeLink.substring(1);
		}
		
		return  environment.getProperty("site.host")+relativeLink;
	}	
}
