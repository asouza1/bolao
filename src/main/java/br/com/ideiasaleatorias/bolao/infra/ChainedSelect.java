package br.com.ideiasaleatorias.bolao.infra;

import java.util.List;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

import org.springframework.beans.BeanWrapperImpl;

/**
 * Serve para montar o json necessário pelo lib remoted chain do jquery.
 * @author alberto
 *
 */
public class ChainedSelect {

	public static String json(List<?> list, String valueField, String labelField) {
		JsonObjectBuilder pairs = Json.createObjectBuilder();
		for (Object object : list) {
			BeanWrapperImpl beanWrapperImpl = new BeanWrapperImpl(object);
			pairs.add(beanWrapperImpl.getPropertyValue(valueField).toString(),
					beanWrapperImpl.getPropertyValue(labelField).toString());
		}
		return pairs.build().toString();
	}
}
