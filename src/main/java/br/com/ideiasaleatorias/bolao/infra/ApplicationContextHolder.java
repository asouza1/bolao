package br.com.ideiasaleatorias.bolao.infra;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Gambizinha para poder acessar essa bagaça de qualquer lugar... Use com sabedoria.
 * @author alberto
 *
 */
@Component
public class ApplicationContextHolder implements ApplicationContextAware{
	
	public static ApplicationContext instance;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		ApplicationContextHolder.instance = applicationContext;
	}

}
