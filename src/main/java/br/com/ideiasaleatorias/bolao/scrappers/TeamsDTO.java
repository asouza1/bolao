package br.com.ideiasaleatorias.bolao.scrappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeamsDTO {

	private TeamDTO mandante = new TeamDTO();
	private TeamDTO visitante = new TeamDTO();

	public TeamDTO getMandante() {
		return mandante;
	}

	public void setMandante(TeamDTO mandante) {
		this.mandante = mandante;
	}

	public TeamDTO getVisitante() {
		return visitante;
	}

	public void setVisitante(TeamDTO visitante) {
		this.visitante = visitante;
	}

	@Override
	public String toString() {
		return "TeamsDTO [mandante=" + mandante + ", visitante=" + visitante + "]";
	}
	
	

}
