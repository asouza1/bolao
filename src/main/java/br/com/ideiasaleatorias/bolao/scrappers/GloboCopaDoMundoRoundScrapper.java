package br.com.ideiasaleatorias.bolao.scrappers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.Game;

@Service
public class GloboCopaDoMundoRoundScrapper {

	@Autowired
	private GloboScrapper globoScrapper;

	public Set<Game> importGames(int group,int round, Championship championship) {
		String endpointJogos = "http://globoesporte.globo.com/servico/esportes_campeonato/responsivo/widget-uuid/cdec28af-302d-4a52-bcdc-283f6d63ea61/fases/fase-grupos-copa-do-mundo-2018/grupo/"+group+"/rodada/"+round+"/jogos.html";

		return globoScrapper.importGames(endpointJogos, round, championship);

	}
}
