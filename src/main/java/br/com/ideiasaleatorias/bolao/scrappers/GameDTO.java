package br.com.ideiasaleatorias.bolao.scrappers;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GameDTO {

	private String data_realizacao;
	private TeamsDTO equipes = new TeamsDTO();
	private SedeDTO sede = new SedeDTO();

	public String getData_realizacao() {
		return data_realizacao;
	}

	public void setData_realizacao(String data_realizacao) {
		this.data_realizacao = data_realizacao;
	}

	public TeamsDTO getEquipes() {
		return equipes;
	}

	public void setEquipes(TeamsDTO equipes) {
		this.equipes = equipes;
	}

	public SedeDTO getSede() {
		return sede;
	}

	public void setSede(SedeDTO sede) {
		this.sede = sede;
	}
	
	public String getNomeDaSede() {
		return this.sede.getNome_popular();
	}

	public LocalDateTime getGameTime() {
		return LocalDateTime.parse(this.data_realizacao, DateTimeFormatter.ISO_DATE_TIME);
	}

	@Override
	public String toString() {
		return "GameDTO [data_realizacao=" + data_realizacao + ", equipes=" + equipes + ", sede="
				+ sede + "]";
	}

	public boolean hasValidDate() {
		return StringUtils.hasText(data_realizacao);
	}

	
}
