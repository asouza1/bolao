package br.com.ideiasaleatorias.bolao.scrappers;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.Game;

@Service
public class GloboBrasileiraoRoundScrapper {

	@Autowired
	private GloboScrapper globoScrapper;	

	public Set<Game> importGames(int round, Championship championship) {
		String endpointJogos = "https://api.globoesporte.globo.com/tabela/d1a37fa4-e948-43a6-ba53-ab24ab3a45b1/fase/fase-unica-seriea-2019/rodada/"+round+"/jogos/";
		
		return globoScrapper.importGames(endpointJogos, round, championship);

	}
}
