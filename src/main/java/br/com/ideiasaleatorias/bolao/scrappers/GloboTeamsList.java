package br.com.ideiasaleatorias.bolao.scrappers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import br.com.ideiasaleatorias.bolao.models.Team;

@Repository
public class GloboTeamsList {

	private Map<String, String> namesToSlug = new HashMap<String, String>();

	{
		namesToSlug.put("Flamengo", "flamengo");
		namesToSlug.put("Sport", "sport");
		namesToSlug.put("Palmeiras", "palmeiras");
		namesToSlug.put("Atlético-PR", "atletico-pr");
		namesToSlug.put("Atlético-MG", "atletico-mg");
		namesToSlug.put("Santos", "santos");
		namesToSlug.put("Cruzeiro", "cruzeiro");
		namesToSlug.put("Botafogo", "botafogo");
		namesToSlug.put("São Paulo", "sao-paulo");
		namesToSlug.put("Vitória", "vitoria-ba");
		namesToSlug.put("Corinthians", "corinthians");
		namesToSlug.put("Grêmio", "gremio");
		namesToSlug.put("América-MG", "america-mg");
		namesToSlug.put("Fluminense", "fluminense");
		namesToSlug.put("Internacional", "internacional");
		namesToSlug.put("Chapecoense", "chapecoense");
		namesToSlug.put("Vasco", "vasco");
		namesToSlug.put("Bahia", "bahia");
		namesToSlug.put("Ceará", "ceara");
		namesToSlug.put("Paraná", "parana");
		namesToSlug.put("Uruguai", "uruguai");
		namesToSlug.put("Arábia Saudita", "arabia-saudita");
		namesToSlug.put("Egito", "egito");
		namesToSlug.put("Rússia", "russia");
		namesToSlug.put("Espanha", "espanha");
		namesToSlug.put("Irã", "ira");
		namesToSlug.put("Marrocos", "marrocos");
		namesToSlug.put("Portugal", "portugal");
		namesToSlug.put("Austrália", "australia");
		namesToSlug.put("Dinamarca", "dinamarca");
		namesToSlug.put("França", "franca");
		namesToSlug.put("Peru", "peru");
		namesToSlug.put("Argentina", "argentina");
		namesToSlug.put("Croácia", "croacia");
		namesToSlug.put("Islândia", "islandia");
		namesToSlug.put("Nigéria", "nigeria");
		namesToSlug.put("Brasil", "brasil");
		namesToSlug.put("Costa Rica", "costa-rica");
		namesToSlug.put("Sérvia", "servia");
		namesToSlug.put("Suíça", "suica");
		namesToSlug.put("Alemanha", "alemanha");
		namesToSlug.put("Coreia do Sul", "coreia-do-sul");
		namesToSlug.put("México", "mexico");
		namesToSlug.put("Suécia", "suecia");
		namesToSlug.put("Bélgica", "belgica");
		namesToSlug.put("Inglaterra", "inglaterra");
		namesToSlug.put("Panamá", "panama");
		namesToSlug.put("Tunísia", "tunisia");
		namesToSlug.put("Colômbia", "colombia");
		namesToSlug.put("Japão", "japao");
		namesToSlug.put("Polônia", "polonia");
		namesToSlug.put("Senegal", "senegal");
		
	}	
	
	public String findSlugBySiteName(String key) {
		
		Assert.isTrue(namesToSlug.containsKey(key),"Foi passado um nome que não existe no html retornado pelo endpoint da globo. Time = "+key);
		return namesToSlug.get(key);
	}
	
	public Set<Team> allTeams(){
		return namesToSlug.entrySet().stream().map(entry -> {
			return new Team(entry.getKey(), entry.getValue());
		}).collect(Collectors.toSet());
	}
}
