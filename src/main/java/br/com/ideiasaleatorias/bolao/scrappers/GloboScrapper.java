package br.com.ideiasaleatorias.bolao.scrappers;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import br.com.ideiasaleatorias.bolao.daos.TeamDao;
import br.com.ideiasaleatorias.bolao.models.Championship;
import br.com.ideiasaleatorias.bolao.models.Game;

@Service
public class GloboScrapper {

	@Autowired
	private TeamDao teamDao;
	private Logger logger = LoggerFactory.getLogger(GloboScrapper.class);

	private HttpComponentsClientHttpRequestFactory getRequestFactory() {
		try {
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain,
					String authType) -> true;

			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
					.loadTrustMaterial(null, acceptingTrustStrategy).build();

			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

			requestFactory.setHttpClient(httpClient);

			return requestFactory;
		} catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException e) {
			throw new RuntimeException(e);
		}
	}

	public Set<Game> importGames(String endpoint, int round, Championship championship) {
		HashSet<Game> games = new HashSet<Game>();

		RestTemplate rest = new RestTemplate(getRequestFactory());
		List<GameDTO> rodada = rest.exchange(endpoint, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<GameDTO>>() {
				}).getBody();

		for (GameDTO gameDTO : rodada) {
			if(!gameDTO.hasValidDate()) {
				logger.info("Ignorando jogo por falta de data"+gameDTO);
				continue;
			}
			
			LocalDateTime gameTime = gameDTO.getGameTime();

			games.add(new Game(
					teamDao.findBySlug(gameDTO.getEquipes().getMandante().getSigla()).get(),
					teamDao.findBySlug(gameDTO.getEquipes().getVisitante().getSigla()).get(),
					gameTime, championship, gameDTO.getNomeDaSede(), round));
		}

		return games;
	}

	public static void main(String[] args) {
		LocalDateTime dateTime = LocalDateTime.parse("2019-05-01T16:00",
				DateTimeFormatter.ISO_DATE_TIME);
		System.out.println(dateTime.getHour());
	}
}
