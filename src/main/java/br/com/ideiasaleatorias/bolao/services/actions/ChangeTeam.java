package br.com.ideiasaleatorias.bolao.services.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.models.SystemUser;
import br.com.ideiasaleatorias.bolao.models.Team;

@Service
public class ChangeTeam {

	@Autowired
	private SystemUserDao userDao;
	
	public SystemUser execute(Integer userId, Team team) {
		SystemUser databaseUser = userDao.findOne(userId);
		databaseUser.setTeam(team);
		return databaseUser;
	}

	
}
