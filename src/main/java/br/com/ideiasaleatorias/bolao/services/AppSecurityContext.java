package br.com.ideiasaleatorias.bolao.services;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Service
public class AppSecurityContext {

	public void changeCurrentUser(SystemUser currentUser) {
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken(currentUser, null, currentUser
						.getAuthorities()));
	}
	
	public SystemUser get(){
		return (SystemUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
