package br.com.ideiasaleatorias.bolao.services.actions;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroupMember;
import br.com.ideiasaleatorias.bolao.models.SweepstakesMember;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Service
public class BecomeSweepstakesGroupMemberAction {

	@PersistenceContext
	private EntityManager manager;
	@Autowired
	private SweepstakesMemberDao sweepstakesMemberDao;
	private static Logger logger = LoggerFactory
			.getLogger(BecomeSweepstakesGroupMemberAction.class);

	public void execute(GroupInvite groupInvite, SystemUser user) {
		Assert.isTrue(groupInvite.getEmail().equals(user.getEmail()),
				"O convite não é para o usuário => convite(" + groupInvite.getEmail()
						+ ") e usuario(" + user.getEmail() + ")");

		if (groupInvite.isAccepted()) {
			logger.info(
					"Tentou registrar um membro para um group que ele ja tava registrado grupo:{}, usuario:{}",
					groupInvite.getUuid(), user.getName());
			return;
		}
		
		execute(groupInvite.getSweepstakesGroup(), user);

		groupInvite.accept();
	}

	public void execute(SweepstakesGroup group, SystemUser currentUser) {
		SweepstakesGroupMember newGroupMember = new SweepstakesGroupMember(group, currentUser);
		manager.persist(newGroupMember);

		Optional<SweepstakesMember> foundMember = sweepstakesMemberDao
				.findByChampionshipIdAndSystemUserId(newGroupMember.getChampionship().getId(),
						newGroupMember.getMember().getId());

		if (!foundMember.isPresent()) {
			sweepstakesMemberDao.save(new SweepstakesMember(newGroupMember.getChampionship(),
					newGroupMember.getMember()));
		}
	}

}
