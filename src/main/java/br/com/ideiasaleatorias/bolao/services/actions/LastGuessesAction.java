package br.com.ideiasaleatorias.bolao.services.actions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.builders.GameGuessBuilder;
import br.com.ideiasaleatorias.bolao.daos.GameDao;
import br.com.ideiasaleatorias.bolao.daos.GameGuessDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesMemberDao;
import br.com.ideiasaleatorias.bolao.models.Game;
import br.com.ideiasaleatorias.bolao.models.GameGuess;

@Service
public class LastGuessesAction {

	@Autowired
	private GameDao gameDao;
	@Autowired
	private GameGuessDao gameGuessDao;
	@Autowired
	private SweepstakesMemberDao memberDao;

	/**
	 * 
	 * @param championshipId
	 * @param userId
	 * @return
	 */
	public List<GameGuess> load(Integer championshipId, Integer userId,LocalDateTime instant) {
		List<Game> nextGames = gameDao.findNextGamesOfChampionship(championshipId,instant);
		List<GameGuess> guesses = gameGuessDao.findAllByChampionshipAndUser(championshipId, userId);

		Map<Game, List<GameGuess>> gameAndGuesses = guesses.stream().collect(
				Collectors.groupingBy(guess -> guess.getGame()));

		ArrayList<GameGuess> lastGuesses = new ArrayList<GameGuess>();

		for (Game nextGame : nextGames) {
			GameGuess newGuess = GameGuessBuilder.member(memberDao, userId, championshipId).build(
					nextGame, 0, 0, false);

			lastGuesses.add(gameAndGuesses.getOrDefault(nextGame, Arrays.asList(newGuess)).get(0));
		}
		return lastGuesses;
	}

}
