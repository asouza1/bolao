package br.com.ideiasaleatorias.bolao.services.actions;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.daos.RoleDao;
import br.com.ideiasaleatorias.bolao.daos.SystemUserDao;
import br.com.ideiasaleatorias.bolao.models.Role;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Service
public class SaveOrUpdateUserAction {
	
	@Autowired
	private SystemUserDao userDao;
	@Autowired
	private RoleDao roleDao;

	public SystemUser execute(User profile){
				
		Optional<SystemUser> foundSocialUser = userDao.findBySocialId(profile.getId());
				
		
		if (foundSocialUser.isPresent()) {
			return foundSocialUser.get().updateSocial(profile);

		} 
		
		Optional<SystemUser> foundUserByMail = userDao.findByEmail(profile.getEmail());	
		
		
		if(foundUserByMail.isPresent() && !foundUserByMail.get().isSocialUser()){
			throw new RuntimeException("Já existe um usuário cadastrado com o mesmo email do seu facebook");
		}
		
		SystemUser currentUser = null;
		
		if(foundUserByMail.isPresent() && foundUserByMail.get().isSocialUser()){
			currentUser = foundUserByMail.get().updateSocial(profile);
		}
		
		else {									
			currentUser = SystemUser.buildFrom(profile,
					roleDao.findByName(Role.PARTICIPANTE.getName()));
			userDao.save(currentUser);
		}	
		
		return currentUser;
	}
}
