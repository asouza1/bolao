package br.com.ideiasaleatorias.bolao.services.actions;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.daos.RankingViewEntryDao;
import br.com.ideiasaleatorias.bolao.daos.SweepstakesStartGuessDao;
import br.com.ideiasaleatorias.bolao.models.ProjectRankingViewEntry;
import br.com.ideiasaleatorias.bolao.models.RankingItem;
import br.com.ideiasaleatorias.bolao.models.RankingViewEntry;
import br.com.ideiasaleatorias.bolao.models.SweepstakesGroup;
import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;

@Service
public class ProjectionAcion {

	@Autowired
	private RankingViewEntryDao rankingViewEntryDao;
	@Autowired
	private SweepstakesStartGuessDao sweepstakesStartGuessDao;

	//TODO um teste aqui? pelo amor! O teste deve indicar o que da para quebrar mais...
	public TreeSet<? extends RankingItem> execute(SweepstakesGroup group, SweepstakesStartGuess guessToCompare) {

		RankingEntriesCollection entries = new RankingEntriesCollection(rankingViewEntryDao.findByGroupIdAndGroupChampionshipId(group.getId(),group.getChampionship().getId()));

		List<SweepstakesStartGuess> startGuesses = findStartGuesses(group, entries.source());
		
		TreeSet<ProjectRankingViewEntry> projectionEntries = new TreeSet<ProjectRankingViewEntry>();

		for (SweepstakesStartGuess guess : startGuesses) {
			BigDecimal extraScore = guess.calculateExtraScore(guessToCompare,group);
			projectionEntries.add(new ProjectRankingViewEntry(entries.get(guess.getMember()), extraScore));
		}
		
		return projectionEntries;
	}

	private List<SweepstakesStartGuess> findStartGuesses(SweepstakesGroup group,
			List<RankingViewEntry> entries) {
		Set<Integer> userIds = entries.stream().map(entry -> entry.getSystemUser().getId())
				.collect(Collectors.toSet());
		
		List<SweepstakesStartGuess> startGuesses = sweepstakesStartGuessDao
				.findAllByMembersAndChampionship(userIds, group.getChampionship().getId());
		return startGuesses;
	}

}
