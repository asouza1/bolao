package br.com.ideiasaleatorias.bolao.services;

import java.util.HashMap;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.infra.FreeMarkerTemplateComponent;
import br.com.ideiasaleatorias.bolao.infra.emails.EmailTemplateLoader;
import br.com.ideiasaleatorias.bolao.infra.emails.HostUrlBuilder;
import br.com.ideiasaleatorias.bolao.infra.emails.HtmlMailBuilder;
import br.com.ideiasaleatorias.bolao.models.GroupInvite;

/**
 * Ela centraliza os emails que precisam ser disparados. No inicio podemos
 * aceitar uma certa falta de coesão nela. Um detalhe importate, esse objeto
 * dispara emails fazendo requisições para o servidor. Se você estiver num meio
 * de transação para criação de um objeto e precisar deste mesmo objeto no outro
 * request, a regra vai falhar.
 * 
 * @author alberto
 *
 */
@Service
public class CentralEmails {

	@Autowired
	private DefaultMailSender mailer;
	@Autowired
	private FreeMarkerTemplateComponent template;
	@Autowired
	private HostUrlBuilder hostUrlBuilder;

	public void sendGroupInvite(GroupInvite groupInvite) {

		HashMap<String, Object> params = new HashMap<>();
		params.put("hostUrlBuilder", hostUrlBuilder);
		params.put("acceptanceLink", "/sweepstakes/invite/" + groupInvite.getUuid());
		String body = template.execute("new-group-invite.ftl", params);
		mailer.send(groupInvite.getEmail(), "Venha participar do bolão",body);
	}

}
