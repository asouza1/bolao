package br.com.ideiasaleatorias.bolao.services.actions;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.daos.SweepstakesStartGuessDao;
import br.com.ideiasaleatorias.bolao.models.SweepstakesStartGuess;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

@Service
public class RegisterStartSweepstakesGuessAction {

	@Autowired
	private SweepstakesStartGuessDao sweepstakesStartGuessDao;

	public void execute(SystemUser currentUser, SweepstakesStartGuess newGuess) {
		Optional<SweepstakesStartGuess> foundGuess = sweepstakesStartGuessDao
				.findByMemberAndChampionship(currentUser.getId(), newGuess.getChampionship().getId());
		if (foundGuess.isPresent()) {
			SweepstakesStartGuess currentGuess = foundGuess.get();
			currentGuess.updateWithNewGuess(newGuess);
		} else {
			sweepstakesStartGuessDao.save(newGuess);
		}		
	}

}
