package br.com.ideiasaleatorias.bolao.services.actions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import br.com.ideiasaleatorias.bolao.models.GroupInvite;
import br.com.ideiasaleatorias.bolao.models.Invitable;
import br.com.ideiasaleatorias.bolao.services.CentralEmails;

/**
 * Ação de mandar convites para os participantes de um grupo
 * @author alberto
 *
 */
@Service
public class InviteAction {
	
	@Autowired
	private CentralEmails centralEmails;

	@Async
	public void execute(Invitable invitable) {
		Iterable<GroupInvite> invites = invitable.getInvites();
		for (GroupInvite groupInvite : invites) {
			centralEmails.sendGroupInvite(groupInvite);
		}
	}

}
