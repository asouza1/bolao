package br.com.ideiasaleatorias.bolao.services;

import br.com.ideiasaleatorias.bolao.exceptions.NotAuthorizedException;
import br.com.ideiasaleatorias.bolao.models.BelongsToUser;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

public class VerifyOwnershipService {

	/**
	 * 
	 * @param currentUser
	 * @param property
	 * @param errorMessage
	 * @throws NotAuthorizedException Caso o usuário não seja dono do recurso
	 */
	public static void userHasProperty(SystemUser currentUser,BelongsToUser property,String errorMessage){
		if(!property.belongsTo(currentUser)){
			throw new NotAuthorizedException(errorMessage);
		}
	}
}
