/**
 * Vai guardar todas as classes que representam ações no sistema. É complicado achar elas, mas vamos tentar pensar
 * em cada uma e experimentar, para ver como sai o código final.
 */
package br.com.ideiasaleatorias.bolao.services.actions;