package br.com.ideiasaleatorias.bolao.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DefaultMailSender {

	@Autowired
	private MailGunSender mailer;

	public void send(String to, String subject, String body) {
		mailer.send(to, subject, body);
	}
}
