package br.com.ideiasaleatorias.bolao.services.actions;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.util.Assert;

import br.com.ideiasaleatorias.bolao.models.RankingViewEntry;
import br.com.ideiasaleatorias.bolao.models.SystemUser;

class RankingEntriesCollection {

	private List<RankingViewEntry> entries;
	private Map<SystemUser, List<RankingViewEntry>> userAndRankingEntry;

	public RankingEntriesCollection(List<RankingViewEntry> entries) {
		this.entries = entries;
		userAndRankingEntry = entries.stream().collect(Collectors.groupingBy(entry -> entry.getSystemUser()));
	}

	public List<RankingViewEntry> source() {
		return entries;
	}

	public RankingViewEntry get(SystemUser member) {
		List<RankingViewEntry> userEntry = userAndRankingEntry.get(member);
		Assert.isTrue(userEntry.size() == 1, "Só deveria ter uma entrada no ranking associada ao usuário");
		return userEntry.get(0);
	}
	
	

}
