package br.com.ideiasaleatorias.bolao.services;

import java.net.URI;
import java.security.cert.X509Certificate;
import java.util.Base64;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Component
public class MailGunSender {
	
	private RestTemplate restTemplate;
	private HttpHeaders headers = new HttpHeaders();

	public MailGunSender() {

		try {
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain,
					String authType) -> true;

			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
					.loadTrustMaterial(null, acceptingTrustStrategy).build();

			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

			CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

			requestFactory.setHttpClient(httpClient);

			restTemplate = new RestTemplate(requestFactory);
			headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			headers.add("Authorization", "Basic "+Base64.getEncoder()
			.encodeToString("api:key-694cd92050cdafc595e5dafb40dfc95c".getBytes()));
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void send(String to,String subject,String text){
		LinkedMultiValueMap<String, String> form = new LinkedMultiValueMap<>();
		form.add("from", "Partiu bolão <administrador@partiubolao.com.br>");
		form.add("to", to);
		form.add("subject", subject);
		form.add("text", text);		
		HttpEntity<Object> httpEntity = new HttpEntity<>(form,headers);
		
		
		
		
		try {
			restTemplate.postForEntity(
					URI.create("https://api.mailgun.net/v3/partiubolao.com.br/messages"), httpEntity,
					String.class);
		} catch (HttpClientErrorException e) {
			throw new RuntimeException(e);
		}
				
	}
}
