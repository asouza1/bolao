<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="../../taglibs.jsp" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	${successMessage}
	<c:if test="${not empty championshipList}">
		<form:form action="/championship/join" commandName="sweepstakesMemberForm">
			<form:select path="championshipId" items="${championshipList}" itemValue="id" itemLabel="name"/>
			<input type="submit"/>
		</form:form>
	</c:if>
	<c:if test="${empty championshipList}">
		Você já participa de todos os bolões!
	</c:if>
	
</body>
</html>