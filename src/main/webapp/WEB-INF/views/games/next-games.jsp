<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Palpites">
	<jsp:attribute name="extraScripts">
		<script src="/assets/js/pages/guess-game.js"></script>
		<script src="/assets/js/ajax/ajax-status-codes.js"></script>
		<script type="text/javascript">
			var guessGame = GuessGame();
			guessGame.clickGuess(".form-game-guess");
			guessGame.clickAverageInfo(".average-guess-info");
		</script>	
	</jsp:attribute>
	<jsp:body>
		<c:set var="previousRound" scope="page" value="0" />
		<c:forEach items="${gameGuessList}" var="guess">
			<c:set var="actualRound" scope="page"
					value="${guess.game.round}" />
			<c:if test="${actualRound != previousRound}">
				<div class="box">
					<div class="box-header" style="text-align: center">
							<h2>Rodada ${actualRound}</h2>
							<span> - <a
									href="/guess/timeline?championshipId=${guess.game.championship.id}&round=${actualRound}"
									target="_blank">Ver auditoria da rodada</a></span>
					</div>
				</div>
			</c:if>
			<div class="box">
				<div style="text-align: center">
					<div style="display: inline; width: 60px; height: 60px;">Dia e
						Hora: ${dateFormatter.format(guess.game.gameTime)} - Rodada
						${guess.game.round}</div>
				</div>
				<form method="post" action="/guess/games"
						data-game-id="${guess.game.id}" class="games_form">
					<input type="hidden" name="gameId" value="${guess.game.id}" />
					<div style="text-align: center">
						<label for="principalScore" class="games_label principal">${guess.game.principalTeam.name}</label>
						<input name="principalScore" value="${guess.principalScore}"
								id="principalScore" class="games_input" style="width: 30px" /> <span
								class="versus">x</span> <input name="visitingScore"
								value="${guess.visitingScore}" id="visitingScore"
								class="games_input" style="width: 30px" /> <label
								for="visitingScore" class="games_label visiting">${guess.game.visitingTeam.name}</label>
						<span class="alert" data-game-id="${guess.game.id}"
								style="color: red"></span>
					</div>
					<div class="no-border">
						<div class="row">
							<div class="col-xs-6 text-right">
								<div style="display: inline;">
									Dobrado? <input type="checkbox" value="true" name="doubled"
											${guess.doubled ? 'checked' : ''} id="doubled"
											class="games_input_checkbox">
								</div>
							</div>
							<div class="col-xs-6 text-left">
								<div style="display: inline">
									<button type="submit" class="btn btn-xs form-game-guess">Palpitar</button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!--COmeco das porcentagens -->
				<div class="box box-default collapsed-box no-border">
					<div class="box-header with-border">
						<div class="box-tools pull-right">
							<button type="button"
									class="btn bg-teal btn-sm average-guess-info"
									data-widget="collapse"
									data-href="/guess/game/${guess.game.id}/averages"
									data-game-id="${guess.game.id}">Percentuais de chutes</button>
						</div>
					</div>
					<div class="box-body" style="display: none;">
						<div class="box-body table-responsive no-padding"
								style="text-align: center">
							<table id="average-info-${guess.game.id}"
									class="table table-hover" style="display: none">
								<tbody>
									<tr>
										<th>Vitória da casa (%)</th>
										<th>Empate (%)</th>
										<th>Vitória do visitante (%)</th>
									</tr>
									<tr>
										<td>PRINCIPAL_WINNER</td>
										<td>DRAW</td>
										<td>VISITING_WINNER</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<c:set var="previousRound" scope="page" value="${guess.game.round}" />
			</div>
		</c:forEach>
	</jsp:body>
</bolao:masterPage>
