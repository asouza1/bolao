<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Palpite">
	<jsp:body>
		<c:forEach items="${gameGuessList}" var="guess">
			<c:set var="actualRound" scope="page" value="${guess.game.round}" />
			<c:if test="${actualRound != previousRound}">
				<div class="box">
					<div class="box-header" style="text-align: center">
							<h2>Rodada ${actualRound}</h2>
							<span> - <a
									href="/guess/timeline?championshipId=${guess.game.championship.id}&round=${actualRound}"
									target="_blank">Ver auditoria da rodada</a></span>
					</div>
				</div>
			</c:if>
			<div class="box">
				<div style="text-align: center">
					<div style="display: inline; width: 60px; height: 60px;">Dia e
						Hora: ${dateFormatter.format(guess.game.gameTime)} - Rodada
						${guess.game.round}</div>
				</div>
				<div style="text-align: center">
					<label for="principalScore" class="games_label principal">${guess.game.principalTeam.name}</label>
					<input readonly="readonly" name="principalScore" value="${guess.principalScore}" id="principalScore" class="games_input" style="width: 20px"/> 
					<span class="versus">x</span> 
					<input readonly="readonly" name="visitingScore"	value="${guess.visitingScore}" id="visitingScore" class="games_input" style="width: 20px" /> 
					<label for="visitingScore" class="games_label visiting">${guess.game.visitingTeam.name}</label>
				</div>
				<div class="no-border">
					<div style="text-align: center">
						Dobrado? <span>${guess.doubled ? 'sim' : 'não'}</span>
					</div>
					<c:if test="${not guess.game.allowGuess()}">
						<div style="text-align: center">
								<a href="/report/guesses/game/${guess.game.id}"
										target="_blank">Ver todos palpites para esse jogo</a></span>						
						</div>
					</c:if>					
				</div>
				
			</div>
			<c:set var="previousRound" scope="page" value="${guess.game.round}"/>
		</c:forEach>
	</jsp:body>
</bolao:masterPage>