<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ include file="../../taglibs.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/forms.css" />
		<link rel="stylesheet" href="/assets/css/reset.css" />
		<link rel="stylesheet" href="/assets/css/sections.css" />
		<link rel="stylesheet" href="/assets/css/tables.css" />

		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<title>Resultados</title>
	</head>

	<body>
		<div class="overlay">
			<div class="container_medium">

				<header>
					<div class="dashboard_link">
						<a href="/dashboard">Voltar para o Dashboard</a>
					</div>
					<div>
						<h1 class="title">Bolão</h1>
					</div>
				</header>

				<div class="content">
					<div>${successMessage}</div>
					<c:forEach items="${games}" var="game">
						<form method="post" class="games_form" action="/magic/score/klfhjdfsdkhfjkdshfjkd9589476">
								<input type="hidden" name="gameId" value="${game.id}">
								<div>
									<div class="games_title">
										<label for="principalScore" class="games_label principal">${game.principalTeam.name}</label>
										<input name="principalScore"  ${gamesResults.findResult(game).present ? 'readonly' : ''} value="${gamesResults.getPrincipalScore(game)}" id="principalScore" class="games_input"/>
										<span class="versus">x</span>
										<input name="visitingScore" ${gamesResults.findResult(game).present ? 'readonly' : ''} value="${gamesResults.getVisitingScore(game)}" id="visitingScore" class="games_input"/>
										<label for="visitingScore" class="games_label visiting">${game.visitingTeam.name}</label>
									</div>
	
									<div>Dia e Hora: ${dateFormatter.format(game.gameTime)} - Rodada ${game.round}</div/>
								</div>
								<button type="submit" class="form-game-guess">Processar resultado</button>
						</form>
					</c:forEach>
				</div>
			</div>
		</div>
	</body>
</html>