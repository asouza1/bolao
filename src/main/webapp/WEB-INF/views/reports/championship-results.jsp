<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<jsp:attribute name="extraCss">
		<link href="/assets/js/jquery/multiple-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/projection.css" />
	</jsp:attribute>
	<jsp:attribute name="extraScripts">
		<script src="/assets/js/jquery/jquery.chained.remote.min.js"></script>
		<script>
			$("#teamId").remoteChained({
	    		parents : "#championshipId",
	    		url : "/ajax/championship/teams",
	    		loading: "Carregando times",
	    		attribute: "data-param"
			});			
		</script>	
	</jsp:attribute>
	<jsp:body>
		<div class="box">
			<div class="box-header" style="text-align: center">
				<h2>Pesquise o histórico do time</h2>
			</div>
		</div>
		<div class="box" style="text-align: center">
			<form:form action="/report/championship/results" method="get" commandName="reportChampionshipResultForm">
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="championshipId">Campeonato</label>
						</div>
					<div class="col-xs-6 text-left">
						<form:select path="championshipId" data-param="id">
							<form:option value="">--Selecione--</form:option>
							<form:options items="${championshipList}" itemLabel="name" itemValue="id"/>							
						</form:select>
						<form:errors path="championshipId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="bestDefenseTeamId">Times</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="teamId" id="teamId" items="${teamList}" itemLabel="name" itemValue="id"/>
						<form:errors path="teamId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row" style="margin:5px">
					<button type="submit" class="btn">Pesquisar"</button> 
				</div>
			</form:form>				
		</div>
					
		<div class="box-body table-responsive no-padding"  style="text-align: center">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th style="text-align: center">Rodada</th>
						<th style="text-align: center">Jogo</th>
						<th style="text-align: center">Resultado</th>
						<th style="text-align: center">Placar</th>
					</tr>
					<c:forEach var="result" items="${resultList}">
						<tr>
							<td>${result.game.round}</td>
							<td>${result.game.principalTeam.name}x${result.game.visitingTeam.name}</td>
							<td>${result.outcome}</td>
							<td>${result.principalScore}x${result.visitingScore}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
</jsp:body>
</bolao:masterPage>