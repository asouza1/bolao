<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Palpites iniciais">
	<div class="box">
		<div class="box-header"  style="text-align: center">
			<h3 class="box-title">Todos os palpites iniciais</h3>
		</div>		
		<div class="box-body table-responsive no-padding">
			<table class="table table-hover table-striped dataTable" role="grid">
				<tbody>
					<tr style="text-align: center">
						<td>Palpite</td>
						<td>Participante</td>
						<td>Dobrado</td>
						<td>Horário</td>
					</tr>
					<c:forEach items="${guesses}" var="guess">
						<tr style="text-align: center">
							<td>${guess.principalScore} x ${guess.visitingScore}</td>
							<td>${guess.member.systemUser.name}</td>
							<td>${guess.doubled ? 'Sim' : 'Não'}</td>
							<td>${dateFormatter.format(guess.time)}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</bolao:masterPage>