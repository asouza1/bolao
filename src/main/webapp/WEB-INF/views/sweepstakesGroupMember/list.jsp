<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<div class="box">
		<div class="box-header"  style="text-align: center">
			<h2>Bolão ${group.name}</h2>
		</div>
		<div class="box-body table-responsive"  style="text-align: center">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th class="col-xs-6 text-right">Clube</th>
						<th class="col-xs-6 text-left">Nome</th>
					</tr>
					<c:forEach var="groupMember" items="${members}">
						<tr>
							<td class="col-xs-6 text-right">${dashboardView.teamShieldImage(groupMember.member, 2)}</td>
							<td class="col-xs-6 text-left">${groupMember.member.name}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</bolao:masterPage>