<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../taglibs.jsp" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
  		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>Partiu Bolão | Login</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
		<link rel="stylesheet" href="/assets/css/customStyle.css">
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
<body class="hold-transition login-page">

<c:if test="${errorMessage!=null or param.error!=null}">
	<div class="alert alert-danger alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    	<h4><i class="icon fa fa-ban"></i>Ops!</h4>
		${errorMessage}
		${param.error!=null ? SPRING_SECURITY_LAST_EXCEPTION.message : ''}
	</div>
</c:if>

<div class="login-box">
  <div class="login-logo">
    <b>Partiu Bolão</b>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Faça seu login</p>

    <form action="/login" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Login">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Senha">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
      	<label>
      		Manter logado para sempre?
        	<input type="checkbox"  name="remember-me" value="true">
        </label>
      </div>
      <div class="row">

        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

<!--     <div class="social-auth-links text-center"> -->
<!--       <p>- OU -</p> -->
<!--       <form action="/connect/facebook" method="POST" class="btn btn-block btn-social btn-facebook btn-flat"> -->
<!-- 		<i class="fa fa-facebook"></i> -->
<!-- 		<input type="hidden" name="scope" value="email" /> -->
<!-- 		<button type="submit" class="facebook-submit">Entrar com o facebook</button> -->
<!-- 	   </form> -->
<!--     </div> -->
    <!-- /.social-auth-links -->

    <a href="/rules">Veja as regras</a><br>
    <a href="/users/form">Cadastre-se</a><br>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.0 -->
<script src="/assets/js/jquery/jquery-2.1.4.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/assets/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>
<!-- 
	<body>
		<div class="overlay">
			<div class="container_index">

				<header>
					<div>
						<h1 class="title">Bolão</h1>
					</div>
				</header>

				<div>
					<p class="success_message">
						${successMessage}
					</p>
					<p class="error_message">
						${errorMessage}
						${param.error!=null ? SPRING_SECURITY_LAST_EXCEPTION.message : ''}
					</p>
				</div>

				<fieldset class="fieldset_login">
					<legend>Login</legend>

					<form action="/login" method="POST">
						<div>
							<label for="username">Email:</label>
							<input type="text" name="username" id="username"/>
						</div>

						<div>
							<label for="password">Senha:</label>
							<input type="password" name="password" id="password"/>
						</div>

						<button type="submit" class="button">Login</button>
					</form>

					<form action="/connect/facebook" method="POST">
						<input type="hidden" name="scope" value="email" />

						<button type="submit" class="button button_facebook">Connect to Facebook</button>
					</form>

					<a href="/users/form">Cadastre-se</a>
				</fieldset>

				<a href="/rules" class="link_index">Regras</a>
			</div>
		</div>
	</body>
</html>-->