<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Convites pendentes">
	<div class="box">
				<div class="content">
					<p class="success_message">
						${successMessage}
					</p>

					<ul>
						<c:forEach items="${pendingGroupInviteList}" var="groupInvite">
							<li class="group_invite">
								<div>
									<h3>Convite ${groupInvite.sweepstakesGroup.name}</h3>

									<form action="/sweepstakes/invite/${groupInvite.uuid}/accept" method="POST">
										<button type="submit">Entrar para o grupo</button>
									</form>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>		
	</div>
</bolao:masterPage>