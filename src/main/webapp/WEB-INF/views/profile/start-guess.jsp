<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<jsp:attribute name="extraCss">
		<link href="/assets/js/jquery/multiple-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/projection.css" />
	</jsp:attribute>
	<jsp:attribute name="extraScripts">
		<script src="/assets/js/jquery/jquery.chained.remote.min.js"></script>
		<script>
			$(".guess").remoteChained({
	    		parents : "#championshipId",
	    		url : "/ajax/championship/teams",
	    		loading: "Carregando times",
	    		attribute: "data-param"
			});			
		</script>
		<script src="/assets/js/jquery/multiple-select/js/jquery.multi-select.js"></script>
		<script>
			$('#g4TeamsIds').multiSelect();
			$('#z4TeamsIds').multiSelect();
		</script>	
	</jsp:attribute>
	<jsp:body>
		<div class="box">
			<div class="box-header" style="text-align: center">
				<h2>Palpites iniciais</h2>
			</div>
			<p class="text-center">
				Consulte com a pessoa que criou a sua liga sobre quais são os palpites que serão considerados no calculo do resultado final.
				Ex: pode ser que para o seu bolão você e seus amigos tenham definido que só vale o campeão. Mesmo assim, pelo menos por enquanto,
				você é obrigado a preencher todas as informações. Preencha tudo que não é obrigatório com o que você quiser e aí, no calculo final,
				só será considerado o que a pessoa que criou a liga definiu. 
			</p>
		</div>
		<div class="box" style="text-align: center">
			<form:form commandName="startGuessForm" action="/sweepstake/start/guess" method="post">
				<form:hidden path="championshipId"/>
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="championshipId">Campeonato</label>
						</div>
					<div class="col-xs-6 text-left">
						<span>${currentChampionship.name}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="bestDefenseTeamId">Melhor defesa</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="bestDefenseTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="bestDefenseTeamId" cssClass="error_message"/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="bestStrikeTeamId">Melhor Ataque</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="bestStrikeTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="bestStrikeTeamId" cssClass="error_message"/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="championTeamId">Campeão</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="championTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="championTeamId" cssClass="error_message"/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="viceChampionTeamId">Vice-campeão</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="viceChampionTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="viceChampionTeamId" cssClass="error_message"/>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="topScorer">Artilheiro</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:input path="topScorer" class="form-control"/>
						<form:errors path="topScorer" cssClass="error_message"/>
					</div>
				</div>								
				
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="worstTeamId">Último colocado</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="worstTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="worstTeamId" cssClass="error_message"/>
					</div>
				</div>
					
				<div class="row">
					<label for="g4TeamsIds">Times do G4(pressione ctrl para escolher cada um dos times)</label>
					<form:select path="g4TeamsIds" id="g4TeamsIds" items="${teamList}" itemLabel="name" itemValue="id" multiple="true" cssClass="guess"/>
					<form:errors path="g4TeamsIds" cssClass="error_message"/>
				</div>
				<div class="row">
					<label for="z4TeamsIds">Times do Z4(pressione ctrl para escolher cada um dos times)</label>
					<form:select path="z4TeamsIds" id="z4TeamsIds" items="${teamList}" itemLabel="name" itemValue="id" multiple="true" cssClass="guess"/>
					<form:errors path="z4TeamsIds" cssClass="error_message"/>
				</div>
				<div class="row" style="margin:5px">
					<button type="submit" class="btn">Enviar chutes</button> 
				</div>
				</form:form>
			</div>

	</jsp:body>
</bolao:masterPage>
