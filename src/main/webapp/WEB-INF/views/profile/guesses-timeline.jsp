<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Auditoria">
	<div class="box">
		<div class="box-body table-responsive no-padding">
			<table class="table table-hover table-striped dataTable" role="grid">
				<tbody>
					<tr style="text-align: center">
						<td>Jogo</td>
						<td>Resultado</td>
						<td>Palpite</td>
						<td>Dobrado</td>
						<td>Proporoces</td>
						<td>Calculo</td>
					</tr>
					<c:forEach items="${gameGuessList}" var="guess">
						<tr style="text-align: center">
							<td>${guess.game.principalTeam.name} x
								${guess.game.visitingTeam.name}</td>
							<td>${results.of(guess.game).principalScore} x
								${results.of(guess.game).visitingScore}</td>
							<td>${guess.principalScore}x${guess.visitingScore}</td>
							<td><spring:message code="info.${guess.doubled}" /></td>
							<td>
								<c:forEach items="${proportions.of(guess.game)}" var="proportion">
									<spring:message code="gameOutcome.${proportion.possibleGameResult}" />
									-${proportion.percentage} |						
								</c:forEach>
							</td>
							<td style="font-weight:bold">${scores.of(guess).score}</td>
						</tr>
					</c:forEach>
					<tr>
						<td colspan="6" style="font-weight:bold">Total ${gameGuessList.total}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</bolao:masterPage>