<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<jsp:attribute name="extraCss">
		<link rel="stylesheet" href="/assets/css/images.css" />
	</jsp:attribute>
	<jsp:body>
		<div class="box">
			<div class="box-header" style="text-align: center">
				<h2>Escolha seu time</h2>
				<p class="success_message">
					${successMessage}
				</p>
			</div>
			<div class="box-body">
				<ul class="clearfix" style="list-style: none;">
					<c:forEach var="team" items="${teamList}">
						<li style="height:10vw; text-align:center; width:25%; float:left; padding:10px; margin-bottom:10px" >
							<form action="/profile/choose/team/${team.slug}" method="post" class="teamChoice">
								<img class="teamShield" src="${team.shieldLink}" alt="${team.name}"/>
								<button type="submit" class="btn btn-default pull-center" style="display:block; margin:0 auto">Escolher</button>
								<c:if test="${myTeamView.currentUserChoosen(team)}">
									<p class="text-red">Seu time do coração</p>
								</c:if>
							</form>
						</li>
					</c:forEach>
				</ul>
			</div>
		</div>
	</jsp:body>
</bolao:masterPage>