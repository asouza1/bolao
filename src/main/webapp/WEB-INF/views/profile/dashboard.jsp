<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../taglibs.jsp" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
					
<sec:authentication property="principal" var="user"/>					
<bolao:masterPage title="Dashboard">
						<div class="success_message">
							${successMessage}
						</div>
					
						<h4>Bolões que você participa</h4>

							<c:forEach var="group" items="${groups}">
								<div class="col-md-4">
									<div class="box box-solid box-default">
										<div class="box-header">${group.name}</div>
										<div class="box-body">
											<ul class="dashboard_list">
											<li class="dashboard_list_item"><a href="/ranking/group/${group.id}">Ranking interno</a></li>
											<li class="dashboard_list_item"><a href="/ranking/group/${group.id}/by/round">Ranking por rodada</a></li>
											<li class="dashboard_list_item"><a href="/ranking/group/${group.id}/projection/form">Projeção do fim do campeonato</a></li>
											<li class="dashboard_list_item"><a href="/sweepstakes/group/${group.id}/members">Participantes</a></li>
											<c:if test="${group.belongsTo(user)}">
												<li class="dashboard_list_item"><a href="/profile/group/sweepstakes/${group.id}/invite/form">Adicionar outros participantes</a></li>
												<li>Link para compartilhar : http://partiubolao.com.br/users/form?shareCode=${group.shareCode}</li>
											</c:if>
											</ul>
										</div>
									</div>
								</div>							
							</c:forEach>
							<div class="clearfix"></div>
							<h4>Opções dos seus campeonatos</h4>
							<div class="col-md-4">
								<div class="box box-solid box-success">
									<div class="box-header">Faça seus palpites</div>
									<div class="box-body">
										<ul class="dashboard_list">
											<c:forEach var="championship" items="${enrolledChampionships}">
												<li class="dashboard_list_item"><a href="/guess/${championship.id}/games/form">Palpite para ${championship.name}</a></li>
												<li class="dashboard_list_item"><a href="/guess/done/${championship.id}/games">Lista de palpites feitos</a></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="box box-solid box-warning">
									<div class="box-header">Ranking geral dos campeonatos</div>
									<div class="box-body">
										<ul class="dashboard_list">
											<c:forEach var="championship" items="${enrolledChampionships}">
												<li class="dashboard_list_item"><a href="/ranking/championship/${championship.id}">Ranking ${championship.name}</a></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="box box-solid box-info">
									<div class="box-header">Palpites iniciais</div>
									<div class="box-body">
										<ul class="dashboard_list">
											<c:forEach var="championship" items="${enrolledChampionships}">
												<li class="dashboard_list_item"><a href="/sweepstake/start/guess/form?championshipId=${championship.id}">Palpite inicial ${championship.name}</a></li>
											</c:forEach>
										</ul>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
							<h4>Relatórios</h4>		
							<div class="col-md-4">
								<div class="box box-solid box-info">
									<div class="box-header">Opções</div>
									<div class="box-body">
										<ul class="dashboard_list">											
												<li class="dashboard_list_item"><a href="/report/championship/results/form">Histórico de resultados</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>												
</bolao:masterPage>							
