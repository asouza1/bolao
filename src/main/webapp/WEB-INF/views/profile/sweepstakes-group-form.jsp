<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>

<bolao:masterPage title="Palpite">
	<jsp:attribute name="extraCss">
		<link href="/assets/js/jquery/multiple-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/projection.css" />
	</jsp:attribute>
	<jsp:attribute name="extraScripts">
		<script src="/assets/js/jquery/multiple-select/js/jquery.multi-select.js"></script>
		<script>
			$('#startGuessTypes').multiSelect();
		</script>			
	</jsp:attribute>
	<jsp:body>
		<div class="box" style="text-align: center">
			<div>
				<h1 class="title">Partiu Bolão!</h1>
			</div>
		</div>
		<div class="box">
			<div class="box-header" style="text-align: center">
				<h2>Crie um grupo e chame seus amigos</h2>
				<p class="success_message">
					${successMessage}
				</p>
			</div>
			<div class="box-body" style="text-align: center">
				<form:form action="/profile/group/sweepstakes" method="post" commandName="sweepstakesGroupForm">
					<div class="form-group" style="margin-top:1vw">
						<label for="name">Nome:</label>
						<form:input path="name" class="form-control"/>
						<form:errors path="name" class="text-red"/>
					</div>
					<div class="form-group">
						<label for="emails">Emails separados por vírgula:</label>
						<form:textarea path="emails" cols="30" placeholder="amigo@gmail.com,amigo2@gmail.com" class="form-control"/>
						<form:errors path="emails" class="text-red"/>
					</div>
					<div class="form-group">
						<label for="idChampionship">Campeonato:</label>
						<form:select path="idChampionship" items="${championshipList}" itemLabel="name" itemValue="id" class="form-control"/>
						<form:errors path="idChampionship" class="text-red"/>
					</div>
					<div class="form-group">
						<label for="startGuessTypes">Tipos de palpites iniciais a serem considerados:</label>
						<form:select path="startGuessTypes" items="${startGuessTypes}" itemLabel="label" itemValue="name" class="form-control"/>
						<form:errors path="startGuessTypes" class="text-red"/>
					</div>					
					<div class="form-group">
						<label for="idOwnerGroup">Convidar membros de outro grupo:</label>
						<form:select path="idOwnerGroup" class="form-control" >
							<form:option value="">--Seus outros grupos--</form:option>
							<form:options items="${ownerGroups}" itemLabel="name" itemValue="id"/>
						</form:select>
						<form:errors path="idOwnerGroup" class="text-red"/>
					</div>
					
					<button type="submit" class="btn">Criar grupo</button>
					
				</form:form>
			</div>
		</div>
</jsp:body>
</bolao:masterPage>