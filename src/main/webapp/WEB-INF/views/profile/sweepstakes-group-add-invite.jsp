<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<div class="box">
		<div class="box-header"  style="text-align: center">
			<h2>Convide amigos para seu bolao </h2>
		</div>
		<form:form action="/profile/group/sweepstakes/${group.id}/invite" method="post" 
					commandName="sweepstakesGroupInviteForm" class="form-group">
			<div class="box-body table-responsive no-padding"  style="text-align: ">
				<label>Emails separados por vírgula:</label>	
					<form:textarea path="emails" rows="15" class="form-control"/>
					<form:errors path="emails" cssClass="error_message"/>
			</div>
			<div class="box-footer">
                <button type="submit" class="btn btn-primary">Enviar convites</button>
             </div>
		</form:form>
	</div>
</bolao:masterPage>
