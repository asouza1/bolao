<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<sec:authentication property="principal" var="user"/>

<bolao:masterPage title="Trocar senha" menu="true">
	<jsp:body>
		<div class="box">
			<div class="box-header"  style="text-align: center">
				<h2 class="box-title">Partiu Bolão!</h2>
			</div>
		</div>
		<div class="box col-md-4" style="text-align: center">
			<div class="box-header"  style="text-align: center">
				<h3 class="box-title">Altere sua senha</h3>
			</div>
			<form:form action="/password/update" method="post" commandName="updatePasswordForm">
				<div class="box-body">
					<div class="form-group">
						<label for="email">Email:</label>
						<form:input path="email" class="form-control" readonly="true"/>
						<form:errors path="email" cssClass="error_message"/>
					</div>
					<div class="form-group">	
						<label for="secretPhrase">Frase secreta:</label>
						<form:input path="secretPhrase" class="form-control"/>
						<form:errors path="secretPhrase" cssClass="error_message"/>
					</div>
					<div class="form-group">						
						<label for="rawPassword">Nova senha</label>
						<form:password path="rawPassword" class="form-control"/>
						<form:errors path="rawPassword" cssClass="error_message"/>
					</div>

					<button type="submit" class="button">Atualizar a senha</button>							
				</div>
			</form:form>
		</div>		
	</jsp:body>	
</bolao:masterPage>
