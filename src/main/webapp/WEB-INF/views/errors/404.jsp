<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ include file="../../taglibs.jsp" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/forms.css" />
		<link rel="stylesheet" href="/assets/css/lists.css" />
		<link rel="stylesheet" href="/assets/css/reset.css" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Não encontrada</title>
	</head>

	<body>
		<div class="overlay">
			<div class="container_medium">

				<header>
					<div>
						<h1 class="title">Bolão</h1>
					</div>
				</header>

				<div class="content">
					A página procurada não existe. <a href="/dashboard">Clique aqui para visitar a página inicial</a>
				</div>
			</div>
		</div>
	</body>
</html>