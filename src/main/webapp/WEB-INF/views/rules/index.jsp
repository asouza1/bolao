<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo" menu="true">
	<jsp:body>
		<div class="box">
			<div class="box-header"  style="text-align: center">
				<h1 class="subtitle_rules">Regras de pontuação do bolão</h1>
			</div>
		</div>
		<div class="box" style="text-align: center">
			<div class="box">
				<div class="box-header">
					<h3 class="subtitle_rules">Pontuação básica (PB)</h3>
				</div>
				<div class="box-body">
					<dl>
						<dt>Acertou na mosca</dt>
						<dd>10 pontos</dd>

						<dt>Acerto parcial do placar: resultado mais um dos placares</dt>
						<dd>7 pontos</dd>

						<dt>Acertou o resultado, mas errou os placares</dt>
						<dd>5 pontos</dd>

						<dt>Errou o resultado, mas acertou um dos placares</dt>
						<dd>2 pontos</dd>

						<dt>Errou tudo</dt>
						<dd>Já sabe né? Zero!</dd>
					</dl>
				</div>
			</div>
			
			<div class="box">
				<div class="box-header">
					<h3 class="subtitle_rules">Palpite dobrado</h3>
				</div>
				<div class="box-body">
					<p>Para cada rodada do campeonato, você pode fazer um palpite e ter o <strong>valor dobrado</strong>. Por exemplo: caso você acerte na mosca, em vez de ganhar 10 pontos, vai ganhar 20.</p>
					<p>A ideia é gerar mais disputa dentro do seu grupo de bolão.</p>
				</div>
			</div>
			
			<div class="box">
				<div class="box-header">
					<h3 class="subtitle_rules">Pontuação dinâmica</h3>
				</div>
				<div class="box-body">
					<p>Essa é uma parte que tem tudo para dar aquela graça no bolão, pois vai privilegiar os que apostarem <em>contra a maioria</em>.</p>
					<p>Vamos ver um exemplo: Existe um jogo entre o time A e B. Dos apostadores, 8 apostaram na vitória de A, 1 no empate, e 1, na vitória de B. Os que apostaram na vitória de A ganharão (PA) * 1.2, já que eles estavam contra apenas 20% dos apostadores.</p>
					<p>Continuando, quem apostou no empate(PE), apostou contra 9 de 10 pessoas. Nesse caso ele vai ganhar (PE) * 1.9, assim como aquele que apostou na vitória de B, pois também estava contra 90% dos apostadores.</p>
					<p>Dessa forma, damos mais valor a quem corre mais riscos e deixamos o bolão mais imprevisível. A parte ruim é que o cálculo fica mais chato, mas já está sendo produzido um relatório onde você poderá acompanhar todos os cálculos por rodada do seu campeonato. :)</p>
					<p>Importante lembrar, a pontuação dinâmica só é valida se você acertou, no mínimo, o resultado. Só para deixar claro, caso você tenha sido o único a apostar no empate e o resultado do jogo foi vitória de alguém, não vai ter pontuação dinâmica para você.</p>
				</div>
			</div>
			
			<div class="box">
				<div class="box-header">
					<h2 class="subtitle_rules">Regras de chutes do bolão</h2>
				</div>
				<div class="box-body">
					<h3 class="subtitle_rules">Chutes iniciais</h3>
					<p>Antes de começar os campeonatos, você pode chutar o seguinte: Campeão, melhor ataque, melhor defesa e último colocado. Essa é mais uma ideia para deixar tudo aberto no bolão, ganhar não vai ser fácil. Cada acerto vai te render 30 pontos a mais no bolão.</p>

					<h3 class="subtitle_rules">Limite para o palpite</h3>
					<p>Os palpites podem ser feitos até 30 minutos antes dos jogos. Não esqueça disso! Depois não adianta vim choramingar.</p>
				</div>
			</div>
		</div>
</jsp:body>
</bolao:masterPage>