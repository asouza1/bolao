<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<div class="box">
		<div class="box-header"  style="text-align: center">
			<h2>Ranking geral do campeonato</h2>
			<c:if test="${lastResult.isPresent()}">
				<h3>atualizado as ${dateFormatter.format(lastResult.get().creationTime)}</h3>
			</c:if>
		</div>
		<div class="box-body table-responsive no-padding"  style="text-align: center">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th style="text-align: center">Posição</th>
						<th style="text-align: center">Nome</th>
						<th style="text-align: center">Pontuação</th>
					</tr>
					<c:forEach var="entry" items="${entries}" varStatus="status">
						<tr>
							<td>${status.count}</td>
							<td>${entry.memberName}</td>
							<td>${entry.acumulatedScore}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</bolao:masterPage>
