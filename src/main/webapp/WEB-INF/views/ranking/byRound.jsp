<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
						<div class="box">
							<div class="box-header"  style="text-align: center">
								<h3 class="box-title">Ranking da rodada ${round}</h3>
								<div class="box-tools">
									<form action="/ranking/group/${group.id}/by/round" method="get">
										<div class="input-group input-group-sm" style="width: 150px;">
											<input type="number" name="round" min="1"
												class="form-control pull-right" value="${param.round}" />
											<div class="input-group-btn">
												<button type="submit" class="btn btn-default">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="box-body table-responsive no-padding"  style="text-align: center">
								<table class="table table-hover">
									<tbody>
										<tr>
											<th style="text-align: center">Posição</th>
											<th style="text-align: center">Nome</th>
											<th style="text-align: center">Pontuação</th>
										</tr>
										<c:forEach var="entry" items="${entries}" varStatus="status">
											<tr>
												<td>${status.count}</td>
												<td>${entry.memberName}</td>
												<td>${entry.acumulatedScore}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
</bolao:masterPage>