<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<%@include file="../../taglibs.jsp"%>
<bolao:masterPage title="Ranking do grupo">
	<jsp:attribute name="extraCss">
		<link href="/assets/js/jquery/multiple-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,300,600" type="text/css">
		<link rel="stylesheet" href="/assets/css/fonts.css" />
		<link rel="stylesheet" href="/assets/css/projection.css" />
	</jsp:attribute>
	<jsp:attribute name="extraScripts">
		<script src="/assets/js/jquery/jquery.chained.remote.min.js"></script>
		<script>
			$(".guess").remoteChained({
	    		parents : "#championshipId",
	    		url : "/ajax/championship/teams",
	    		loading: "Carregando times",
	    		attribute: "data-param"
			});			
		</script>
		<script src="/assets/js/jquery/multiple-select/js/jquery.multi-select.js"></script>
		<script>
			$('#g4TeamsIds').multiSelect();
			$('#z4TeamsIds').multiSelect();
		</script>	
	</jsp:attribute>
	<jsp:body>
		<div class="box">
			<div class="box-header" style="text-align: center">
				<h2>Simule o fim do campeonato</h2>
			</div>
		</div>
		<div class="box" style="text-align: center">
			<form:form action="/ranking/group/${groupId}/projection" method="get" commandName="startGuessForm">
				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="championshipId">Campeonato</label>
						</div>
					<div class="col-xs-6 text-left">
						<form:select path="championshipId" data-param="id">
							<form:option value="">--Selecione--</form:option>
							<form:options items="${championshipList}" itemLabel="name" itemValue="id"/>							
						</form:select>
						<form:errors path="championshipId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="bestDefenseTeamId">Melhor defesa</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="bestDefenseTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="bestDefenseTeamId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="bestStrikeTeamId">Melhor Ataque</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="bestStrikeTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="bestStrikeTeamId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="championTeamId">Campeão</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="championTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="championTeamId" cssClass="error_message"/>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6 text-right">
						<label for="worstTeamId">Último colocado</label>
					</div>
					<div class="col-xs-6 text-left">
						<form:select path="worstTeamId" items="${teamList}" itemLabel="name" itemValue="id" cssClass="guess"/>
						<form:errors path="worstTeamId" cssClass="error_message"/>
					</div>
				</div>
					
				<div class="row">
						<label for="g4TeamsIds">Times do G4(pressione ctrl para escolher cada um dos times)</label>
						<form:select path="g4TeamsIds" items="${teamList}" itemLabel="name" itemValue="id" multiple="true"/>
						<form:errors path="g4TeamsIds" cssClass="error_message"/>
					</div>
				<div class="row">
						<label for="z4TeamsIds">Times do Z4(pressione ctrl para escolher cada um dos times)</label>
						<form:select path="z4TeamsIds" items="${teamList}" itemLabel="name" itemValue="id" multiple="true"/>
						<form:errors path="z4TeamsIds" cssClass="error_message"/>
					</div>
				<div class="row" style="margin:5px">
					<button type="submit" class="btn">Pesquisar"</button> 
				</div>
			</form:form>				
		</div>
					
		<div class="box-body table-responsive no-padding"  style="text-align: center">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th style="text-align: center">Posição</th>
						<th style="text-align: center">Nome</th>
						<th style="text-align: center">Pontuação</th>
					</tr>
					<c:forEach var="entry" items="${entries}" varStatus="status">
						<tr>
							<td>${status.count}</td>
							<td>${entry.memberName}</td>
							<td>${entry.acumulatedScore}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
</jsp:body>
</bolao:masterPage>