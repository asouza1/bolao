<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="../../taglibs.jsp" %>
<%@taglib tagdir="/WEB-INF/tags/" prefix="bolao"%>
<bolao:masterPage title="Ranking do grupo">
	<div class="box">
		<div class="box-header"  style="text-align: center">
			<h3 class="box-title">Ranking do grupo</h3>
		</div>
		<div class="box-body table-responsive no-padding"  style="text-align: center">
			<table class="table table-hover">
				<tbody>
					<tr>
						<th style="text-align: center">Posição</th>
						<th style="text-align: center">Nome</th>
						<th style="text-align: center">Pontuação</th>
					</tr>
					<c:forEach var="entry" items="${entries}" varStatus="status">
						<tr>
							<td>${status.count}</td>
							<td>${entry.memberName}</td>
							<td>${entry.acumulatedScore}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</bolao:masterPage>
