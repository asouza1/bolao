<%@ tag language="java" pageEncoding="UTF-8"%>
<%@attribute name="title" required="true"%>
<%@attribute name="extraScripts" fragment="true"%>
<%@attribute name="extraCss" fragment="true"%>
<%@attribute name="menu"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@include file="../taglibs.jsp"%>

<sec:authentication property="principal" var="user"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="/assets/css/AdminLTE.min.css">
<link rel="stylesheet" href="/assets/css/skin-blue.min.css">
<link rel="stylesheet" href="/assets/css/customStyle.css">
<jsp:invoke fragment="extraCss"/>
<title>${title}</title>
</head>

<body>
	<div class="wrapper">
		<header class="main-header" style="background-color:#${dashboardView.teamColorPrimary(user)}"> 
			<a href="/dashboard" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini" style="color:#${dashboardView.teamColorSecondary(user)}"><b>P</b>B</span> <!-- logo for regular state and mobile devices -->
				<span class="logo-lg" style="color:#${dashboardView.teamColorSecondary(user)}"><b>Partiu</b> Bolão</span>
			</a> 
			
			<nav class="navbar navbar-static-top" role="navigation"> 
				
				<c:if test="${not menu}">
				<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="color:#${dashboardView.teamColorSecondary(user)}">
					<span class="sr-only" style="color:#${dashboardView.teamColorSecondary(user)}">Ver menu</span>
				</a>
				</c:if>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li><a href="#" style="color:#${dashboardView.teamColorSecondary(user)}">Pense bem antes de palpitar!</a></li>
					</ul>
				</div>
			</nav> 
		</header>
		<c:if test="${not menu}">
			<aside class="main-sidebar" style="background-color:#${dashboardView.teamColorSecondary(user)}"> 
				<section class="sidebar">
					<div class="user-panel">
						<div class="pull-left image">
							${dashboardView.teamShieldImage(user)}					
						</div>
						<div class="pull-left info">
							<p style="color:#${dashboardView.teamColorPrimary(user)}">${user.name}</p>
							<a href="/logout"><i class="fa fa-circle text-success"></i>
								Logout</a>
						</div>
					</div>
					<ul class="nav sidebar-menu">
						<li class="header"><p style="color:#${dashboardView.teamColorPrimary(user)}">Menu de opções</p></li>
						<li><a href="/dashboard">Dashboard</a></li>
						<li><a href="/password/update/form">Trocar senha</a></li>
						<li><a href="/rules">Visualizar regras</a></li>
						<li><a href="/profile/group/sweepstakes/form">Criar novo
								grupo</a></li>
						<li><a href="/profile/choose/team">Escolha seu time</a></li>
						<li><a href="/sweepstakes/invite/pending">${invites.size()}
								convites pendentes</a></li>
					</ul>
				</section> 
			</aside>
		</c:if>

		<div class="${not menu ? 'content-wrapper' : 'col-md-6 col-md-offset-3'}">
			<div class="content-body">
				<div class="content-body">
					<div class="content">
						<jsp:doBody />
					</div>
				</div>
			</div>
		</div>

	</div>


	<!-- jQuery 2.2.0 -->
	<script src="/assets/js/jquery/jquery-2.1.4.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="/assets/js/bootstrap/bootstrap.min.js"></script>
	<script src="/assets/js/app.js"></script>
	<jsp:invoke fragment="extraScripts" />
</body>
</html>
