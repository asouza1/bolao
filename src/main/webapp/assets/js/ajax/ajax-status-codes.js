var ErrorPages = function(extraHandler){
	var handleError = function(data,xhr) {
		if(xhr.status >= 500) {
//			document.location.href = "/errors/500"
			console.log(data);
			return true;
		}
		return false;
	};
	
	var handlers = [extraHandler,handleError];
	
	return {
		handle(data,xhr){			
			for(i=0;i<handlers.length;i++){				
				if(handlers[i](data,xhr)){
					return;
				}
			}			
		}
	};
}