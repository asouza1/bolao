var GuessGame = function(){
	
	var baseErrorHandler = function(result,xhr,messageDisplayer){
		var error = JSON.parse(result.responseText);
		var validationMessages = error.validationMessages;
		
		for(i=0; i < validationMessages.length; i++){			
			messageDisplayer.append("<p class='error_message'>"+validationMessages[i].message+"</p>");
		}
		
		messageDisplayer.fadeIn("slow");
	}
	var guessGameSuccessFunction = function(result,messageDisplayer){
		messageDisplayer.html("<p class='success_message'>Palpite registrado com sucesso</p>");
		messageDisplayer.fadeIn("slow");
	}	
	
	return {		
		clickGuess : function(buttonSelector){
			$(buttonSelector).click(function(event){
				event.preventDefault();
				
				var button = $(this);				
				var form = button.closest('form');				
				var xhr = $.post(form.attr("action"),form.serialize());
				
				var messageDisplayer = function(){
					var spanMessage = $("span[data-game-id='"+form.attr("data-game-id")+"']");
					spanMessage.empty();
					spanMessage.hide();
					return spanMessage;					
				};
				
				var loading = messageDisplayer();
				loading.html("<p class='success_message'>Registrando...</p>");
				loading.fadeIn("slow");
				
				xhr.done(function(result){
					guessGameSuccessFunction(result,messageDisplayer());
				}).fail(function(result){
					baseErrorHandler(result,xhr,messageDisplayer());				
				});
			});			
		},
		clickAverageInfo : function(linkSelector){
			$(linkSelector).click(function(event){				
				event.preventDefault();
				var link = $(this);
				var gameId = link.attr("data-game-id");
				var infoElement = $("#average-info-"+gameId);
				
				var outcomeReplacer = function(outcomes,infoElement){
					for(index in outcomes){
						var outcome = outcomes[index].possibleGameResult;
						infoElement.html(infoElement.html().replace(outcome,outcomes[index].percentageBy100));						
					}
					
				};
				
				if(infoElement.is(":hidden")){
					var xhr = $.get(link.attr("data-href"));					
					xhr.done(function(outcomes){
						outcomeReplacer(outcomes,infoElement);
						infoElement.toggle();
					}).fail(function(result){
						baseErrorHandler(result,xhr,function(){
							return algumLugarErrorsGerais;
						});				
					});
				} else {
					infoElement.hide();
				}
			});			
		}		
	}
}